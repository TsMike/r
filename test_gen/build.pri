MOC_DIR = $$TOPDIR/tmp/moc
UI_DIR  = $$TOPDIR/tmp/ui
RCC_DIR = $$TOPDIR/tmp/rcc

# TODO: OS dependency
# TODO: compiler dependency -_CRT_SECURE_NO_DEPRECATE   

DEFINES += OS_WIN LOG_DEBUG_FLUSH
//DEFINES += QT_NO_DEBUG_OUTPUT


CONFIG(debug, debug|release) {
   DESTDIR = $$TOPDIR/bin/debug
   OBJECTS_DIR = $$TOPDIR/tmp/obj/debug
} else {
   DESTDIR = $$TOPDIR/bin/release
   OBJECTS_DIR = $$TOPDIR/tmp/obj/release
}

LIBS += -L$$DESTDIR -L$$TOPDIR/libs
