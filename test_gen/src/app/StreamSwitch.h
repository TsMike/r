#if !defined(STREAM_SWITCH_H)
#define STREAM_SWITCH_H

#include "app/CfgDefs.h"
#include "app/Msg.h"

#include <QtCore/QMap>


//TMsgWrapperPoolQueue*

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TStreamSwitch
{
	public:
		typedef unsigned TChannelAddr;

		//---
		bool addChannelBranch(TChannelAddr channel, int outChannelsNum)
		{
			if(getKeyIterator(channel) != mSwitchMap.end()) {
				qDebug() << "ChannelBranch" << channel << "exist";
				return false;
			}
			mSwitchMap.insert(channel,TQueueArray(outChannelsNum + 1));
			return true;
		}

		int dispatchMessages();
		TMsgWrapperPoolQueue* connectInflowQueue(TChannelAddr channel) { return connectQueue(channel,0); }
		bool disconnectInflowQueue(TChannelAddr channel) { return disconnectQueue(channel,0);	}
		TMsgWrapperPoolQueue* connectOutflowQueue(TChannelAddr channel, int nQueue)
		{
			if(!isQueueConnected(channel,nQueue+1))
				return connectQueue(channel,nQueue+1);
			else
				return 0;
		}

		bool disconnectOutflowQueue(TChannelAddr channel, int nQueue) { return disconnectQueue(channel,nQueue+1); }

		//---
		bool isQueueConnected(TChannelAddr channel, int nQueue)
		{
			TSwitchMap::iterator switchChannel;
			if(isQueueExist(channel,nQueue,switchChannel)) {
				return switchChannel.value()[nQueue].mConnected;
			}
			return false;
		}

		//---
		int outQueueNum(TChannelAddr channel)
		{
			TSwitchMap::iterator switchChannel = getKeyIterator(channel);
			if(switchChannel != mSwitchMap.end()) {
				if(switchChannel.value().size())
					return switchChannel.value().size() - 1;
			}
			return 0;
		}

	private:
		struct TStreamChannelParams
		{
			TStreamChannelParams() : mQueue(), mConnected(false) {}

			TMsgWrapperPoolQueue mQueue;
			bool                 mConnected;
		};

		typedef QVector<TStreamChannelParams>  TQueueArray;
		typedef QMap<TChannelAddr,TQueueArray> TSwitchMap;

		//---
		TSwitchMap::iterator getKeyIterator(TChannelAddr channel) { return mSwitchMap.find(channel); }

		//---
		bool isQueueExist(TChannelAddr channel, int nQueue, TSwitchMap::iterator& switchChannel)
		{
			switchChannel = getKeyIterator(channel);
			if(switchChannel != mSwitchMap.end()) {
				if(switchChannel.value().size() > nQueue) {
					return true;
				}
			}
			return false;
		}

		//---
		TMsgWrapperPoolQueue* connectQueue(TChannelAddr channel, int nQueue)
		{
			TSwitchMap::iterator switchChannel;
			if(isQueueExist(channel,nQueue,switchChannel)) {
				switchChannel.value()[nQueue].mConnected = true;
				return &switchChannel.value()[nQueue].mQueue;
			}
			return 0;
		}

		//---
		bool disconnectQueue(TChannelAddr channel, int nQueue)
		{
			TSwitchMap::iterator switchChannel;
			if(isQueueExist(channel,nQueue,switchChannel)) {
				switchChannel.value()[nQueue].mConnected = false;
				return true;
			}
			return false;
		}

		//---
		int outConnectionsNum(TSwitchMap::iterator channel)
		{
			int num = 0;
			for(int n = 1; n < channel.value().size(); ++n) {
				if(channel.value()[n].mConnected)
					++num;
			}
			return num;
		}

		TSwitchMap mSwitchMap;
};

#endif // STREAM_SWITCH_H


