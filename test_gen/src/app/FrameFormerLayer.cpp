//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/FrameFormer.h"
#include "app/FrameFormerLayer.h"
#include "app/AF_System.h"
#include "ip_pipe/IP_PipeLib.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#if defined(MATH_IP_PIPE_TX)
	static const TCHAR* ToMathPipeFile = TEXT("toMath");
	static const TCHAR* ToMathPipeView = TEXT("toMath_tx");
	static const uint32_t PipeToMathChunkSize = 1024*1024;
	static const uint32_t PipeToMathChunkNum  = 256;
	static const int32_t  PipeToMathTimeout   = 10;
#endif

//-----------------------------------------------------------------------------
TFrameFormerLayer::TFrameFormerLayer(TSystemAf* systemAf) :
									 TThread(L"FrameFormerThread"),
                                     #if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
                                        mTestGenTimer(),
                                     #endif
                                     mSystemAf(systemAf),
                                     mSemaphore(),
                                     mDrvBufQueue(),
                                     mDevBundleParser(),
									 mStreamSwitch(),
									 mVideoFrameFormer(0),
									 mInfoFrameFormer(0),
									 #if defined(MATH_IP_PIPE_TX)
										 mMathIpPipeTxQueue(0),
									 #endif
									 mScreenFrameQueue()
{
    #if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
        mTestGenTimer.setTimerType(Qt::PreciseTimer);
		connect(&mTestGenTimer,&QTimer::timeout,this,&TFrameFormerLayer::testGenSlot);
        mTestGenTimer.start(CfgDefs::TestFramePeriod);
    #endif

	mVideoFrameFormer = new TVideoFrameFormer(mSystemAf->getBufPool().getPool<TVideoScreenFrame::TFramePool>());
	mInfoFrameFormer  = new TInfoFrameFormer(mSystemAf->getBufPool().getPool<TInfoScreenFrame::TFramePool>());

	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		#if defined(STREAM_SWITCH_TEST)
			mStreamSwitch.addChannelBranch(CfgDefs::VideoStreamId,3);
			mStreamSwitch.addChannelBranch(CfgDefs::InfoStreamId,2);
			mStreamSwitch.addChannelBranch(CfgDefs::TestStreamId,1);
		#else
			#if defined(MATH_IP_PIPE_TX)
				mStreamSwitch.addChannelBranch(CfgDefs::VideoStreamId,2);
				mMathIpPipeTxQueue = mStreamSwitch.connectOutflowQueue(CfgDefs::VideoStreamId, 1);
			#else
				mStreamSwitch.addChannelBranch(CfgDefs::VideoStreamId,1);
			#endif
			mStreamSwitch.addChannelBranch(CfgDefs::InfoStreamId,1);
			mVideoFrameFormer->setInQueue(mStreamSwitch.connectOutflowQueue(CfgDefs::VideoStreamId, 0));
			mVideoFrameFormer->setOutQueue(&mScreenFrameQueue);
			mInfoFrameFormer->setInQueue(mStreamSwitch.connectOutflowQueue(CfgDefs::InfoStreamId, 0));
			mInfoFrameFormer->setOutQueue(&mScreenFrameQueue);
		#endif

		//--- dev 1
		TDevParser* devParser1 = new TDevParser;
		devParser1->attachChannelParser(CfgDefs::VideoStreamId, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::VideoStreamId)));
		devParser1->attachChannelParser(CfgDefs::InfoStreamId, new TFrameParser(CfgDefs::InfoScreenFrameWidth,CfgDefs::InfoScreenFrameHeight,mSystemAf->getBufPool().getPool<TInfoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::InfoStreamId)));
		devParser1->attachChannelParser(CfgDefs::TestStreamId, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::TestStreamId)));
		mDevBundleParser.attachDevParser(100,devParser1);

		//--- dev 2
		TDevParser* devParser2 = new TDevParser;
		devParser2->attachChannelParser(CfgDefs::VideoStreamId, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::VideoStreamId)));
		devParser2->attachChannelParser(CfgDefs::InfoStreamId, new TFrameParser(CfgDefs::InfoScreenFrameWidth,CfgDefs::InfoScreenFrameHeight,mSystemAf->getBufPool().getPool<TInfoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::InfoStreamId)));
		devParser2->attachChannelParser(CfgDefs::TestStreamId, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::TestStreamId)));
		mDevBundleParser.attachDevParser(CfgDefs::getViewNetSrcFilter(),devParser2);
	#endif

	#if (WORK_MODE == WORK_APPLICATION)
        #if defined(MATH_IP_PIPE_TX)
            mStreamSwitch.addChannelBranch(CfgDefs::VideoStreamId,2);
            mMathIpPipeTxQueue = mStreamSwitch.connectOutflowQueue(CfgDefs::VideoStreamId,1);
        #else
            mStreamSwitch.addChannelBranch(CfgDefs::VideoStreamId,1);
        #endif
        mStreamSwitch.addChannelBranch(CfgDefs::InfoStreamId,1);
        mVideoFrameFormer->setInQueue(mStreamSwitch.connectOutflowQueue(CfgDefs::VideoStreamId, 0));
        mVideoFrameFormer->setOutQueue(&mScreenFrameQueue);
        mInfoFrameFormer->setInQueue(mStreamSwitch.connectOutflowQueue(CfgDefs::InfoStreamId, 0));
        mInfoFrameFormer->setOutQueue(&mScreenFrameQueue);

        //--- dev 1
        TDevParser* devParser1 = new TDevParser;
        devParser1->attachChannelParser(CfgDefs::VideoStreamId, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::VideoStreamId)));
        devParser1->attachChannelParser(CfgDefs::InfoStreamId, new TFrameParser(CfgDefs::InfoScreenFrameWidth,CfgDefs::InfoScreenFrameHeight,mSystemAf->getBufPool().getPool<TInfoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::InfoStreamId)));
        devParser1->attachChannelParser(CfgDefs::TestStreamId, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::TestStreamId)));
        mDevBundleParser.attachDevParser(CfgDefs::getViewNetSrcFilter(),devParser1);
	#endif

	#if defined(MATH_IP_PIPE_TX)
		bool status = IP_pipe::createPipeView(ToMathPipeFile,TEXT("tx"),PipeToMathChunkSize,PipeToMathChunkNum);
		qDebug() << "[INFO] IP_Pipe " << qPrintable(QString::fromWCharArray(ToMathPipeFile)) << "(tx) creation status: " << status;
		IP_pipe::setRdy(ToMathPipeView);
	#endif

    #if 1
        qDebug() << qPrintable(QString("[FrameFormerLayer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
TFrameFormerLayer::~TFrameFormerLayer()
{
    threadFinish();
	delete mVideoFrameFormer;
	delete mInfoFrameFormer;

    #if 1
        qDebug() << qPrintable(QString("[FrameFormerLayer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::onExec()
{

    mSemaphore.acquire();
    if(threadExit())
        return threadExit();

    #if 0
        qDebug() << qPrintable(QString("[FrameFormerLayer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("onExec()").leftJustified(SysUtils::JobNameJustify));
    #endif

	TScreenFramePtr framePtr;

	#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
        if(genTestFrame(framePtr)) {
			framePtr->setNetDst(CfgDefs::VideoStreamId);
			emit sendFrameSignal(framePtr);
        }
	#endif

	#if ((WORK_MODE == TESTGEN_AT_DRV_LAYER) || (WORK_MODE == WORK_APPLICATION))
        TDrvBufPtr drvBufPtr;
		if(mDrvBufQueue.get(drvBufPtr) && drvBufPtr) {
            //qDebug() << "[drvBuf received] srcAddr:" << qPrintable(QString("[drvBuf received] src: %1").arg(drvBufPtr->netSrc(),8,16,QLatin1Char('0'))) << "msgClassId:" << drvBufPtr->msgClassId();
			mDevBundleParser.parseDrvBuf(drvBufPtr);
			#if defined(STREAM_SWITCH_TEST) && (WORK_MODE == TESTGEN_AT_DRV_LAYER)
				streamTest();
			#else
				/* int nDispatchedMsgs = */ mStreamSwitch.dispatchMessages();
				/* int nVideoFrames    = */ mVideoFrameFormer->formFrames();
				/* int nInfoFrames     = */ mInfoFrameFormer->formFrames();
				#if 0 /*TEST*/
					if(nVideoFrames || nInfoFrames)
						qDebug() << "video frames formed:" << nVideoFrames << "info frames formed:" << nInfoFrames;
				#endif
				if(mScreenFrameQueue.get(framePtr)) {
					frameFilter(framePtr);
				}
				#if 0 /*TEST*/
					if(nDispatchedMsgs) {
						mSystemAf->getBufPool().bufPoolInfo();
					}
				#endif
			#endif
        }
    #endif

	#if defined(MATH_IP_PIPE_TX)
		if(mMathIpPipeTxQueue && mMathIpPipeTxQueue->get(framePtr) && framePtr && (framePtr->netSrc() == CfgDefs::MathNetSrcFilter)) {
			if(!IP_pipe::isPipeReady(ToMathPipeView)) {
				return threadExit();
			}
			if(!IP_pipe::isBufFull(ToMathPipeView)) {
				uint32_t* bufSizePtr;
				uint8_t*  chunk;
				IP_pipe::TStatus trStatus = IP_pipe::chunkAccess(ToMathPipeView, bufSizePtr, chunk, PipeToMathTimeout);
				if((trStatus == IP_pipe::Ok) && (serializeFrame<TBaseFrame,TRawFrame>(framePtr,chunk,bufSizePtr))) {
					trStatus = IP_pipe::advanceIdx(ToMathPipeView);
					if(trStatus == IP_pipe::Ok) {
						if((framePtr->msgId()%100) == 0)
							qDebug() << "[INFO] [mathTx]" << "src:" << framePtr->netSrc() << "id:" << framePtr->msgId() << "transfer Ok";
					} else {
						qDebug() << "[ERROR] [mathTx]" << "src:" << framePtr->netSrc() << "id:" << framePtr->msgId() << "status:" << trStatus << "transfer error (3)";
					}
				} else {
					qDebug() << "[ERROR] [mathTx]" << "src:" << framePtr->netSrc() << "id:" << framePtr->msgId() << "status:" << trStatus << "transfer error (2)";
				}
			} else {
				qDebug() << "[ERROR] [mathTx]" << "src:" << framePtr->netSrc() << "id:" << framePtr->msgId() << "transfer error (1)";
			}

		}
	#endif

    return threadExit();
}

//-----------------------------------------------------------------------------
void TFrameFormerLayer::receiveDrvBufSlot(TDrvBufPtr drvBufPtr)
{
	//qDebug() << "[TFrameFormer::receiveDrvBufSlot]" << SysUtils::getThreadId();
    mDrvBufQueue.put(drvBufPtr);
    mSemaphore.release();
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::getFrame(TScreenFramePtr& testFrame)
{
    return mSystemAf->getBufPool().getBuf<TVideoScreenFrame::TFramePool>(testFrame);
}

//-----------------------------------------------------------------------------
#if (WORK_MODE != TESTGEN_AT_FRAME_FORMER_LAYER)
void TFrameFormerLayer::frameFilter(TScreenFramePtr& framePtr)
{
	if(framePtr->netSrc() == /*CfgDefs::getViewNetSrcFilter()*/CfgDefs::ViewNetSrcFilter) {
		emit sendFrameSignal(framePtr);
		//qDebug() << "frame transmitted to UI Layer" << framePtr->netDst();
	} else {
		qDebug() << "frame rejected" << framePtr->netSrc() << framePtr->netDst();
		#if !defined(MSG_SELF_RELEASE)
			releaseMsg(framePtr);
		#endif
	}
}
#endif

#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)

//-----------------------------------------------------------------------------
template<typename T,int FNum> static inline typename T::TPixel testPixGen(int, int, int);

#if 0
//-----------------------------------------------------------------------------
typedef TFrame<TQtFrameImpl<QImage::Format_Indexed8>>	TGrayFrame;

//-----------------------------------------------------------------------------
template<> static inline typename TGrayFrame::TPixel testPixGen<TGrayFrame,MOVING_BANDS>(int,int col, int bufNum)
{
    return ((col + bufNum) & 0xFF);
}

//-----------------------------------------------------------------------------
template<> static inline typename TGrayFrame::TPixel testPixGen<TGrayFrame,MOVING_CROSS>(int cond,int, int)
{
    return (cond ? 200:100);
}
#endif

#if 0
//-----------------------------------------------------------------------------
typedef TFrame<TQtFrameImpl<QImage::Format_RGB32>>	TColorFrame;

//-----------------------------------------------------------------------------
template<> static inline typename TColorFrame::TPixel testPixGen<TColorFrame,MOVING_BANDS>(int,int col, int bufNum)
{
    quint8 pixVal = (col + bufNum) & 0xFF;
    return qRgba(0,pixVal,pixVal,255);
}

//-----------------------------------------------------------------------------
template<> static inline typename TColorFrame::TPixel testPixGen<TColorFrame,MOVING_CROSS>(int cond,int brightness, int)
{
    return (cond ? qRgba(200,0,0,255) : qRgba(brightness,0,brightness,255));
}
#endif

//-----------------------------------------------------------------------------
typedef TScreenFrame	TScreenFrameProxy;

//-----------------------------------------------------------------------------
template<> static inline typename TScreenFrameProxy::TPixel testPixGen<TScreenFrameProxy,MOVING_BANDS>(int,int col, int bufNum)
{
	quint8 pixVal = (col + bufNum) & 0xFF;
	return qRgba(0,pixVal,pixVal,255);
}

//-----------------------------------------------------------------------------
template<> static inline typename TScreenFrameProxy::TPixel testPixGen<TScreenFrameProxy,MOVING_CROSS>(int cond,int brightness, int)
{
	return (cond ? qRgba(200,0,0,255) : qRgba(brightness,0,brightness,255));
}


//-----------------------------------------------------------------------------
bool TFrameFormerLayer::genTestFrame(TScreenFramePtr& testFrame)
{
    if(getFrame(testFrame)) {
        TBaseFrame* testFrameProxy = checkMsg<TBaseFrame>(testFrame);
        /*TEST*/ //TBaseFrame* testFrameProxy = checkMsg<TVideoScreenFrame>(testFrame);
        if(!testFrameProxy) {
            #if !defined(MSG_SELF_RELEASE)
                releaseMsg(testFrame);
            #endif
            qDebug() << "[ERROR] TFrameFormer::genTestFrame";
            return false;
        }

        #if defined(FRAME_RESIZE_TEST)
            static int width = testFrameProxy->width();
            static int height = testFrameProxy->height();
            if(width < CfgDefs::VideoScreenFrameWidth/8) {
                width = CfgDefs::VideoScreenFrameWidth;
            } else {
                width -= 4;
            }
            if(height < CfgDefs::VideoScreenFrameHeight/8) {
                height = CfgDefs::VideoScreenFrameHeight;
            } else {
                height -= 4;
            }
            testFrameProxy->resizeImg(width,height);
        #endif
        static quint8 bufNum = 0;
        TScreenFrame::TPixel* frameBuf = testFrameProxy->getPixelBuf<TScreenFrame>();

        #if (PIXGEN_FUNC == MOVING_BANDS)
            for(int row = 0; row < testFrameProxy->height(); ++row) {
                for(int col = 0; col < testFrameProxy->width(); ++col) {
                    *frameBuf++ = testPixGen<TScreenFrameProxy,MOVING_BANDS>(row,col,bufNum);
                }
            }
        #elif (PIXGEN_FUNC == MOVING_CROSS)
            const int crossWidthX = 10;
            const int crossWidthY = 10;
            static int xCross     = 0;
            static int yCross     = 0;

            for(int row = 0; row < testFrameProxy->height(); ++row) {
                for(int col = 0; col < testFrameProxy->width(); ++col) {
                    int isCross = ((row >= yCross) && (row <= yCross + crossWidthY)) || ((col >= xCross) && (col <= xCross + crossWidthX)) ? 1 : 0;
                    *frameBuf++ = testPixGen<TScreenFrameProxy,MOVING_CROSS>(isCross,bufNum,0);
                }
            }
            if(++yCross >= testFrameProxy->height())
                yCross = 0;
            if(++xCross >= testFrameProxy->width())
                xCross = 0;
        #else
        #endif
        ++bufNum;
        return true;
    } else {
        return false;
    }
}
#endif

#if defined(STREAM__TEST)
//-----------------------------------------------------------------------------
void TFrameFormerLayer::streamTest()
{
	const unsigned TestPeriod = 200;
    const unsigned ChannelsNum = 3;
    const unsigned BranchesNum = 3;
    const unsigned NetSrcNum   = 2;

    static unsigned FramesRvd[ChannelsNum][BranchesNum][NetSrcNum];

	static unsigned TestCount = TestPeriod;

	if(--TestCount == 0) {
		TestCount = TestPeriod;
        //qDebug() << "*** slonick ***";

        //---
        TMsgWrapperPoolQueue* queuePool[ChannelsNum][BranchesNum];
        for(int channel = 0; channel < ChannelsNum; ++channel) {
            for(int branch = 0; branch < BranchesNum; ++branch) {
				queuePool[channel][branch] = mStreamSwitch.connectOutflowQueue(channel+1, branch);
            }
        }

        //---
        QElapsedTimer timer;
        timer.start();
		int nDispatchedMessages = mStreamSwitch.dispatchMessages();
        qDebug() << "time elapsed:" << qPrintable(SysUtils::getElapsedTime(timer));

        if(nDispatchedMessages) {
            int nFreeV = mSystemAf->getBufPool().getPool<TVideoRawFrame::TFramePool>()->size();
            int nFreeI  = mSystemAf->getBufPool().getPool<TInfoRawFrame::TFramePool>()->size();
            qDebug() << "[messages dispatched] msgs:" << nDispatchedMessages << "freeV:" << nFreeV << "freeI:" << nFreeI;
        }

        //---
        TBaseMsgWrapperPtr msg;
        for(int channel = 0; channel < ChannelsNum; ++channel) {
            for(int branch = 0; branch < BranchesNum; ++branch) {
                if(queuePool[channel][branch]) {
                    while(queuePool[channel][branch]->get(msg)) {
                        //qDebug() << "[frame dispatched] src:" << msg->netSrc() << "channel:" << channel+1 /* << "msg pool Id:" << msg->msgPoolId() */ << "dst:" << msg->netDst() << "branch:" << branch;
                        int srcIdx = (msg->netSrc() == 100) ? 0 : 1;
                        if(FramesRvd[channel][branch][srcIdx] != msg->msgId()) {
                            FramesRvd[channel][branch][srcIdx] = msg->msgId()+1;
                            qDebug() << "[ERROR] slon" << channel << branch << srcIdx << "*" << FramesRvd[channel][branch][srcIdx] << msg->msgId();
                        } else {
                            ++FramesRvd[channel][branch][srcIdx];
                            qDebug() << "[INFO] slon" << channel << branch << srcIdx << "*" <<  msg->msgId();
                        }

                        #if !defined(MSG_SELF_RELEASE)
                            releaseMsg(msg);
                        #endif
                    }
                }
            }
        }

        //---
        for(int channel = 0; channel < ChannelsNum; ++channel) {
            for(int branch = 0; branch < BranchesNum; ++branch) {
				mStreamSwitch.disconnectOutflowQueue(channel+1, branch);
            }
        }
    }
}

#endif
