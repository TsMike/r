//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/qmath.h>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtGui/QResizeEvent>
#include <QtCore/QDebug>
/*TEST*/ //#include <QtOpenGL/QGLWidget>
#include <QtCore/QCoreApplication> /*TEST*/

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/AF_System.h"
#include "app/UI_Layer.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
T_UI_Layer::T_UI_Layer(TSystemAf* systemAf) :
                                QMainWindow(0, 0/* Qt::FramelessWindowHint*/),
								mSystemAf(systemAf),
								mMainScene(0),
								mMainView(0),
								mVideoStreamPixmap(0),
								mInfoStreamPixmap(0),
								mTimer()
{
	#if (GENERATED_STREAM == THERMO_STREAM_GEN)
		setWindowTitle(tr("Thermo Stream Generator"));
	#elif (GENERATED_STREAM == VIDEO_STREAM_GEN)
		setWindowTitle(tr("Video Stream Generator"));
	#else
	#endif

	/*QStatusBar* statusInfo = */ statusBar();
    /*TEST*/ //statusInfo->setSizeGripEnabled(false);
    /*TEST*/ //statusInfo->showMessage(tr("slonick"),2000);
    createMainScene();
	mTimer.start();

    #if 1
        qDebug() << qPrintable(QString("[UI_Layer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
T_UI_Layer::~T_UI_Layer()
{
    #if defined(USE_AF_SYSTEM_SINGLETON)
        delete mMainView;
        delete mMainScene;
    #endif

    #if 1
        qDebug() << qPrintable(QString("[UI_Layer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
void T_UI_Layer::createMainScene()
{
    #if !defined(USE_AF_SYSTEM_SINGLETON)
        mMainScene = new TGraphicsScene(this);
    #else
        mMainScene = new TGraphicsScene;
    #endif

    #if 0 /*TEST*/
        QRadialGradient gradient(0,0,100);
        gradient.setSpread(QGradient::RepeatSpread);
        mMainScene->setBackgroundBrush(gradient);
        // Qt::lightGray

    #endif
	mMainScene->setBackgroundBrush(QColor(0x00,0x85,0xFF));
	//mMainScene->setBackgroundBrush(QColor(0x00,0x00,0x00));

	initPixmapItem(CfgDefs::InfoScreenFrameWidth,CfgDefs::InfoScreenFrameHeight,QColor(0x00,0x00,0x00,255),0);
	mVideoStreamPixmap = initPixmapItem(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,QColor(0xc0,0xc0,0xc0,255),1);
	mInfoStreamPixmap = initPixmapItem(CfgDefs::InfoScreenFrameWidth,CfgDefs::InfoScreenFrameHeight,QColor(0x00,0x00,0x00,0),2);
	mMainView = new TGraphicsView(mMainScene,this);

    /*TEST*/ //mMainView->setAttribute(Qt::WA_PaintOnScreen);

    /*TEST*/ mMainView->setFrameStyle(QFrame::NoFrame);
    /*TEST*/ //mMainView->setFrameStyle(QFrame::Box | QFrame::Plain);

    /*TEST*/ //QGLWidget* glWidget = new QGLWidget(QGLFormat(QGL::SampleBuffers));
    /*TEST*/ //mMainView->setViewport(glWidget);

    /*TEST*/ //mMainView->setLineWidth(10);

    /*TEST*/ //mMainView->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    /*TEST*/ //mMainView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    /*TEST*/ //mMainView->setAlignment(Qt::AlignCenter);
    setCentralWidget(mMainView);
}

//-----------------------------------------------------------------------------
QGraphicsPixmapItem* T_UI_Layer::initPixmapItem(unsigned width, unsigned height, QColor color, qreal zValue)
{
	QPixmap pixmap(width, height);
	pixmap.fill(color);
	QGraphicsPixmapItem* pixmapItem = new QGraphicsPixmapItem(pixmap);
	pixmapItem->setZValue(zValue);
	qreal dx = (CfgDefs::MainFrameWidth - width)/2;
	qreal dy = (CfgDefs::MainFrameHeight - height)/2;
	qDebug() << "dx" << dx << "dy" << dy;
	pixmapItem->moveBy(dx,dy);
	/*TEST*/ pixmapItem->setCacheMode(QGraphicsItem::NoCache);
	mMainScene->addItem(pixmapItem);
	return pixmapItem;
}

//-----------------------------------------------------------------------------
void T_UI_Layer::showFrame(TScreenFramePtr& framePtr, QGraphicsPixmapItem* pixmapItem)
{
	TBaseFrame* frame = checkMsg<TBaseFrame>(framePtr);
	if(frame) {
		//qDebug() << "showFrame:" << (*framePtr).msgId();
		QPixmap pixmap = QPixmap::fromImage(*(frame->getImage()));
		pixmapItem->setPixmap(pixmap);
		#if defined(FRAME_RESIZE_TEST)
			slon!
			mMainView->setSceneRect(mVideoStreamPixmap->boundingRect());
			QResizeEvent resizeEvent(mMainView->size(),mMainView->size());
			/*TEST*//*DON'T WORK!*/ //QCoreApplication::sendEvent(mMainView,&resizeEvent);
			mMainView->resizeEvent(&resizeEvent);
		#endif
	} else {
		qDebug() << "[ERROR] T_UI_Layer::receiveFrameSlot src:" << framePtr->netSrc() << "dst:" << framePtr->netDst();
	}
}

//-----------------------------------------------------------------------------
void T_UI_Layer::receiveFrameSlot(TScreenFramePtr framePtr)
{
    /*TEST*/ // qDebug() << "Rx Frame:" << framePtr->msgPoolId() << SysUtils::getThreadId();
    if(framePtr) {
		switch(framePtr->netDst()) {
			case CfgDefs::VideoStreamId:
				showFrame(framePtr,mVideoStreamPixmap);
				break;
			case CfgDefs::InfoStreamId:
				showFrame(framePtr,mInfoStreamPixmap);
				break;
			case CfgDefs::TestStreamId:
				showFrame(framePtr,mVideoStreamPixmap);
				//qDebug() <<  "frame rate:" << mTimer.elapsed() << "ms";
				mTimer.start();

				break;
			default:
				qDebug() << "this dst id not showed on the screen" << framePtr->netDst();
				break;
		}
        #if !defined(MSG_SELF_RELEASE)
            releaseMsg(framePtr);
        #endif
    } else {
        qDebug() << "[ERROR] Rx Frame";
    }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TGraphicsScene::TGraphicsScene(QObject* parent) : QGraphicsScene(parent)
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsScene]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
TGraphicsScene::~TGraphicsScene()
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsScene]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TGraphicsView::TGraphicsView(QGraphicsScene* scene, T_UI_Layer* manager) :
                                                                              QGraphicsView(scene),
                                                                              mManager(manager)
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsView]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
TGraphicsView::~TGraphicsView()
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsView]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
void TGraphicsView::resizeEvent(QResizeEvent* event)
{
    QSize newSize = event->size();
    const int dX = 20;
    const int dY = 20;
    qreal viewScale = 1.0;

	#if 1
		const int width  = CfgDefs::MainFrameWidth;
		const int height = CfgDefs::MainFrameHeight;
    #else
        const int width  = mManager->getVideoPixmap()->pixmap().width();
        const int height = mManager->getVideoPixmap()->pixmap().height();
    #endif

    if((newSize.width() > (width + dX)) && (newSize.height() > ( + dY))) {
        qreal scaleX = (contentsRect().width()  - dX)/static_cast<qreal>(width);
        qreal scaleY = (contentsRect().height() - dY)/static_cast<qreal>(height);
        viewScale = qMin(scaleX,scaleY);
    }
    setTransform(QTransform::fromScale(viewScale,viewScale),false);
    /*TEST*/ //testViewRelativeCrd(newSize,viewScale);
    QGraphicsView::resizeEvent(event);
}

//-----------------------------------------------------------------------------
/*TEST*/ void TGraphicsView::testViewRelativeCrd(QSize size, qreal scale)
{
    QPointF p = mapToScene(QPoint(0,0));
    qreal dX = (p.x() > 0) ? 0.0 : qFabs(p.x());
    qreal dY = (p.y() > 0) ? 0.0 : qFabs(p.y());

    qreal x = (contentsRect().width() - scale*2*dX)/scale;
    qreal y = (contentsRect().height() - scale*2*dY)/scale;
    qDebug() << "[Viewport]" << size << rect() << contentsRect() /*<< sceneRect()*/ << p << "x:" << x << "y:" << y << "scale:" << scale;
}

