//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtWidgets/QApplication>
#include <QTextCodec>
//#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/AF_System.h"
#include "app/DrvBuf.h"

QT_USE_NAMESPACE

//-----------------------------------------------------------------------------
#if (GENERATED_STREAM == THERMO_STREAM_GEN)
	SysUtils::TAppSingleton AppSingleton("THERMO");
#elif (GENERATED_STREAM == VIDEO_STREAM_GEN)
	SysUtils::TAppSingleton AppSingleton("VIDEO");
#else
#endif

//-----------------------------------------------------------------------------
int main(int argc, char** argv)
{
    if(AppSingleton.isRunning()) {
        qDebug() << "Attempt to start the second App instance";
        return 0;
    }

    QApplication app(argc, argv);

    #if defined(Q_OS_WIN)
        if (QLocale::system().country() == QLocale::RussianFederation)
            QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM866"));
    #endif

    #if defined(USE_AF_SYSTEM_SINGLETON)
        TSystemAf& systemAf = TSystemAf::instance();
    #else
        TSystemAf systemAf;
    #endif
    systemAf.showUi();

    #if 0 /*TEST*/
        qDebug() << "TDrvBufUSB classId:" << SysUtils::TTypeEnumerator<TDrvBufUSB>::classId();
        qDebug() << "TRawBuf classId:" << SysUtils::TTypeEnumerator<TRawBuf>::classId();
    #endif

    #if 0 /*TEST*/
        TMsgWrapper<int,TMsgNoPoolPolicy,TRoutingPolicy,TMsgNotDeleted> slon1(new int(5));
        TMsgWrapper<int,TMsgNoPoolPolicy,TRoutingPolicy,TMsgNotDeleted> slon2(new int(7));
        int* msg1 = TBaseMsgWrapper<TMsgNoPoolPolicy,TRoutingPolicy>::checkMsg<int>(&slon1);
        int* msg2 = TBaseMsgWrapper<TMsgNoPoolPolicy,TRoutingPolicy>::checkMsg<int>(&slon2);

        qDebug() << "m(1)" << *msg1 << *msg2;
        slon1.msgCloneImpl(&slon2);
        qDebug() << "m(2)" << *msg1 << *msg2;
    #endif

    return app.exec();
}
