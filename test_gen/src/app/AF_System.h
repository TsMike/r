#if !defined(AF_SYSTEM_H)
#define AF_SYSTEM_H

#include <QtCore/QObject>

#include "app/CfgDefs.h"
#include "app/BufPool.h"

#if defined(USE_AF_SYSTEM_SINGLETON)
    #include "app/tsingleton.h"
#endif


//-----------------------------------------------------------------------------
class T_UI_Layer;
class TFrameFormerLayer;
class TDrvLayer;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#if defined(USE_AF_SYSTEM_SINGLETON)
    class TSystemAf : public QObject, public TSingleton<TSystemAf>
#else
    class TSystemAf : public QObject
#endif

{
 Q_OBJECT
 #if defined(USE_AF_SYSTEM_SINGLETON)
    friend class TSingleton<TSystemAf>;
 #endif

 public:
    #if !defined(USE_AF_SYSTEM_SINGLETON)
        TSystemAf();
    #endif
    ~TSystemAf();
	void showUi();
    TBufPool& getBufPool() { return mBufPool; }
	WId msgLoopWinId() const { return mMsgLoopWinId; }

 private:
    #if defined(USE_AF_SYSTEM_SINGLETON)
        TSystemAf();
    #endif
	T_UI_Layer*        mUiLayer;
    TFrameFormerLayer* mFrameFormerLayer;
    TDrvLayer*         mDrvLayer;
    TBufPool           mBufPool;
	WId                mMsgLoopWinId;

 private slots:
    void quitSlot();
};

#endif // AF_SYSTEM_H


