//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/DrvLayer.h"
#include "app/AF_System.h"
#include "app/udp_lib_adapter.h"


#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
static const unsigned ChannelNum = 3;
static const TChannelStreamGen::TParams StreamParams[ChannelNum] = {
	{
		CfgDefs::VideoStreamId,
		CfgDefs::VideoScreenFrameWidth,
		CfgDefs::VideoScreenFrameHeight,
		//&TChannelStreamGen::TVS_MovingVertLine
		#if (GENERATED_STREAM == THERMO_STREAM_GEN)
			&TChannelStreamGen::TVS_testFFT
		#elif (GENERATED_STREAM == VIDEO_STREAM_GEN)
			&TChannelStreamGen::TVS_MovingVertLine
		#else
		#endif
	},
	{
		CfgDefs::InfoStreamId,
		CfgDefs::InfoScreenFrameWidth,
		CfgDefs::InfoScreenFrameHeight,
		&TChannelStreamGen::IS_TestPattern
	},
	{
		CfgDefs::TestStreamId,
		CfgDefs::VideoScreenFrameWidth,
		CfgDefs::VideoScreenFrameHeight,
		&TChannelStreamGen::ZeroPattern
	}
};
static const unsigned WorkTime[ChannelNum] = { 1, 0, 0 };
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
CfgDefs::TNetAddr CfgDefs::getViewNetSrcFilter()
{
	return T_UDP_LibAdapter::makeNetAddr(inet_addr(CfgDefs::HostAddr),CfgDefs::IrChannelStreamPort);;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TDrvLayer::TDrvLayer(TSystemAf* systemAf) :
							   TThread(L"DrvLayerThread"),
							   mSystemAf(systemAf),
							   #if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
								mTestGenTimer(),
								mDevBundleStreamGen(),
							   #endif
							   mSemaphore()
{
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		CfgDefs::TNetAddr srcDev = T_UDP_LibAdapter::makeNetAddr(inet_addr(CfgDefs::HostAddr),CfgDefs::IrChannelStreamPort);
		CfgDefs::TNetAddr dstDev = T_UDP_LibAdapter::makeNetAddr(inet_addr(CfgDefs::IrChannelAddr),CfgDefs::IrChannelStreamPort);
		//printf("src IP: %x, src Port: %d\n",T_UDP_LibAdapter::getIpAddr(srcDev),T_UDP_LibAdapter::getPort(srcDev));

		mDevBundleStreamGen.insertDevGen(
										  srcDev,                         // srcDev
										  dstDev,                         // dstDev
										  mSystemAf->getBufPool().getPool<TDrvBufStreamETH::TDrvBufPool>(), // pool
										  128,                            // devWorkTime
										  StreamParams,                   // channelParams
										  WorkTime,                       // channelWorkTime
										  ChannelNum,                     // channelNum
										  16*CfgDefs::EthStreamPacketSize // mChunkLen (default: 8192)
										 );

		mTestGenTimer.setTimerType(Qt::PreciseTimer);
		connect(&mTestGenTimer,&QTimer::timeout,this,&TDrvLayer::testGenSlot);
		mTestGenTimer.start(CfgDefs::TestDrvBufPeriod);
	#endif

	#if 0
		T_UDP_RxChannel::TParams rxParams = {
												{
													CfgDefs::EthStreamPacketSize,      // netPacketSize
													CfgDefs::EthStreamPacketsInBuf,    // numPacketsInBundle
													128,                               // numBundles
													THREAD_PRIORITY_NORMAL,            // threadPriority
													2000,                              // timeout
													8*1024*1024,                       // socketBufSize
													inet_addr(CfgDefs::IrChannelAddr), // peerAddr
													CfgDefs::IrChannelStreamPort,        // peerPort
													T_UDP_LibAdapter::rxNotify         // onTransferReady
												},
												mSystemAf->getBufPool().getPool<TDrvBufStreamETH::TDrvBufPool>()		 // msgBufPool
											};
	#endif
	#if 1
		T_UDP_TxChannel::TParams txParams = {
												{
													CfgDefs::EthStreamPacketSize,      // netPacketSize
                                                    CfgDefs::EthStreamPacketsInBuf,    // numPacketsInBundle
													128,                               // numBundles
													THREAD_PRIORITY_HIGHEST,           // threadPriority
													2000,                              // timeout
													8*1024*1024,                       // socketBufSize
													inet_addr(CfgDefs::IrChannelAddr), // peerAddr
													CfgDefs::IrChannelStreamPort,        // peerPort
													T_UDP_LibAdapter::txNotify         // onTransferReady
												},
												16                                     // txQueueLimit
											};
		T_UDP_LibAdapter::init();
    #endif
    #if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		connect(this,&TDrvLayer::sendDrvBufAuxSignal,this,&TDrvLayer::sendDrvBufSignal);
		connect(this,&TDrvLayer::sendDrvBufAuxSignal,T_UDP_LibAdapter::getInstance(),&T_UDP_LibAdapter::receiveMsgSlot);
    #endif
	#if (WORK_MODE == WORK_APPLICATION)
        connect(T_UDP_LibAdapter::getInstance(),&T_UDP_LibAdapter::sendMsgSignal,this,&TDrvLayer::sendDrvBufSignal);
    #endif
		if(T_UDP_LibAdapter::createSocket(inet_addr(CfgDefs::HostAddr),CfgDefs::IrChannelStreamPort,/*&rxParams*/0,&txParams) == UDP_LIB::Ok) {
            qDebug() << "[INFO] IrTestPort socket created";
        }
	#if 1
		qDebug() << qPrintable(QString("[DrvLayer]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
	#endif
}

//-----------------------------------------------------------------------------
TDrvLayer::~TDrvLayer()
{
	threadFinish();
	#if 1
		T_UDP_LibAdapter::cleanUp();
	#endif

	#if 1
		qDebug() << qPrintable(QString("[DrvLayer]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
	#endif
}

//-----------------------------------------------------------------------------
void TDrvLayer::activate()
{
	#if (WORK_MODE == WORK_APPLICATION)
	#else
		start(QThread::InheritPriority);
	#endif
}

//-----------------------------------------------------------------------------
bool TDrvLayer::onExec()
{
	mSemaphore.acquire();
	if(threadExit())
		return threadExit();

	#if 0
		qDebug() << qPrintable(QString("[DrvLayer]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("onExec()").leftJustified(SysUtils::JobNameJustify));
	#endif

	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		TDrvBufPtr drvBufPtr = mDevBundleStreamGen.genDrvBuf();
		sendDrvBufAuxSignal(drvBufPtr);

		#if 0
		TDrvBufPtr drvBufPtr;
		if(getDrvBuf(drvBufPtr)) {
			mDevBundleStreamGen.fillDrvBuf(drvBufPtr);
			sendDrvBufSignal(drvBufPtr);
		}
		#endif
	#endif

	return threadExit();
}

//-----------------------------------------------------------------------------
bool TDrvLayer::getDrvBuf(TDrvBufPtr& drvBufPtr)
{
	return mSystemAf->getBufPool().getBuf<TDrvBufUSB::TDrvBufPool>(drvBufPtr);
}
