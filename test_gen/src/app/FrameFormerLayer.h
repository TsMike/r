#if !defined(FRAME_FORMER_LAYER_H)
#define FRAME_FORMER_LAYER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/tthread.h"
#include "app/Frame.h"
#include "app/DrvBuf.h"
#include "app/StreamParser.h"
#include "app/StreamSwitch.h"

#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
    #include <QtCore/QTimer>
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TSystemAf;
class TBaseFrameFormer;

class TFrameFormerLayer : public TThread
{
 Q_OBJECT

 public:

 signals:
    void sendFrameSignal(TScreenFramePtr);

 public slots:
    void receiveDrvBufSlot(TDrvBufPtr);

 protected slots:
    #if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
        void testGenSlot() { mSemaphore.release(); }
    #endif

 protected:
    virtual bool onExec();
    void threadFinish() { setThreadExit(); mSemaphore.release();  TThread::threadFinish(); }

 public:
    explicit TFrameFormerLayer(TSystemAf* systemAf);
    ~TFrameFormerLayer();

 private:
    TSystemAf*           mSystemAf;
    #if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
        QTimer mTestGenTimer;
        bool genTestFrame(TScreenFramePtr&);
    #endif
    QSemaphore            mSemaphore;
    TMsgWrapperPoolQueue  mDrvBufQueue;
    TDevBundleParser      mDevBundleParser;
    TStreamSwitch         mStreamSwitch;
	TBaseFrameFormer*     mVideoFrameFormer;
	TBaseFrameFormer*     mInfoFrameFormer;
	#if defined(MATH_IP_PIPE_TX)
		TMsgWrapperPoolQueue*  mMathIpPipeTxQueue;
	#endif
	TMsgWrapperPoolQueue  mScreenFrameQueue;

    bool getFrame(TScreenFramePtr& testFrame);
	#if (WORK_MODE != TESTGEN_AT_FRAME_FORMER_LAYER)
		void frameFilter(TScreenFramePtr& framePtr);
	#endif

    #if defined(STREAM_SWITCH_TEST)
        void streamSwitchTest();
    #endif
};

#endif // FRAME_FORMER_LAYER_H


