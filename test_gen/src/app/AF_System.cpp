//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#include <QtCore/qmath.h>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtGui/QResizeEvent>
#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/AF_System.h"
#include "app/UI_Layer.h"
#include "app/FrameFormerLayer.h"
#include "app/DrvLayer.h"

#if defined(USE_AF_SYSTEM_SINGLETON)
    #include <QtCore/QCoreApplication>
#endif


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TSystemAf::TSystemAf() : QObject(),
                         mUiLayer(0),
						 mFrameFormerLayer(0),
						 mDrvLayer(0),
						 mBufPool(),
						 mMsgLoopWinId(0)

{
    //---
    mBufPool.insertPool(new TDrvBufUSB::TDrvBufPool(CfgDefs::DrvBufNum,TDrvBufUSB::TCreator()));
    mBufPool.insertPool(new TVideoScreenFrame::TFramePool(CfgDefs::VideoScreenFrameNum,TVideoScreenFrame::TCreator()));
    mBufPool.insertPool(new TInfoScreenFrame::TFramePool(CfgDefs::InfoScreenFrameNum,TInfoScreenFrame::TCreator()));
    mBufPool.insertPool(new TVideoRawFrame::TFramePool(CfgDefs::VideoRawFrameNum,TVideoRawFrame::TCreator()));
    mBufPool.insertPool(new TInfoRawFrame::TFramePool(CfgDefs::InfoRawFrameNum,TInfoRawFrame::TCreator()));
	mBufPool.insertPool(new TDrvBufStreamETH::TDrvBufPool(CfgDefs::EthStreamBufNum,TDrvBufStreamETH::TCreator()));

    #if 1
        qDebug() << "\n*** class Id's (1) ***";
		qDebug() << "TDrvBufUSB::TDrvBufPool classId:       " << SysUtils::TTypeEnumerator<TDrvBufUSB::TDrvBufPool>::classId();
		qDebug() << "TVideoScreenFrame::TFramePool classId: " << SysUtils::TTypeEnumerator<TVideoScreenFrame::TFramePool>::classId();
		qDebug() << "TInfoScreenFrame::TFramePool classId:  " << SysUtils::TTypeEnumerator<TInfoScreenFrame::TFramePool>::classId();
		qDebug() << "TVideoRawFrame::TFramePool classId:    " << SysUtils::TTypeEnumerator<TVideoRawFrame::TFramePool>::classId();
		qDebug() << "TInfoRawFrame::TFramePool classId:     " << SysUtils::TTypeEnumerator<TInfoRawFrame::TFramePool>::classId();
		qDebug() << "TDrvBufStreamETH::TDrvBufPool classId: " << SysUtils::TTypeEnumerator<TDrvBufStreamETH::TDrvBufPool>::classId();

        qDebug() << "\n*** class Id's (2) ***";
        qDebug() << "TDrvBufUSB classId:       " << SysUtils::TTypeEnumerator<TDrvBufUSB>::classId();
        qDebug() << "TVideoScreenFrame classId:" << SysUtils::TTypeEnumerator<TVideoScreenFrame>::classId();
        qDebug() << "TInfoScreenFrame classId: " << SysUtils::TTypeEnumerator<TInfoScreenFrame>::classId();
        qDebug() << "TVideoRawFrame classId:   " << SysUtils::TTypeEnumerator<TVideoRawFrame>::classId();
        qDebug() << "TInfoRawFrame classId:    " << SysUtils::TTypeEnumerator<TInfoRawFrame>::classId();
		qDebug() << "TDrvBufStreamETH classId: " << SysUtils::TTypeEnumerator<TDrvBufStreamETH>::classId();

        qDebug() << "\n*** class Id's (3) ***";
        qDebug() << "TRawBuf classId:          " << SysUtils::TTypeEnumerator<TRawBuf>::classId();
        qDebug() << "TBaseFrame classId:       " << SysUtils::TTypeEnumerator<TBaseFrame>::classId();
    #endif

    //---
    qRegisterMetaType<TDrvBufPtr>("TDrvBufPtr");
    qRegisterMetaType<TScreenFramePtr>("TScreenFramePtr");


    //---
	mUiLayer          = new T_UI_Layer(this);
	mMsgLoopWinId     = mUiLayer->winId();
	mFrameFormerLayer = new TFrameFormerLayer(this);
    mDrvLayer         = new TDrvLayer(this);

    //---
	connect(mDrvLayer,&TDrvLayer::sendDrvBufSignal,mFrameFormerLayer,&TFrameFormerLayer::receiveDrvBufSlot,Qt::DirectConnection);
	connect(mFrameFormerLayer,&TFrameFormerLayer::sendFrameSignal,mUiLayer,&T_UI_Layer::receiveFrameSlot,Qt::QueuedConnection);

    //---
	mFrameFormerLayer->start(QThread::InheritPriority);
	//mDrvLayer->start(QThread::InheritPriority);
	mDrvLayer->activate();

    //---
    #if defined(USE_AF_SYSTEM_SINGLETON)
        connect(QCoreApplication::instance(),&QCoreApplication::aboutToQuit,this,&TSystemAf::quitSlot);
    #endif
}

//-----------------------------------------------------------------------------
TSystemAf::~TSystemAf()
{
    quitSlot();
}

//-----------------------------------------------------------------------------
void TSystemAf::showUi()
{
    mUiLayer->show();
	//mUiLayer->showMinimized();
}

//-----------------------------------------------------------------------------
void TSystemAf::quitSlot()
{
    //---
    delete mDrvLayer;
    mDrvLayer = 0;
    delete mFrameFormerLayer;
    mFrameFormerLayer = 0;
    delete mUiLayer;
    mUiLayer = 0;
}
