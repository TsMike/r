//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//#include <QtWidgets/QApplication>
#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"

#if defined(_WIN32)
	#include <windows.h>
#endif
#include "app/udp_lib_adapter.h"

//#include <QtCore/QElapsedTimer> /*DEBUG*/

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void TStreamTester::fillBuf(uint8_t* buf, unsigned len)
{
	len /= sizeof(TData);
	TData* dataBuf = reinterpret_cast<TData*>(buf);
	for(unsigned i = 0; i < len; ++i) {
		*dataBuf++ = mValue;
		mValue += mIncrement;
	}
}

//------------------------------------------------------------------------------
unsigned TStreamTester::checkBuf(uint8_t* buf, unsigned len)
{
	len /= sizeof(TData);
	TData* dataBuf = reinterpret_cast<TData*>(buf);
	unsigned errNum = 0;
	for(unsigned i = 0; i < len; ++i,++dataBuf) {
		if(*dataBuf != mValue) {
			++errNum;
            #if 0
                TData delta = (TData)(abs((int64_t)(mValue)-(int64_t)(*dataBuf)));
                printf("[DATA CHECK ERROR] expected: %10u, received: %10u, delta: %10u, errNum: %5d\n",mValue,*dataBuf,delta,errNum);
            #endif
			mValue = *dataBuf;
		}
		mValue += mIncrement;
	}
	return errNum;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
unsigned T_UDP_RxChannel::getTransferLen(const TRawBuf* buf, const UDP_LIB::Transfer& transfer) const
{
	if(transfer.direction != UDP_LIB::Receive) {
		return 0;
	}
	if(transfer.length <= static_cast<int>(buf->bufSize<T_UDP_LibAdapter::TEthData>())) {
		if(transfer.isStream && (transfer.length != transfer.bufLength)) {
			return 0;
		}
		return transfer.length;
	}
	return 0;
}

//-----------------------------------------------------------------------------
void T_UDP_RxChannel::receiveData()
{
	UDP_LIB::Transfer transfer;
	if(UDP_LIB::tryGetTransfer(mHostAddr,mHostPort,UDP_LIB::Receive,transfer) == UDP_LIB::Ok) {
        TBaseMsgWrapperPtr msg;
		TRawBuf* buf;
		mParams.msgBufPool->get(msg);
		if(msg && (buf = checkMsg<TRawBuf>(msg))) {
			unsigned transferLen = getTransferLen(buf,transfer);
            if(transferLen) {
				T_UDP_LibAdapter::TEthData* src = transfer.buf;
				#if defined(TEST_UDP_STREAM)
                    unsigned errNum = mStreamTester.checkBuf(src,transferLen);
                    if(errNum) {
                        qDebug() << "[ERROR] [checkBuf]" << errNum << "errors in buf";
                    }
				#endif
				T_UDP_LibAdapter::TEthData* dst = buf->getDataBuf<T_UDP_LibAdapter::TEthData>();
				std::memcpy(dst,src,transferLen);
				buf->setDataLen<T_UDP_LibAdapter::TEthData>(transferLen);
				msg->setNetSrc(T_UDP_LibAdapter::makeNetAddr(mHostAddr,mHostPort));
				if(T_UDP_LibAdapter::isValid()) {
					T_UDP_LibAdapter::getInstance()->sendMsgSignal(msg);
                    //qDebug() << "[INFO] [T_UDP_RxChannel::receiveData] bundle" << transfer.bundleId << "received";
				} else {
                    qDebug() << "[ERROR] [T_UDP_RxChannel::receiveData] T_UDP_LibAdapter not valid";
				}
            } else {
				//qDebug() << "[WARN] [T_UDP_RxChannel::receiveData] zero transfer len";
            }
		} else {
            qDebug() << "[ERROR] [T_UDP_RxChannel::receiveData] bad msg or bufmsg";
		}
		UDP_LIB::submitTransfer(mHostAddr,mHostPort,UDP_LIB::Receive,transfer);
	} else {
        qDebug() << "[ERROR] [T_UDP_RxChannel::receiveData] tryGetTransfer error";
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
CfgDefs::TNetAddr T_UDP_TxChannel::getPeerNetAddr() const
{
	return T_UDP_LibAdapter::makeNetAddr(mParams.params.peerAddr,mParams.params.peerPort);
}

//-----------------------------------------------------------------------------
bool T_UDP_TxChannel::putToQueue(TBaseMsgWrapperPtr msg)
{
	if(!msg) {
		return false;
	}
	if(mParams.txQueueLimit && (mMsgQueue.size() > mParams.txQueueLimit)) {
		mMsgQueue.get(msg); // ???
		return false;
	} else {
		mMsgQueue.put(msg);
		return true;
	}
}

//-----------------------------------------------------------------------------
unsigned T_UDP_TxChannel::getTransferLen(TRawBuf* buf) const
{
	unsigned hostMsgDataLen =  buf->dataLen<T_UDP_LibAdapter::TEthData>();
	unsigned udpLibBufLen   = mParams.params.netPacketSize*mParams.params.numPacketsInBundle;
	bool isUdpStream        = (mParams.params.numPacketsInBundle > 1);

	if(hostMsgDataLen <= udpLibBufLen) {
		if(isUdpStream && (hostMsgDataLen != udpLibBufLen)) {
			return 0;
		}
		return hostMsgDataLen;
	}
	return 0;
}

//-----------------------------------------------------------------------------
bool T_UDP_TxChannel::tryTransfer(int callerId)
{
	UDP_LIB::Transfer transfer;
	TBaseMsgWrapperPtr msg;
	TRawBuf* buf;

	while(1) {
        volatile TWinCsGuard::TLocker lock(mGuard);
		if(!mMsgQueue.empty() && UDP_LIB::getReadyTransferNum(mHostAddr,mHostPort,UDP_LIB::Transmit)) {
			mMsgQueue.get(msg);
            buf = checkMsg<TRawBuf>(msg);
			if(buf) {
				unsigned transferLen = getTransferLen(buf);
				if(transferLen && (UDP_LIB::tryGetTransfer(mHostAddr,mHostPort,UDP_LIB::Transmit,transfer) == UDP_LIB::Ok)) {
					T_UDP_LibAdapter::TEthData* dst = transfer.buf;
					#if !defined(TEST_UDP_STREAM)
						T_UDP_LibAdapter::TEthData* src = buf->getDataBuf<T_UDP_LibAdapter::TEthData>();
						std::memcpy(dst,src,transferLen);
					#else
						mStreamGen.fillBuf(dst,transferLen);
					#endif
                    transfer.length = transferLen; // not present in original RTestGen !!!
					if(UDP_LIB::submitTransfer(mHostAddr,mHostPort,UDP_LIB::Transmit,transfer) == UDP_LIB::Ok) {
                        //printf("[SLON] UDP_LIB::submitTransfer %x %d %d %d %d\n",mHostAddr,mHostPort,transfer.length,transfer.bufLength,transferLen);
						#if 0
                            if(callerId) {
								qDebug() << "[INFO] [T_UDP_TxChannel::tryTransfer]" << callerId;
							}
						#endif
					} else {
						qDebug() << "[ERROR] [T_UDP_TxChannel::tryTransfer]"  << callerId;
						return false;
					}
				}
			} else {
				qDebug() << "[ERROR] [T_UDP_TxChannel::tryTransfer]";
			}
		} else {
			break;
		}
	}
	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
T_UDP_LibAdapter* T_UDP_LibAdapter::mInstance = 0;

static const unsigned IpOffset   = 32;
static const unsigned IpMask     = 0xFFFFFFFF;
static const unsigned PortOffset = 16;
static const unsigned PortMask   = 0xFFFF;

//-----------------------------------------------------------------------------
CfgDefs::TNetAddr T_UDP_LibAdapter::makeNetAddr(unsigned long ipAddr, unsigned portAddr)
{
	CfgDefs::NetAddrWordType NetAddrIP   = static_cast<CfgDefs::NetAddrWordType>(ipAddr & IpMask) << IpOffset;
	CfgDefs::NetAddrWordType NetAddrPort = static_cast<CfgDefs::NetAddrWordType>(portAddr & PortMask) << PortOffset;

	return CfgDefs::TNetAddr(NetAddrIP | NetAddrPort);
}

//-----------------------------------------------------------------------------
CfgDefs::TNetAddr T_UDP_LibAdapter::makeNetAddr(const UDP_LIB::TNetAddr& netAddr)
{
	return makeNetAddr(netAddr.ipAddr, netAddr.port);
}


//-----------------------------------------------------------------------------
unsigned long T_UDP_LibAdapter::getIpAddr(CfgDefs::TNetAddr netAddr)
{
	return (netAddr >> IpOffset) & IpMask;
}

//-----------------------------------------------------------------------------
unsigned T_UDP_LibAdapter::getPort(CfgDefs::TNetAddr netAddr)
{
	return (netAddr >> PortOffset) & PortMask;
}


//-----------------------------------------------------------------------------
bool T_UDP_LibAdapter::init()
{
	mInstance = new T_UDP_LibAdapter;
	#if 1
		if(isValid()) {
			qDebug() << "[INFO] [UDP_LibAdapter::init] ok";
		} else {
			qDebug() << "[ERROR] [UDP_LibAdapter::init]";
		}
	#endif
	return isValid();
}

//-----------------------------------------------------------------------------
void T_UDP_LibAdapter::cleanUp()
{
	delete mInstance;
	qDebug() << "[INFO] [UDP_LibAdapter::cleanUp]";
}

//-----------------------------------------------------------------------------
T_UDP_LibAdapter::~T_UDP_LibAdapter()
{
	//---
	mValid = false;
	UDP_LIB::cleanUp();

	//--- Tx channels
	T_USB_TxChannelMap& txChannelMap = getInstance()->mTxChannelMap;
	for(T_USB_TxChannelMap::iterator txChannel = txChannelMap.begin(); txChannel != txChannelMap.end(); ++txChannel) {
		delete txChannel.value();
	}
	txChannelMap.clear();

	//--- Rx channels
	T_USB_RxChannelMap& rxChannelMap = getInstance()->mRxChannelMap;
	for(T_USB_RxChannelMap::iterator rxChannel = rxChannelMap.begin(); rxChannel != rxChannelMap.end(); ++rxChannel) {
		delete rxChannel.value();
	}
	rxChannelMap.clear();

}

//-----------------------------------------------------------------------------
T_UDP_RxChannel* T_UDP_LibAdapter::getRxChannel(CfgDefs::TNetAddr srcAddr)
{
	T_USB_RxChannelMap::iterator rxChannelIter = mRxChannelMap.find(srcAddr);
	if(rxChannelIter != mRxChannelMap.end()) {
		return rxChannelIter.value();
	}
	return 0;
}

//-----------------------------------------------------------------------------
T_UDP_TxChannel* T_UDP_LibAdapter::getTxChannel(CfgDefs::TNetAddr srcAddr, CfgDefs::TNetAddr dstAddr)
{
	T_USB_TxChannelMap::iterator txChannelIter = mTxChannelMap.find(srcAddr);
	if((txChannelIter != mTxChannelMap.end()) && (txChannelIter.value()->getPeerNetAddr() == dstAddr)) {
		return txChannelIter.value();
	}
	return 0;
}

//-----------------------------------------------------------------------------
UDP_LIB::TStatus T_UDP_LibAdapter::createSocket(unsigned long hostAddr, unsigned hostPort, const T_UDP_RxChannel::TParams* rxParams, const T_UDP_TxChannel::TParams* txParams)
{
	//---
	if(!isValid()) {
		return UDP_LIB::NotInitialized;
	}

	//---
    UDP_LIB::TStatus status = UDP_LIB::createSocket(hostAddr,hostPort,&(rxParams->params),&(txParams->params));
	if(status != UDP_LIB::Ok) {
		return status;
	}

	//---
	if(rxParams) {
		CfgDefs::TNetAddr netAddr = makeNetAddr(hostAddr,hostPort);
		T_USB_RxChannelMap& rxChannelMap = getInstance()->mRxChannelMap;
		T_USB_RxChannelMap::iterator rxChannelIter = rxChannelMap.find(netAddr);
		if(rxChannelIter == rxChannelMap.end()) {
			rxChannelMap.insert(netAddr,new T_UDP_RxChannel(hostAddr,hostPort,rxParams));
		} else {
			qDebug() << "[ERROR] [T_UDP_LibAdapter::createSocket] Rx";
			return UDP_LIB::SocketCreationError;
		}
	}


	//---
	if(txParams) {
		CfgDefs::TNetAddr netAddr = makeNetAddr(hostAddr,hostPort);
		T_USB_TxChannelMap& txChannelMap = getInstance()->mTxChannelMap;
		T_USB_TxChannelMap::iterator txChannelIter = txChannelMap.find(netAddr);
		if(txChannelIter == txChannelMap.end()) {
			txChannelMap.insert(netAddr,new T_UDP_TxChannel(hostAddr,hostPort,txParams));
		} else {
			qDebug() << "[ERROR] [T_UDP_LibAdapter::createSocket] Tx";
			return UDP_LIB::SocketCreationError;
		}
	}

	return UDP_LIB::Ok;
}

//-----------------------------------------------------------------------------
void T_UDP_LibAdapter::receiveMsgSlot(TBaseMsgWrapperPtr msg)
{
	if(!msg)
		return;
	if(!isValid()) {
		qDebug() << "[receiveMsgSlot] not valid!";
		return;
	}
    T_UDP_TxChannel* txChannel = getTxChannel(msg->netSrc(),msg->netDst());

	if(txChannel && txChannel->putToQueue(msg)) {
        //printf("[1] %08x %5d %08x %5d\n",getIpAddr(msg->netSrc()),getPort(msg->netSrc()),getIpAddr(msg->netDst()),getPort(msg->netDst()));
		txChannel->tryTransfer(1);
	}
}

//-----------------------------------------------------------------------------
void T_UDP_LibAdapter::rxNotify(const UDP_LIB::TNetAddr& hostAddr, const UDP_LIB::TNetAddr&, UDP_LIB::TDirection)
{
	if(isValid()) {
		//printf("[2] %08x %5d\n",hostAddr.ipAddr, hostAddr.port);
		T_UDP_RxChannel* rxChannel = getInstance()->getRxChannel(makeNetAddr(hostAddr));
		if(rxChannel) {
			rxChannel->receiveData();
		}
	} else {
		qDebug() << "[rxNotify] not valid!";
	}
}

//-----------------------------------------------------------------------------
void T_UDP_LibAdapter::txNotify(const UDP_LIB::TNetAddr& hostAddr, const UDP_LIB::TNetAddr& peerAddr, UDP_LIB::TDirection)
{
	if(isValid()) {
        //printf("[2] %08x %5d %08x %5d\n",hostAddr.ipAddr, hostAddr.port, peerAddr.ipAddr, peerAddr.port);
		T_UDP_TxChannel* txChannel = getInstance()->getTxChannel(makeNetAddr(hostAddr),makeNetAddr(peerAddr));
		if(txChannel) {
			txChannel->tryTransfer(2);
		}
	} else {
		qDebug() << "[txNotify] not valid!";
	}
}
