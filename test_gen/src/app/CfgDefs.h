#if !defined(CFG_DEFS_H)
#define CFG_DEFS_H

#include <cstdint>

#define THERMO_STREAM_GEN 0
#define VIDEO_STREAM_GEN  1

#define GENERATED_STREAM VIDEO_STREAM_GEN

#if (GENERATED_STREAM == THERMO_STREAM_GEN)
	static const int ScreenFrameWidth  = 640;
	static const int ScreenFrameHeight = 480;
#elif (GENERATED_STREAM == VIDEO_STREAM_GEN)
	static const int ScreenFrameWidth  = 1280;
	static const int ScreenFrameHeight = 960;
#else
#endif


#define WORK_APPLICATION              0
#define TESTGEN_AT_FRAME_FORMER_LAYER 1
#define TESTGEN_AT_DRV_LAYER          2

#define WORK_MODE TESTGEN_AT_DRV_LAYER	// CONFIG

//#define MATH_IP_PIPE_TX                  // CONFIG

//*** test settings section ***************************************************

//#define USE_AF_SYSTEM_SINGLETON    // !!! program crashed when finished

#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
	#define MOVING_BANDS 1
	#define MOVING_CROSS 2

	#define PIXGEN_FUNC MOVING_CROSS	// CONFIG
	//#define FRAME_RESIZE_TEST			// CONFIG - work only when (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
#endif

#define MSG_SELF_RELEASE				// CONFIG

//*** test settings section (end) *********************************************

namespace CfgDefs {

//-----------------------------------------------------------------------------
typedef int64_t NetAddrWordType;
struct NetAddrType
{
	NetAddrType(NetAddrWordType nAddr = -1) : netAddr(nAddr) {}
	operator NetAddrWordType() const { return netAddr; }

	NetAddrWordType netAddr;
};
inline bool operator<(const NetAddrType& el1, const NetAddrType& el2) {
	return el1.netAddr < el2.netAddr;
}

//typedef NetAddrWordType TNetAddr;
typedef NetAddrType TNetAddr;
static TNetAddr NetNoAddr() { return TNetAddr(-1); }
//-----------------------------------------------------------------------------

static const int VideoRawFrameNum       = 32;
static const int VideoScreenFrameNum    = 4;
static const int VideoScreenFrameWidth  = ScreenFrameWidth;
static const int VideoScreenFrameHeight = ScreenFrameHeight;

static const int InfoRawFrameNum        = 16;
static const int InfoScreenFrameNum     = 4;
static const int InfoScreenFrameWidth   = ScreenFrameWidth;
static const int InfoScreenFrameHeight  = ScreenFrameHeight;

static const int MainFrameWidth         = qMax(VideoScreenFrameWidth,InfoScreenFrameWidth);
static const int MainFrameHeight        = qMax(VideoScreenFrameHeight,InfoScreenFrameHeight);

static const int DrvBufNum              = 16;
static const int DrvBufSize             = 32768;

static const int EthStreamBufNum        = 512;
static const int EthStreamPacketSize    = 1472;
static const int EthStreamPacketsInBuf  = 64;
static const int EthStreamBufSize       = EthStreamPacketSize*EthStreamPacketsInBuf;


#if (GENERATED_STREAM == THERMO_STREAM_GEN)
    static const char* HostAddr             = "192.168.10.2";
    static const char* IrChannelAddr        = "192.168.10.1";
	static const unsigned IrChannelStreamPort = 50002;
#elif (GENERATED_STREAM == VIDEO_STREAM_GEN)
    static const char* HostAddr             = "192.168.10.2"; // old value 192.168.10.3
	static const char* IrChannelAddr        = "192.168.10.1";
    static const unsigned IrChannelStreamPort = 50000;        // old value 50012
#else
#endif

static const unsigned VideoStreamId     = 1;
static const unsigned InfoStreamId      = 2;
static const unsigned TestStreamId      = 3;

//*** test settings section ***************************************************
extern TNetAddr getViewNetSrcFilter();

//-----------------------------------------------------------------------------
#if (WORK_MODE == WORK_APPLICATION)
	static const TNetAddr ViewNetSrcFilter = getViewNetSrcFilter(); // CONFIG
	static const TNetAddr MathNetSrcFilter = TNetAddr(0);           // CONFIG
#endif

//-----------------------------------------------------------------------------
#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
	static const int TestFramePeriod       = 40;	        // CONFIG
	static const TNetAddr MathNetSrcFilter = TNetAddr(100); // CONFIG
#endif

//-----------------------------------------------------------------------------
#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
	static const int TestDrvBufPeriod      = 1;	        // CONFIG

	static const TNetAddr ViewNetSrcFilter = getViewNetSrcFilter(); // CONFIG
	static const TNetAddr MathNetSrcFilter = TNetAddr(0);           // CONFIG

	//#define STREAM_SWITCH_TEST			                // CONFIG
#endif

//*** test settings section (end) *********************************************

}

#endif // CFG_DEFS_H


