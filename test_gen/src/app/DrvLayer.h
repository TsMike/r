#if !defined(DRV_LAYER_H)
#define DRV_LAYER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"

#include "app/tthread.h"
#include "app/DrvBuf.h"

#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
	#include <QtCore/QTimer>
	#include "app/TestStreamGen.h"
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TSystemAf;

class TDrvLayer : public TThread
{
 Q_OBJECT

 signals:
	void sendDrvBufSignal(TDrvBufPtr);
	void sendDrvBufAuxSignal(TDrvBufPtr);

 protected slots:
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		void testGenSlot() { mSemaphore.release(); }
	#endif

 protected:
	virtual bool onExec();
	void threadFinish() { setThreadExit(); mSemaphore.release();  TThread::threadFinish(); }

 public:
	explicit TDrvLayer(TSystemAf* systemAf);
	~TDrvLayer();
	void activate();

 private:
	bool getDrvBuf(TDrvBufPtr& drvBufPtr);

	TSystemAf*   mSystemAf;
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		QTimer mTestGenTimer;
		TDevBundleStreamGen mDevBundleStreamGen;
	#endif
	QSemaphore mSemaphore;
};

#endif // DRV_LAYER_H


