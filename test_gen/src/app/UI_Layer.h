#if !defined(UI_LAYER_H)
#define UI_LAYER_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>

//#include <QPaintEvent> /*TEST*/

#include "app/Frame.h"
#include "app/RawBuf.h"

//-----------------------------------------------------------------------------
class TGraphicsScene;
class TGraphicsView;
class TSystemAf;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class T_UI_Layer : public QMainWindow
{
 Q_OBJECT

 public:
    explicit T_UI_Layer(TSystemAf* systemAf);
    ~T_UI_Layer();
    QGraphicsPixmapItem* getVideoPixmap() { return mVideoStreamPixmap; }

 public slots:
   void receiveFrameSlot(TScreenFramePtr);

 private:
    void createMainScene();
	QGraphicsPixmapItem*  initPixmapItem(unsigned width, unsigned height, QColor color, qreal zValue);
	void showFrame(TScreenFramePtr& framePtr, QGraphicsPixmapItem* pixmapItem);

 signals:

 private:
    TSystemAf*           mSystemAf;
    TGraphicsScene*      mMainScene;
    TGraphicsView*       mMainView;
    QGraphicsPixmapItem* mVideoStreamPixmap;
	QGraphicsPixmapItem* mInfoStreamPixmap;
	QElapsedTimer        mTimer;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TGraphicsScene : public QGraphicsScene
{
 Q_OBJECT

 public:
    TGraphicsScene(QObject* parent = 0);
    ~TGraphicsScene();

 private:

 public slots:

 signals:

 private:
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TGraphicsView : public QGraphicsView
{
 Q_OBJECT

 protected:
    //void paintEvent(QPaintEvent* event) { /*QGraphicsView::paintEvent(event); */qDebug() << "slon" << event->rect() << event->region(); }

 public:
    TGraphicsView(QGraphicsScene* scene, T_UI_Layer* manager);
    ~TGraphicsView();
    virtual void resizeEvent(QResizeEvent* event);
    /*TEST*/ void testViewRelativeCrd(QSize size, qreal scale);

    private:
        T_UI_Layer* mManager;
};

#endif // UI_LAYER_H


