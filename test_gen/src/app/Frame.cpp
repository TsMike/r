//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/Frame.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
QVector<QRgb>& TQtImageFormat<QImage::Format_Indexed8>::frameColorTable()
{
	static bool colorTableInit = false;
	static QVector<QRgb> ColorTable(ColorNum);

	if(!colorTableInit) {
		for(int i=0; i < ColorNum; ++i) {
			ColorTable[i] = qRgb(i,i,i);
		}
		colorTableInit = true;
	}
	return ColorTable;
}
