//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/FrameFormer.h"


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
int TBaseFrameFormer::formFrames()
{
	if(!mInQueue)
		return 0;

	int nFrames = 0;
	while(mInQueue->size()) {
		TBaseMsgWrapperPtr inFramePtr;
		mInQueue->get(inFramePtr);
		if(mFramePool && mOutQueue) {
			TBaseMsgWrapperPtr outFramePtr;
			if(mFramePool->get(outFramePtr)) {
				TBaseFrame* inFrame  = checkMsg<TBaseFrame>(inFramePtr);
				TBaseFrame* outFrame = checkMsg<TBaseFrame>(outFramePtr);
				if(checkFrameCompatibility(inFrame,outFrame)) {
					if(formFrame(inFrame, outFrame)) {
						outFramePtr->setNetPoints(inFramePtr->netSrc(),inFramePtr->netDst());
						outFramePtr->setMsgId(inFramePtr->msgId());
						mOutQueue->put(outFramePtr);
						++nFrames;
					} else {
						#if !defined(MSG_SELF_RELEASE)
							releaseMsg(outFramePtr);
						#endif
					}
				} else {
					#if !defined(MSG_SELF_RELEASE)
						releaseMsg(outFramePtr);
					#endif
				}
			}
		}
		#if !defined(MSG_SELF_RELEASE)
			releaseMsg(inFramePtr);
		#endif
	}
	return nFrames;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TVideoFrameFormer::formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame)
{
	const int FrameSize = inFrame->size();

	TRawFrame::TPixel* inPixelBuf  = inFrame->getPixelBuf<TRawFrame>();
	TScreenFrame::TPixel* outPixelBuf = outFrame->getPixelBuf<TScreenFrame>();
	if(!inPixelBuf || !outPixelBuf)
		return false;

	for(int n = 0; n < FrameSize; ++n) {
		TRawFrame::TPixel pixVal = *inPixelBuf++;
		#if !defined(MAGIC_SEVEN_TEST)
			pixVal = pixVal >> 2;
		#else
			pixVal = pixVal >> 8;
		#endif
		*outPixelBuf++ = qRgba(pixVal,pixVal,pixVal,255);
	}

	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

bool TInfoFrameFormer::PaletteInitialized = false;

const QRgb TInfoFrameFormer::BasePalette[BasePaletteSize] =
{
	qRgba(0x00,0x00,0x00,0x00), //  0: transparent
	qRgba(0xff,0x00,0x00,0xff), //  1: red
	qRgba(0x00,0xff,0x00,0xff), //  2: green
	qRgba(0x00,0x00,0xff,0xff), //  3: blue
	qRgba(0xff,0xff,0x00,0xff), //  4: yellow
	qRgba(0xff,0x00,0xff,0xff), //  5: magenta
	qRgba(0x00,0xff,0xff,0xff), //  6: cyan
	qRgba(0xff,0xff,0xff,0xff), //  7: white
	qRgba(0xff,0x7f,0x00,0xff), //  8: light brown
	qRgba(0xff,0x00,0x7f,0xff), //  9: dark pink
	qRgba(0x00,0x7f,0xff,0xff), // 10: dark blue
	qRgba(0x7f,0x00,0xff,0xff), // 11: purple
	qRgba(0x7f,0xff,0x00,0xff), // 12: acid
	qRgba(0x00,0xff,0x7f,0xff), // 13: light green
	qRgba(0x00,0x00,0x00,0xff), // 14: reserved
	qRgba(0x00,0x00,0x00,0xff)  // 15: reserved
};

QRgb TInfoFrameFormer::Palette[PaletteSize];

//-----------------------------------------------------------------------------
bool TInfoFrameFormer::formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame)
{
	const int FrameSize = inFrame->size();

	TRawFrame::TPixel* inPixelBuf  = inFrame->getPixelBuf<TRawFrame>();
	TScreenFrame::TPixel* outPixelBuf = outFrame->getPixelBuf<TScreenFrame>();
	if(!inPixelBuf || !outPixelBuf)
		return false;

	for(int n = 0; n < FrameSize; ++n) {
		unsigned colorIdx = *inPixelBuf++ & 0xFF;
		*outPixelBuf++ = Palette[colorIdx];
	}

	return true;
}

//-----------------------------------------------------------------------------
void TInfoFrameFormer::genPalette()
{
	for(unsigned idx = 0; idx < PaletteSize; ++idx) {
		QRgb baseColor = BasePalette[idx & ColorMask];
		unsigned intensity = (idx >> IntensityPos) & IntensityMask;
		unsigned alpha = genAlpha(idx);
		Palette[idx] = genColor(baseColor,intensity,alpha);
	}
}

//-----------------------------------------------------------------------------
unsigned TInfoFrameFormer::genAlpha(unsigned idx)
{
	if((idx & ColorMask) == 0)
		return 0x00;

	if(idx & AlphaMask)
		return 0x7f;

	return 0xff;
}

//-----------------------------------------------------------------------------
QRgb TInfoFrameFormer::genColor(QRgb baseColor, unsigned intensity, int alpha)
{
	if(intensity)
		intensity += 1;

	int red   = qRed(baseColor)*intensity/8;
	int green = qGreen(baseColor)*intensity/8;
	int blue  = qBlue(baseColor)*intensity/8;

	return qRgba(red,green,blue,alpha);
}
