//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/StreamParser.h"


//-----------------------------------------------------------------------------
#if 1
static int sumPix(TBaseMsgWrapperPtr msgPtr, int N = -1)
{
	if(!msgPtr)
		return -1;
	TBaseFrame* frame = checkMsg<TBaseFrame>(msgPtr);
	TRawFrame::TPixel* frameBuf = frame->getPixelBuf<TRawFrame>();

	if(N == -1) {
		N = frame->size();
	}
	int sum = 0;
	while(N--) {
		sum += (*frameBuf++);
	}
	return sum;
}
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void TChannelParser::TStatInfo::updateStat()
{
	if(!mStarted) {
		mStarted = true;
		mPacketTransferred = 0;
		mByteRate = 0.0;
		mPacketRate = 0.0;
		mTimer.start();
	} else {
		++mPacketTransferred;
		if(mTimer.hasExpired(InfoPeriod)) {
			TChannelParser::TNetAddr src = mChannelParser->currentNetSrc();
			TChannelParser::TChannelId channel = mChannelParser->channelId();
			double packetRate = ((double)mPacketTransferred)/mTimer.nsecsElapsed()*1e3;  // packets per us
			double byteRate  = packetRate*mChannelParser->outBufSize();                  // MegaBytes per s
			mTimer.start();
			mPacketTransferred = 0;
			bool printFlag = false;
			if(!SysUtils::isFuzzyEqual(packetRate,mPacketRate)) {
				mPacketRate = packetRate;
				printFlag = true;
			}
			if(!SysUtils::isFuzzyEqual(byteRate,mByteRate)) {
				mByteRate = byteRate;
				printFlag = true;
			}
			if(printFlag) {
				qDebug() << qPrintable(QString("src: %1, channel: %2, byteRate: %3 MB/s, packetRate: %4 pkt/s").arg(src,16/*8*/,16,QLatin1Char('0')).arg(channel,2).arg(mByteRate,4,'f',1).arg(mPacketRate*1e6,4,'f',1));
			}
		}
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TChannelParser::TChannelParser(TMsgWrapperPoolQueue* bufPool, TMsgWrapperPoolQueue* parsedBufQueue) :
																					mStatInfo(this),
																					mChannelId(-1),
																					mBufPool(bufPool),
																					mParsedBufQueue(parsedBufQueue),
																					mActiveBufPtr()
{
	resetParser();
}

//-----------------------------------------------------------------------------
bool TChannelParser::parseDataBlock(TBufData*& begin, TBufData* end, TNetAddr)
{
	while(begin != end) {
		//---
		if(TDevParser::isDFM(*begin)) {
			return false;
		}

		//---
		++begin;
	}
	return true;
}

//-----------------------------------------------------------------------------
void TChannelParser::resetParser()
{
	mCurrentNetSrc = CfgDefs::NetNoAddr();
	mBufPtr = mBufEnd = 0;
	#if !defined(MSG_SELF_RELEASE)
		if(mActiveBufPtr)
			releaseMsg(mActiveBufPtr);
	#endif
	mActiveBufPtr.clear(); // !!! TEST IT !!!
}

//-----------------------------------------------------------------------------
void TChannelParser::sendFilledBuf()
{
	mParsedBufQueue->put(mActiveBufPtr);
	mStatInfo.updateStat();
	//qDebug() << "slon" << outBufSize();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TBaseFrameParser::TBaseFrameParser(unsigned frameWidth, unsigned frameHeight, TMsgWrapperPoolQueue* bufPool, TMsgWrapperPoolQueue* parsedBufQueue) :
																TChannelParser(bufPool, parsedBufQueue),
																mFrameWidth(frameWidth),
																mFrameHeight(frameHeight),
																mSentFrameNum(0)
{
	resetParser();
}

//#define FRAME_PARSER_DEBUG_INFO
#define FRAME_PARSER_ERROR_INFO

//-----------------------------------------------------------------------------
bool TBaseFrameParser::getActiveBuf()
{
	if(mBufPool->get(mActiveBufPtr)) {
		TBaseFrame* buf = checkMsg<TBaseFrame>(mActiveBufPtr);
		if(buf) {
			mBufPtr = buf->getPixelBuf<TRawFrame>();
			mBufEnd   = mBufPtr + buf->size();
			return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TTestStreamParser::parseDataBlock(TBufData*& begin, TBufData* end, TNetAddr netSrc)
{
	mCurrentNetSrc = netSrc;
	if(!mActiveBufPtr) {
		if(!getActiveBuf()) {
            #if defined(FRAME_PARSER_ERROR_INFO)
				qDebug() << qPrintable(QString("[ERROR] getActiveBuf() (1) src: %1").arg(mCurrentNetSrc,8,16,QLatin1Char('0'))) << "channel:" << channelId();
			#endif
			resetParser();
			return true;
		}
	}
	while(begin != end) {
		TBufData currData = *begin;
		*mBufPtr++ = currData;
		if(mBufPtr == mBufEnd) {
			mActiveBufPtr->setNetSrc(mCurrentNetSrc);
			if(mParsedBufQueue) {
				#if defined(FRAME_PARSER_DEBUG_INFO)
					qDebug() << qPrintable(QString("[frame parsed][1] src: %1").arg(mCurrentNetSrc,8,16,QLatin1Char('0'))) << "channel:" << channelId() << "FrameNum:" << mSentFrameNum /* << "msg pool Id:" << mActiveBufPtr->msgPoolId() */;
				#endif
				mActiveBufPtr->setMsgId(mSentFrameNum++);
				sendFilledBuf();
			} else {
				#if !defined(MSG_SELF_RELEASE)
					releaseMsg(mActiveBufPtr);
				#endif
				mActiveBufPtr.clear();  // !!! TEST IT !!!
				#if defined(FRAME_PARSER_DEBUG_INFO)
					qDebug() << qPrintable(QString("[frame parsed][2] src: %1").arg(mCurrentNetSrc,8,16,QLatin1Char('0'))) << "channel:" << channelId();
				#endif
			}
			if(!getActiveBuf()) {
                #if defined(FRAME_PARSER_ERROR_INFO)
					qDebug() << qPrintable(QString("[ERROR] getActiveBuf() (2) src: %1").arg(mCurrentNetSrc,8,16,QLatin1Char('0'))) << "channel:" << channelId();
				#endif
				resetParser();
			}
		}
		++begin;
	}
	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TFrameParser::TFrameParser(unsigned frameWidth, unsigned frameHeight, TMsgWrapperPoolQueue* bufPool, TMsgWrapperPoolQueue* parsedBufQueue) :
																			TBaseFrameParser(frameWidth,frameHeight,bufPool,parsedBufQueue)
{
	resetParser();
}

//-----------------------------------------------------------------------------
void TFrameParser::resetParser()
{
	mParserState = StartWaitSync;
	mStatePhase  = Phase0;
	mExpectedFrameNum = 0;
	mExpectedLineNum  = 0;

	TChannelParser::resetParser();
}

//-----------------------------------------------------------------------------
bool TFrameParser::parseDataBlock(TBufData*& begin, TBufData* end, TNetAddr netSrc)
{
	mCurrentNetSrc = netSrc;
	while(begin != end) {
		TBufData currData = *begin;
		//---
		if(TDevParser::isDFM(currData)) {
			return false;
		}

		//---
		switch(mParserState) {
			//---
			case StartWaitSync:
				if(mStatePhase == Phase0) {
					if(currData == SOF1_Pattern) {
						mStatePhase = Phase1;
					}
				} else {
					mStatePhase = Phase0;
					if(currData <= SOF2_MaxValue) {
						mExpectedFrameNum = updateExpectedFrameNum(currData);
						mSyncCounter = SyncLength;
						mParserState = WaitSync;
						#if defined(FRAME_PARSER_DEBUG_INFO)
							qDebug() << "[FrameParser: StartWaitSync->WaitSync] src:" << mCurrentNetSrc << "channel:" << channelId();
						#endif
					}
				}
			break;

			//---
			case WaitSync:
				if(mStatePhase == Phase0) {
					if(currData == SOF1_Pattern) {
						mStatePhase = Phase1;
					}
				} else {
					mStatePhase = Phase0;
					if(currData == mExpectedFrameNum) {
						if(--mSyncCounter == 0) {

							mExpectedLineNum = 0;
							mParserState   = WorkFlow;
							mWorkFlowState = WaitLine;
							// --- ?! flush output frame queue
							//--- get new frame from pool, and get two pointers: mBufPtr(begin), mBufEnd(end)
							if(!getActiveBuf()) {
                                #if defined(FRAME_PARSER_ERROR_INFO)
									qDebug() << "[ERROR] getActiveBuf() (1)" << "Src:" << mCurrentNetSrc << "channel:" << channelId();
								#endif
								resetParser();
							}
							#if defined(FRAME_PARSER_DEBUG_INFO)
								qDebug() << "[FrameParser: data stream locked] src:" << mCurrentNetSrc << "channel:" << channelId();
							#endif
						} else {
							mExpectedFrameNum = updateExpectedFrameNum(mExpectedFrameNum);
						}
					} else {
						resetParser(); // ? or enough only: mParserState = StartWaitSync;
					}
				}
			break;

			//---
			case WorkFlow:
				parseFrameElem(currData);
			break;

			//---
			default:
			break;
		}

		//---
		++begin;
	}
	return true;
}

//-----------------------------------------------------------------------------
void TFrameParser::parseFrameElem(TBufData frameElem)
{
	switch(mWorkFlowState) {
		//---
		case WaitFrame:
			if(mStatePhase == Phase0) {
				if(frameElem == SOF1_Pattern) {
					mStatePhase = Phase1;
				} else {
                    #if defined(FRAME_PARSER_ERROR_INFO)
                        qDebug() << "[ERROR][Frame Sync Error (phase 0)] src:" << mCurrentNetSrc << "channel" << channelId();
					#endif
					resetParser();
				}
			} else {
				mStatePhase = Phase0;
				if(frameElem == mExpectedFrameNum) {
					mExpectedLineNum = 0;
					mWorkFlowState = WaitLine;
				} else {
                    #if defined(FRAME_PARSER_ERROR_INFO)
                        qDebug() << "[ERROR][Frame Sync Error (phase 1)] src:" << mCurrentNetSrc << "channel" << channelId();
					#endif
					resetParser();
				}
			}
		break;

		//---
		case WaitLine:
			if(mStatePhase == Phase0) {
				if(frameElem == SOL1_Pattern) {
					mStatePhase = Phase1;
				} else {
                    #if defined(FRAME_PARSER_ERROR_INFO)
                        qDebug() << "[ERROR][Line Sync Error (phase 0)] src:" << mCurrentNetSrc << "channel" << channelId();
					#endif
					resetParser();
				}
			} else {
				mStatePhase = Phase0;
				if(frameElem == mExpectedLineNum) {
					mPixelNum = 0;
					mWorkFlowState = WaitPixel;
				} else {
                    #if defined(FRAME_PARSER_ERROR_INFO)
                        qDebug() << "[ERROR][Line Sync Error (phase 1)] src:" << mCurrentNetSrc << "channel" << channelId();
					#endif
					resetParser();
				}
			}
		break;

		//---
		case WaitPixel:
			{
				// in test mode: check pixel value
				*mBufPtr++ = frameElem;
				bool endOfActiveBuffer = (mBufPtr == mBufEnd);
				bool endOfParsedFrame = false;

				mPixelNum = updatePixelNum(mPixelNum);
				if(mPixelNum == 0) {
					mExpectedLineNum = updateExpectedLineNum(mExpectedLineNum);
					if(mExpectedLineNum == 0) {
						mExpectedFrameNum = updateExpectedFrameNum(mExpectedFrameNum);
						endOfParsedFrame = true;
						mWorkFlowState = WaitFrame;
					} else {
						mWorkFlowState = WaitLine;
					}
				}
				if(endOfActiveBuffer != endOfParsedFrame) {
					if(endOfActiveBuffer && !endOfParsedFrame) {
                        #if defined(FRAME_PARSER_ERROR_INFO)
							qDebug() << "[ERROR] ActiveBuffer is filled but input frame not end";
						#endif
					} else {
                        #if defined(FRAME_PARSER_ERROR_INFO)
							qDebug() << "[ERROR] ActiveBuffer is not filled but input frame end";
						#endif
					}
					resetParser();
				} else {
					if(endOfParsedFrame) {
						mActiveBufPtr->setNetSrc(mCurrentNetSrc);
						#if 0 /*TEST*/
							const int CloneNum = 4;
							TBaseMsgWrapperPtr frameClones[CloneNum];
							qDebug() << "(1) free buffers in pool:" << mBufPool->size();
							qDebug() << "sumPix(ref) = " << sumPix(mActiveBufPtr);
							for(int n = 0; n < CloneNum; ++n) {
								if(mBufPool->size() == 0) {
									qDebug() << "no free buffers in pool";
								}
								#if !defined(MSG_SELF_RELEASE)
									mActiveBufPtr->msgClone(frameClones[n]);
								#else
									frameClones[n] = mActiveBufPtr;
								#endif
								qDebug() << "clone:" << n << " src:"	<< frameClones[n]->netSrc() << " msg pool Id:" << frameClones[n]->msgPoolId();
								if(sumPix(mActiveBufPtr) - sumPix(frameClones[n])) {
									qDebug() << "[ERROR] diff != 0:" << sumPix(mActiveBufPtr) - sumPix(frameClones[n]);
								}
							}
							qDebug() << "(2) free buffers in pool:" << mBufPool->size();
							#if !defined(MSG_SELF_RELEASE)
								for(int n = 0; n < CloneNum; ++n) {
									releaseMsg(frameClones[n]);
								}
							#endif
							qDebug() << "(3) free buffers in pool:" << mBufPool->size();
						#endif
						/*TEST*/ //qDebug() << "ref src:" << mActiveBufPtr->netSrc() << " ref msg pool Id:" << mActiveBufPtr->msgPoolId();
						if(mParsedBufQueue) {
							#if defined(FRAME_PARSER_DEBUG_INFO)
								qDebug() << "[frame parsed][1] src:" << mCurrentNetSrc << "channel:" << channelId() << "FrameNum:" << mSentFrameNum /* << "msg pool Id:" << mActiveBufPtr->msgPoolId() */ /* << "next frame num:" << mExpectedFrameNum */;
							#endif
							mActiveBufPtr->setMsgId(mSentFrameNum++);
							sendFilledBuf();
						} else {
							#if !defined(MSG_SELF_RELEASE)
								releaseMsg(mActiveBufPtr);
							#endif
							#if defined(FRAME_PARSER_DEBUG_INFO)
								qDebug() << "[frame parsed][2] src:" << mCurrentNetSrc << "channel:" << channelId() /*<< "next frame num:" << mExpectedFrameNum */;
							#endif
						}
						/*TEST*/ //qDebug() << "(4) free buffers in pool:" << mBufPool->size();
						if(!getActiveBuf()) {
                            #if defined(FRAME_PARSER_ERROR_INFO)
								qDebug() << "[ERROR] getActiveBuf() (2)" << "Src:" << mCurrentNetSrc << "channel:" << channelId();
							#endif
							resetParser();
						}
					}
				}
			}
		break;

		//---
		default:
		break;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TDevParser::TDevParser() :
							mChannelParserBundle(),
							mConnectedChannel(NotConnected),
							mNetSrc(CfgDefs::NetNoAddr())
{

}

//-----------------------------------------------------------------------------
TDevParser::~TDevParser()
{
	for(TChannelParserMap::iterator channelParserParams = mChannelParserBundle.begin(); channelParserParams != mChannelParserBundle.end(); ++channelParserParams) {
		qDebug() << "~DevParser. channel" << channelParserParams.key() << "deleted";
		delete channelParserParams.value().mChannelParser;
	}
	mChannelParserBundle.clear();
}

//-----------------------------------------------------------------------------
bool TDevParser::attachChannelParser(TChannelId channelId, TChannelParser* channelParser)
{
	if(isChannelParserExist(channelId)) {
		return false;
	} else {
		channelParser->setChannelId(channelId);
		mChannelParserBundle.insert(channelId, TChannelParserParams(true,channelParser));
		return true;
	}
}

//-----------------------------------------------------------------------------
//#define PARSE_DRV_BUF_DEBUG_INFO
void TDevParser::parseDrvBuf(TBufData* bufBegin, TBufData* bufEnd)
{
	#if defined(PARSE_DRV_BUF_DEBUG_INFO)
		qDebug() << "\n*** parse DrvBuf from src:" << netSrc() << " ***\n";
	#endif

	unsigned countKnownDFM   = 0;
	unsigned countUnknownDFM = 0;
	unsigned countParsed = 0;
	unsigned countUnparsed = bufEnd - bufBegin;

	while(bufBegin != bufEnd) {
		if(mConnectedChannel == NotConnected) {
			if(isChannelMarker(*bufBegin)) {
				TChannelId channel = getChannelId(*bufBegin);
				//qDebug() << "DFM, channel:" << channel;
				if(isChannelValid(channel)) {
					++countKnownDFM;
					mConnectedChannel = channel;
				} else {
					++countUnknownDFM;
				}
			}
		} else {
			unsigned chunkSize = bufEnd - bufBegin;
			if(!mChannelParserBundle.find(mConnectedChannel).value().mChannelParser->parseDataBlock(bufBegin,bufEnd,netSrc())) {
				mConnectedChannel = NotConnected;
			}
			unsigned parsedElems = chunkSize - (bufEnd - bufBegin);
			//qDebug() << "processed elems:" << parsedElems;
			countParsed += parsedElems;
			continue;
		}
		++bufBegin;
	}
	countUnparsed -= (countKnownDFM + countUnknownDFM + countParsed);

	#if defined(PARSE_DRV_BUF_DEBUG_INFO)
		qDebug() << "\n--- buffer parsing stat ---";
		qDebug() << "KnownDFM's:  " << countKnownDFM;
		qDebug() << "UnknownDFM's:" << countUnknownDFM;
		qDebug() << "parsed:      " << countParsed;
		qDebug() << "unparsed:    " << countUnparsed;
		qDebug() << "total:       " << (countKnownDFM + countUnknownDFM + countParsed + countUnparsed);
		qDebug() << "---------------------------\n";
	#endif
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TDevBundleParser::~TDevBundleParser()
{
	for(TDevParserMap::iterator devParserParams = mDevParserBundle.begin(); devParserParams != mDevParserBundle.end(); ++devParserParams) {
		qDebug() << "~DevBundleParser" << devParserParams.key();
		delete devParserParams.value().mDevParser;
	}
}

//-----------------------------------------------------------------------------
bool TDevBundleParser::attachDevParser(TNetAddr srcAddr, TDevParser* devParser)
{
	if(isDevParserExist(srcAddr)) {
		return false;
	} else {
		devParser->setNetSrc(srcAddr);
		mDevParserBundle.insert(srcAddr,TDevParserParams(true,devParser));
		return true;
	}
}

//-----------------------------------------------------------------------------
void TDevBundleParser::parseDrvBuf(TDrvBufPtr& drvBufPtr)
{
	if(!drvBufPtr)
		return;

	TDevParserMap::const_iterator devParserParams = mDevParserBundle.find(drvBufPtr->netSrc());
	if(devParserParams == mDevParserBundle.end()) {
		qDebug() << qPrintable(QString("[DevBundleParser] DevParser for %1 not exist. DrvBuf rejected").arg(drvBufPtr->netSrc(),8,16,QLatin1Char('0')));
	} else {
		TDevParser* devParser = devParserParams.value().mDevParser;
		if(devParserParams.value().mEnabled && devParser) {
			TRawBuf* buf = checkMsg<TRawBuf>(drvBufPtr);
			if(buf) {
				TBufData* bufBegin = buf->getDataBuf<TBufData>();
				//TBufData* bufEnd   = bufBegin + buf->bufSize<TBufData>();
				TBufData* bufEnd   = bufBegin + buf->dataLen<TBufData>();
				devParser->parseDrvBuf(bufBegin,bufEnd);
			}
		}
	}
	#if !defined(MSG_SELF_RELEASE)
		releaseMsg(drvBufPtr);
	#endif
}


