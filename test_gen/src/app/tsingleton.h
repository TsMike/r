#if !defined(TSINGLETON_H)
#define TSINGLETON_H

#include "app/CfgDefs.h"

#if defined(USE_AF_SYSTEM_SINGLETON)

//*****************************************************************************

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template<typename T> class TSingleton
{
 public:
  static T& instance()
   {
	static T mInstance;
	return mInstance;
   }
};

#endif

#endif // TSINGLETON_H


