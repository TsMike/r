//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/StreamSwitch.h"


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
int TStreamSwitch::dispatchMessages()
{
	int nDispatchedMessages = 0;
	for(TSwitchMap::iterator channel = mSwitchMap.begin(); channel != mSwitchMap.end(); ++channel) {
		const int OutConnections = outConnectionsNum(channel);
		if(channel.value()[0].mConnected) {
			int inQueueMsgNum = channel.value()[0].mQueue.size();
			while(inQueueMsgNum--) {
				TBaseMsgWrapperPtr inMsg;
				channel.value()[0].mQueue.get(inMsg);
				if(OutConnections) {
					int outConnections = OutConnections;
					inMsg->setNetDst(channel.key());
					//qDebug() << "channel:" << channel.key() << "src:" << inMsg->netSrc() << "dst:" << inMsg->netDst() /*<< "msg pool Id:" << inMsg->msgPoolId() */;
					for(int n = 1; n < channel.value().size(); ++n) {
						if(channel.value()[n].mConnected) {
							if(--outConnections == 0) {
								channel.value()[n].mQueue.put(inMsg);
								++nDispatchedMessages;
								//qDebug() << "[0]" << outConnections << n;
							} else {
								//qDebug() << "[!0]" << outConnections << n;
								TBaseMsgWrapperPtr outMsg;
								#if defined(MSG_SELF_RELEASE)
									outMsg = inMsg;
									channel.value()[n].mQueue.put(outMsg);
									++nDispatchedMessages;
								#else
									if(inMsg->msgClone(outMsg)) {
										channel.value()[n].mQueue.put(outMsg);
										++nDispatchedMessages;
									} else {
										qDebug() << "[ERROR] can not clone msg:" << channel.key() << n;
									}
								#endif
							}
						}
					}
				} else {
					//qDebug() << "channel:" << channel.key() << "src:" << inMsg->netSrc() << "dst:" << inMsg->netDst();
					#if !defined(MSG_SELF_RELEASE)
						releaseMsg(inMsg);
					#endif
				}
			}
		}
	}
	return nDispatchedMessages;
}
