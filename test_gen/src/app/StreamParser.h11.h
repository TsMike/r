#if !defined(STREAM_PARSER_H)
#define STREAM_PARSER_H

#include "app/CfgDefs.h"
#include "app/Msg.h"
#include "app/DrvBuf.h"
#include "app/Frame.h"

#include <QtCore/QMap>

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TChannelParser
{
	friend class TDevParser;

	public:
		typedef qint16  TChannelId;
		typedef TRoutingPolicy::TNetAddrType TNetAddrType;
		typedef quint16 TBufData;

		TChannelParser(TMsgWrapperPoolQueue* bufPool, TMsgWrapperPoolQueue* parsedBufQueue);
		virtual ~TChannelParser() { qDebug() << "~ChannelParser"; }
		virtual bool parseDataBlock(TBufData*& begin, TBufData* end, TNetAddrType netSrc);
		TChannelId channelId() const { return mChannelId; }

	protected:
		void setChannelId(TChannelId channelId) { mChannelId = channelId; }

		TNetAddrType          mCurrentNetSrc;
		TChannelId            mChannelId;
		TMsgWrapperPoolQueue* mBufPool;
		TMsgWrapperPoolQueue* mParsedBufQueue;
		TBaseMsgWrapperPtr    mActiveBufPtr;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseFrameParser : public TChannelParser
{
	public:
		TBaseFrameParser(unsigned frameWidth, unsigned frameHeight, TMsgWrapperPoolQueue* bufPool, TMsgWrapperPoolQueue* parsedBufQueue);
		virtual ~TFrameParser() { qDebug() << "Frames parsed and sent:" << mSentFrameNum; }
		virtual bool parseDataBlock(TBufData*& begin, TBufData* end, TNetAddrType netSrc);
		unsigned frameWidth() const { return mFrameWidth; }
		unsigned frameHeight() const { return mFrameHeight; }
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TTestStreamParser : public TChannelParser
{
	public:
		static const TChannelId ChannelId = 0;

		TTestStreamParser(quint16 valueInc,unsigned frameWidth,unsigned frameHeight,TMsgWrapperPoolQueue* bufPool,TMsgWrapperPoolQueue* parsedBufQueue) :
																									mFrameWidth(frameWidth),
																									mFrameHeight(frameHeight),
																									mValueInc(valueInc),
																									TChannelParser(bufPool,parsedBufQueue) {}
		virtual bool parseDataBlock(TBufData*& begin, TBufData* end, TNetAddrType netSrc);

	private:
		unsigned    mFrameWidth;
		unsigned    mFrameHeight;
		quint16		mValueInc;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TFrameParser : public TChannelParser
{
	public:
		TFrameParser(unsigned frameWidth, unsigned frameHeight, TMsgWrapperPoolQueue* bufPool, TMsgWrapperPoolQueue* parsedBufQueue);
		virtual ~TFrameParser() { qDebug() << "Frames parsed and sent:" << mSentFrameNum; }
		virtual bool parseDataBlock(TBufData*& begin, TBufData* end, TNetAddrType netSrc);
		unsigned frameWidth() const { return mFrameWidth; }
		unsigned frameHeight() const { return mFrameHeight; }

	protected:
		static const unsigned SyncLength    = 1;
		static const TBufData MaxFrameNum   = 1023;
		static const TBufData SOF1_Pattern  = ((1 << 10) | 1023);
		static const TBufData SOF2_MaxValue = MaxFrameNum; // ? - in old version SOF2_MaxValue = 1023
		static const TBufData SOL1_Pattern  = ((1 << 10) | 1022);

		enum TStatePhase    { Phase0, Phase1 };
		enum TParserState   { StartWaitSync, WaitSync, WorkFlow };
		enum TWorkFlowState { WaitFrame, WaitLine, WaitPixel };

		void resetParser();
		void parseFrameElem(TBufData frameElem);
		TBufData updateExpectedFrameNum(TBufData value) const { return (value >= MaxFrameNum) ? 0 : (value+1); }
		TBufData updateExpectedLineNum(TBufData value) const { return (value >= (frameHeight()-1)) ? 0 : (value+1); }
		unsigned updatePixelNum(unsigned value) const { return (value >= (frameWidth()-1)) ? 0 : (value+1); }
		bool getActiveBuf();

		TParserState   mParserState;
		TWorkFlowState mWorkFlowState;
		TStatePhase    mStatePhase;
		TBufData       mExpectedFrameNum;
		TBufData       mExpectedLineNum;
		unsigned       mPixelNum;
		unsigned       mSyncCounter;
		unsigned       mSentFrameNum;

		unsigned           mFrameWidth;
		unsigned           mFrameHeight;
		TRawFrame::TPixel* mBufPtr;
		TRawFrame::TPixel* mBufEnd;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TDevParser
{
	friend class TDevBundleParser;

	public:
		typedef TRoutingPolicy::TNetAddrType TNetAddrType;
		typedef TChannelParser::TBufData TBufData;
		typedef TChannelParser::TChannelId TChannelId;

		TDevParser();
		~TDevParser();
		bool attachChannelParser(TChannelId channelId, TChannelParser* channelParser);
		void parseDrvBuf(TBufData* begin, TBufData* end);
		TNetAddrType netSrc() const { return mNetSrc; }

		//---
		static bool isDFM(TBufData data)
		{
			static const TBufData DFM = 0x0800;
			return (data & DFM);
		}

		//---
		static TChannelId channelId(TBufData data)
		{
			static const TBufData ChannelId = 0x001F;
			return (data & ChannelId);
		}

		//---
		bool isChannelParserExist(TChannelId channelId) const {
			TChannelParserMap::const_iterator channelParserParams = mChannelParserBundle.find(channelId);
			return (channelParserParams != mChannelParserBundle.end()) ? true : false;
		}

		//---
		bool isChannelValid(TChannelId channelId) const {
			TChannelParserMap::const_iterator channelParserParams = mChannelParserBundle.find(channelId);
			if(channelParserParams != mChannelParserBundle.end()) {
				if(channelParserParams.value().mEnabled) {
					return true;
				}
			}
			return false;
		}

	protected:
		virtual bool isChannelMarker(TBufData data) const { return TDevParser::isDFM(data); }
		virtual TChannelId getChannelId(TBufData data) const { return TDevParser::channelId(data); }

		static const TChannelId NotConnected = -1;

		struct TChannelParserParams
		{
			TChannelParserParams(bool enabled, TChannelParser* channelParser) : mEnabled(enabled), mChannelParser(channelParser) {}
			bool mEnabled;
			TChannelParser* mChannelParser;
			// pool of free buffers (?)
			// output queue (?)
		};

		void setNetSrc(TNetAddrType netSrc) { mNetSrc = netSrc; }

		typedef QMap<TChannelId,TChannelParserParams> TChannelParserMap;

	private:
		TChannelParserMap mChannelParserBundle;
		TChannelId mConnectedChannel;
		TNetAddrType mNetSrc;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TDevTestParser : public TDevParser
{
	public:
		TDevTestParser() : TDevParser() {}

	protected:
		virtual bool isChannelMarker(TBufData) const { return true; }
		virtual TChannelId getChannelId(TBufData) const { return TTestStreamParser::ChannelId; }
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TDevBundleParser
{
	public:
		typedef TRoutingPolicy::TNetAddrType TNetAddrType;
		typedef TDevParser::TBufData TBufData;

		~TDevBundleParser();
		bool attachDevParser(TNetAddrType src, TDevParser* devParser);
		void parseDrvBuf(TDrvBufPtr& drvBufPtr);
		bool isDevParserExist(TNetAddrType srcAddr) const {
			TDevParserMap::const_iterator devParserParams = mDevParserBundle.find(srcAddr);
			return (devParserParams != mDevParserBundle.end()) ? true : false;
		}

	private:
		struct TDevParserParams
		{
			TDevParserParams(bool enabled, TDevParser* devParser) : mEnabled(enabled), mDevParser(devParser) {}

			bool mEnabled;
			TDevParser* mDevParser;
		};

		typedef QMap<TNetAddrType,TDevParserParams> TDevParserMap;
		TDevParserMap mDevParserBundle;
};

#endif // STREAM_PARSER_H


