greaterThan(QT_MAJOR_VERSION, 4): QT += widgets 


TEMPLATE    = app
TARGET      = RTestGen
CONFIG     += console
DEPENDPATH += .
TOPDIR      = ../..

include($$TOPDIR/common.pri)
include($$TOPDIR/build.pri)

HEADERS    += \
               msg.h                \
               Frame.h              \
               TestStreamGen.h      \
               RawBuf.h             \
               DrvBuf.h             \
               BufPool.h            \
               DrvLayer.h           \
               udp_lib_adapter.h    \
               StreamParser.h       \
               StreamSwitch.h       \
               FrameFormer.h        \
               FrameFormerLayer.h   \
               UI_Layer.h           \
               AF_System.h          \
               tqueue.h             \
               tthread.h            \
               tsingleton.h         \
               SysUtils.h           \
               CfgDefs.h
SOURCES    += \
               Frame.cpp            \
               TestStreamGen.cpp    \
               BufPool.cpp          \
               DrvLayer.cpp         \
               udp_lib_adapter.cpp  \
               StreamParser.cpp     \
               StreamSwitch.cpp     \
               FrameFormer.cpp      \
               FrameFormerLayer.cpp \
               UI_Layer.cpp         \
               AF_System.cpp        \
               main.cpp 

QT         += testlib
QT         += opengl

win32 {
    LIBS += -lsetupapi -luuid -ladvapi32 -lws2_32 -ludp_lib
#    LIBS += -lsetupapi -luuid -ladvapi32 -lws2_32 -lip_pipe_lib -ludp_lib
}

#LIBS += -lapr -llibapr -laprapp -llibaprapp -lusbadapter -lufo

#------------------------------------------------------------------------------------------------
# notes:
#
# QT += core gui - not need because core and gui are included by default


# FORMS += \
