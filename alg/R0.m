%--------------------------------------------------------------------------
function R0(varargin)
    %clear ipPipeCmd;
    clc;
    close all;
    clear all;
    
    %---
    ThermoChannel = 0;
    VideoChannel  = 1;

    %---
    HomePath = 'E:\projects\OESD\';
    IpPipePath   = [HomePath 'A\sw32\ip_pipe\src\math\'];
    AlgUtilsPath = [HomePath 'A\alg\'];
    path(path,IpPipePath);
    path(path,AlgUtilsPath);
    
    import ip_pipe.*
    import alg_utils.*;

    %---
    Channel                  = VideoChannel;
    ApproxFuncFocus          = 0;
    SaveFile                 = 1;
    CompareFuncFocus         = 0;
    FuncFocusSrc             = 1; % 0 - prg, 1 - matlab
    ScopePointNum            = 500;
    MaxRoiSize               = 256;
    MaxAfPoints              = 201;
    FrameWidth               = MaxRoiSize;
    FrameHeight              = MaxRoiSize;
    FramePixelSize           = sizeof('uint16');
    
    FramePipeName            = 'LibRFramePipe';
    FramePipeChunkSize       = FrameWidth*FrameHeight*FramePixelSize + 4096;
    FramePipeChunkNum        = 64;
    FramePipeInitTimeout     = 1000;
    FramePipeTransferTimeout = 200;
    
    frameBuf = genBuf(1,FramePipeChunkSize/FramePixelSize,'uint16');
    focusVec     = zeros(1,MaxAfPoints);
    funcFocusVec = zeros(1,MaxAfPoints);

    %---
    AlgList = [ ... 
                '@PRG'; ...
                'TENG'; ...
                'GLVA'; ...
                'TENV'; 'VOLA'; ...
                'GDER'; 'GLLV'; ...
                'BREN'; 'GRAE'; 'GRAT'; 'GRAS'; 'LAPE'; 'LAPM'; 'LAPD'; 'SFRQ'; ...
                'WAVS'; 'WAVV'; ...
              ];
    global PrgData;
    PrgData = struct( ...
                      'isFinished', 0, ...
                      'Scope1PointNum', ScopePointNum, ...
                      'FuncFocusNum', 10, ...
                      'AlgList', AlgList, ...
                      'currAlg',[] ...
                     ); 
    PrgData.hs = createUi;
    %---
    [status, framePipe] = ipPipeStart(FramePipeName,'rx',FramePipeChunkSize,FramePipeChunkNum,FramePipeInitTimeout);
        if(status ~= 0)
            PrgData.isFinished = 1;
        end
        
    %---
    focusPointsNum = 0;
    focusPointIdx  = 1;
    funcFocusIdx   = 1;
    funcFocusBundle = zeros(1,PrgData.FuncFocusNum)*NaN;
    afTrigger = 0;
    afStart   = 0;
    afEnd     = 0;
    %figure;
    
    %---
    while(~PrgData.isFinished)  
        %---
        nRdyFrame = 0;
        while((ipPipeCmd('isBufEmpty',framePipe) == 0) || (nRdyFrame == 0))
            [status,frameWordsRvd] = ipPipeCmd('transferBuf',framePipe,0,frameBuf,FramePipeTransferTimeout);
            if(status ~= 0) 
                %fprintf(1,'[ERROR] transferBuf status %3d, bufSize: %10d, read %2d frames\n',status,frameWordsRvd,nRdyFrame);
                break;
            end
            nRdyFrame = nRdyFrame + 1;
        end
        %---
        if(getChannelId(frameBuf) == Channel)
            [frameInfo, frame] = getFrame(frameBuf);
             jobTimeStart = tic;
             prgFuncFocusValue = frameInfo.funcFocusValue;
             matFuncFocusValue = focusFunc(frame,frameInfo.afAlgType,frameInfo.afAvgNum);
             jobTime = toc(jobTimeStart);
             err = 100.0*abs(prgFuncFocusValue - matFuncFocusValue)/(matFuncFocusValue+eps);
             if(CompareFuncFocus && (prgFuncFocusValue ~= matFuncFocusValue))
                 if(err > 1e-10)
                    %fprintf(1,'[ERROR] [prg]: %10.1f, [mat]: %10.1f %5d %1d\n',prgFuncFocusValue,matFuncFocusValue,frameInfo.focusValue,frameInfo.afState);
                    fprintf(1,'[ERROR] [prg]: %8.1f, [mat]: %8.5f [err]: %10.6f\n',prgFuncFocusValue,matFuncFocusValue,err);
                 end
             end    
            if(FuncFocusSrc == 0)
                funcFocusValue = prgFuncFocusValue;
            else
                funcFocusValue = matFuncFocusValue;
            end
            scopePlot(1,[funcFocusValue NaN*matFuncFocusValue]);
            
            %---
            printAfInfo = 0;
            switch (frameInfo.afState)
                case 1
                    set(PrgData.hs.hProfileClearButton,'enable','off');
                    focusPointsNum = frameInfo.afParam1;
                    focusPointIdx = 1;
                    focusVec(focusPointIdx)     = frameInfo.focusValue;
                    funcFocusVec(focusPointIdx) = funcFocusValue;
                    funcFocusBundle = zeros(1,PrgData.FuncFocusNum)*NaN;
                    funcFocusBundle(funcFocusIdx) = funcFocusVec(focusPointIdx);
                    scopeInit(2,PrgData.FuncFocusNum,focusPointsNum,1.0);
                    scopePlot(2,funcFocusBundle);
                    %printAfInfo = 1;
                    fprintf(1,'\n[AF start] -------------------');
                    fprintf(1,'total points: %3d\n',focusPointsNum);
                    fprintf(1,'afAlgType: %1d\n',frameInfo.afAlgType);
                    fprintf(1,'afAvgNum:  %1d\n',frameInfo.afAvgNum);
                    fprintf(1,'roiSize:   %3d\n',frameInfo.roiSize);
                    fprintf(1,'roiX0:     %3d\n',frameInfo.roiX0);
                    fprintf(1,'roiY0:     %3d\n',frameInfo.roiY0);
                case {2,3}
                    focusPointIdx = focusPointIdx + 1;
                    focusVec(focusPointIdx)     = frameInfo.focusValue;
                    funcFocusVec(focusPointIdx) = funcFocusValue;
                    funcFocusBundle(funcFocusIdx) = funcFocusVec(focusPointIdx);
                    scopePlot(2,funcFocusBundle);
                    %printAfInfo = 1;
                case 4
                    set(PrgData.hs.hProfileClearButton,'enable','on');
                    funcFocusIdx = funcFocusIdx + 1;
                    if(funcFocusIdx > PrgData.FuncFocusNum)
                        funcFocusIdx = 1;
                    end    
                    maxFuncFocus = max(funcFocusVec(1:focusPointsNum));
                    maxFuncFocusIdx = find(funcFocusVec(1:focusPointsNum) == maxFuncFocus);
                    maxFocus = focusVec(maxFuncFocusIdx);
                    
                    fprintf(1,'[from prg] best focus at: %5d, funcFocus: %7.0f\n',frameInfo.focusValue,prgFuncFocusValue);
                    fprintf(1,'[from mat] best focus at: idx = %2d, focus = %5d, funcFocus = %7.0f\n',maxFuncFocusIdx,maxFocus,maxFuncFocus);
                    
                    %---
                    if(ApproxFuncFocus)
                        approxFuncFocus(focusPointsNum,focusVec,funcFocusVec,SaveFile);
                    end
                otherwise
            end
            if(printAfInfo)
                fprintf(1,'point: %2d, focus: %5d, funcFocus: %8.0f\n',focusPointIdx,focusVec(focusPointIdx),funcFocusVec(focusPointIdx));
            end
            if(frameInfo.mode == 1)
                if(afTrigger == 0)
                    afTrigger = 1;
                    afStart = frameInfo.frameNum;
                end    
            else    
                if(afTrigger == 1)
                    afTrigger = 0;
                    afEnd = frameInfo.frameNum;
                    fprintf(1,'[AF end] Total time (frames): %3d\n',afEnd-afStart);
                end    
            end    
            
            %scopePlot(2,frameInfo.mode);
            %fprintf(1,'focus func: %10.1f\n',frameInfo.funcFocusValue);
            %imshow(frame,[0 1023]);
        end
        drawnow;
        %fprintf(1,'job time:     %10.0f ms\n',jobTime*1000);
    end
    
    clear ipPipeCmd;
    closeFigures();
    fprintf(1,'R0 finished\n');
end

%--------------------------------------------------------------------------
function approxFuncFocus(focusPointsNum,focusVec,funcFocusVec,saveFile)
     persistent fileNum;
     if(isempty(fileNum))
         fileNum = 1;
     end

     %---
     x = focusVec(1:focusPointsNum);
     y = funcFocusVec(1:focusPointsNum);
     
     %---
     if(saveFile)
        fileName = sprintf('FuncFocus%03d',fileNum);
        save(fileName,'x','y');
        fileNum = fileNum + 1;
     end
     
     %---
     DF = 1;
     N  = 9;
     NFig = 2;
     
     figure(NFig);
     x0 = x(1:DF:end);
     y0 = y(1:DF:end);
     p = polyfit(x0,y0,N);
     x1 = x(1):100:x(end);
     y1 = polyval(p,x1);
     hold on;
     plot(x,y,'color','blue');
     plot(x0,y0,'color','red','linestyle','none','marker','+');
     plot(x1,y1,'color','green');
     grid on;
     bestFocusIdx = find(y1 == max(y1),1);
     fprintf(1,'[from approx] best focus at: focus = %5d, funcFocus = %7.0f\n',x1(bestFocusIdx),y1(bestFocusIdx));
end

%--------------------------------------------------------------------------
function [frameInfo,frame] = getFrame(frameBuf)
    frameInfo = struct( ...
                        'channelId',      typecast(frameBuf(1:2),'uint32'), ...
                        'frameNum',       typecast(frameBuf(3:4),'uint32'), ...
                        'afState',        typecast(frameBuf(5:6),'int32'), ...
                        'roiX0',          frameBuf(7), ...
                        'roiY0',          frameBuf(8), ...
                        'roiSize',        frameBuf(9), ...
                        'afAlgType',      frameBuf(10), ...
                        'afAvgNum',       frameBuf(11), ...
                        'afParam1',       frameBuf(12), ...
                        'afParam2',       frameBuf(13), ...
                        'focusValue',     typecast(frameBuf(14:15),'int32'), ...
                        'funcFocusValue', typecast(frameBuf(16:19),'double'), ...
                        'mode',           frameBuf(20) ...
                       );
     roiSize = double(frameInfo.roiSize);
     startPos = 21;
     endPos   = startPos + roiSize*roiSize - 1;
     %frame = frameBuf(startPos:endPos);
     frame = reshape(frameBuf(startPos:endPos),roiSize,roiSize)';
end

%--------------------------------------------------------------------------
function [channelId] = getChannelId(frameBuf)
    channelId = typecast(frameBuf(1:2),'uint32');
end

%--------------------------------------------------------------------------
function [focusFuncVal] = focusFunc(frame,afAlgType,afAvgNum)
    global PrgData;
    import alg_utils.*;

    if(strcmp(PrgData.currAlg,'@PRG'))
        switch afAlgType
            case 0
                focusFuncVal = fmeasure(frame,'TENG',[]);
            case 1
                focusFuncVal = std(double(frame(:)),1); %fmeasure(frame,'GLVA',[]);
            otherwise
                focusFuncVal = fmeasure(frame,'TENG',[]);
        end    
    else
        focusFuncVal = fmeasure(frame,PrgData.currAlg,[]);
    end    
end

%--------------------------------------------------------------------------
function hs = createUi
    global PrgData;
    
    %---
    hs.hUiFig = figure('Name','AutoFocus','NumberTitle','off','Visible','off','DeleteFcn',{@fFigDelete});

    %---                        
    hs.hAlgSelect = uicontrol(hs.hUiFig, ...
                                       'Style', 'popup',...
                                       'String', PrgData.AlgList, ...
                                       'FontSize',8, ...
                                       'Units','normalized', ...
                                       'BackgroundColor', [0.0 0.0 0.2], ...
                                       'ForegroundColor', [1.0 1.0 0.0], ...
                                       'Position', [0.0 0.9 0.1 0.1],...
                                       'Callback', {@fAlgSelect} ...
                             );                       
    currAlgIdx = get(hs.hAlgSelect,'Value');
    PrgData.currAlg = PrgData.AlgList(currAlgIdx,:);
    
    %---
    hs.hProfileClearButton = uicontrol(hs.hUiFig, ...
                                   'Style','pushbutton', ...
                                   'Enable', 'on', ...
                                   'BackgroundColor', [0.0 0.7 0.7], ...
                                   'String','<html><center>clear<center>', ...
                                   'FontSize',10, ...
                                   'Units','normalized', ...
                                   'Position',[0.02 0.2 0.09 0.08 ], ...
                                   'Callback', {@fProfileClearButton } ...
                                   );
    %---
    PrgData.Scope(1).ScopeColorArray = [ 0.0 1.0 0.0; 0.0 0.0 1.0; 1.0 0.0 0.0; ];
    PrgData.Scope(1).hAxes = axes('Parent',hs.hUiFig,'OuterPosition',[0.05 0.5 1.0 0.5 ]);
    set(PrgData.Scope(1).hAxes,'Color',[0.1 0.4 0.2],'YLimMode','auto');
    scopeInit(1,2,PrgData.Scope1PointNum,0.5);
    
    %---
    %PrgData.Scope(2).ScopeColorArray = [ 1.0 1.0 0.0; 1.0 0.0 1.0; 0.0 1.0 1.0; ];
    PrgData.Scope(2).ScopeColorArray = [ ...
                                            1.0 0.0 0.0;
                                            0.0 0.0 1.0;
                                            0.0 1.0 0.0;
                                            0.0 1.0 1.0;
                                            1.0 0.0 1.0;
                                            1.0 1.0 0.0;
                                            1.0 0.5 0.0;
                                            1.0 0.0 0.5;
                                            0.0 1.0 0.5;
                                            0.5 1.0 0.0;
                                            0.5 0.0 1.0;
                                            0.0 0.5 1.0;
                                        ];
    PrgData.Scope(2).hAxes = axes('Parent',hs.hUiFig,'OuterPosition',[0.05 0.0 1.0 0.5]);
    %set(PrgData.Scope(2).hAxes,'Color',[0.1 0.4 0.8],'YLimMode','auto');
    set(PrgData.Scope(2).hAxes,'Color',[0.3 0.3 0.3],'YLimMode','auto');
    scopeInit(2,PrgData.FuncFocusNum,100,1.0);
    
    %---
    guidata(hs.hUiFig,struct('hs',hs));
    set(hs.hUiFig,'Visible','on');
    drawnow;
end

%--------------------------------------------------------------------------
function fFigDelete(~,~)
    global PrgData;
    PrgData.isFinished = 1;
    closeFigures;
end

%--------------------------------------------------------------------------
function closeFigures
    figHandles = findall(0,'Type','figure');
    delete(figHandles);
end

%--------------------------------------------------------------------------
function fProfileClearButton(~,~)
    global PrgData;
    
    %---
    cla(PrgData.Scope(2).hAxes);
end

%--------------------------------------------------------------------------
function fAlgSelect(hObject,~)
    global PrgData;
    currAlgIdx = get(hObject,'Value');
    PrgData.currAlg = PrgData.AlgList(currAlgIdx,:);
end

%--------------------------------------------------------------------------
function scopeInit(nScope,graphNum,xVecSize,fadeOld)
    global PrgData;
    if(PrgData.isFinished)
        return;
    end
    
    %---
    %cla(PrgData.Scope(nScope).hAxes);
    
    %---
    PrgData.Scope(nScope).graphNum  = graphNum;
    PrgData.Scope(nScope).xScopeVec = 1:xVecSize;
    PrgData.Scope(nScope).fadeOld   = fadeOld;

    %---
    PrgData.Scope(nScope).idx = 1;
    PrgData.Scope(nScope).range = numel(PrgData.Scope(nScope).xScopeVec);
    PrgData.Scope(nScope).colorIdx = 1;
    PrgData.Scope(nScope).yScopeVec = ones(PrgData.Scope(nScope).graphNum,PrgData.Scope(nScope).range)*NaN;
    hold(PrgData.Scope(nScope).hAxes,'on');
    PrgData.Scope(nScope).hGraph = zeros(PrgData.Scope(nScope).graphNum,2);
    for k = 1:PrgData.Scope(nScope).graphNum
        for j = 1:2
            PrgData.Scope(nScope).hGraph(k,j) = plot(PrgData.Scope(nScope).hAxes,PrgData.Scope(nScope).xScopeVec,PrgData.Scope(nScope).yScopeVec(k,:));
            set(PrgData.Scope(nScope).hGraph(k,j),'Color',PrgData.Scope(nScope).ScopeColorArray(k,:));
        end    
    end
    set(PrgData.Scope(nScope).hAxes,'XLim',[PrgData.Scope(nScope).xScopeVec(1) PrgData.Scope(nScope).xScopeVec(end)]);
    grid(PrgData.Scope(nScope).hAxes,'on');
end

%--------------------------------------------------------------------------
function scopePlot(nScope, vecY)
    global PrgData;
    if(PrgData.isFinished)
        return;
    end    

    for k = 1:PrgData.Scope(nScope).graphNum
        PrgData.Scope(nScope).yScopeVec(k,PrgData.Scope(nScope).idx) = vecY(k);
    end
    PrgData.Scope(nScope).idx = PrgData.Scope(nScope).idx + 1;
    if(PrgData.Scope(nScope).idx > PrgData.Scope(nScope).range)
        PrgData.Scope(nScope).idx = 1;
        
        colorIdx = PrgData.Scope(nScope).colorIdx;
        if(colorIdx == 1)
            PrgData.Scope(nScope).colorIdx = 2;
        else
            PrgData.Scope(nScope).colorIdx = 1;
        end
        for k = 1:PrgData.Scope(nScope).graphNum
            set(PrgData.Scope(nScope).hGraph(k,colorIdx),'Color',PrgData.Scope(nScope).ScopeColorArray(k,:)*PrgData.Scope(nScope).fadeOld,'YData',PrgData.Scope(nScope).yScopeVec(k,:));
            PrgData.Scope(nScope).yScopeVec(k,:) = PrgData.Scope(nScope).yScopeVec(k,:) + NaN;
            set(PrgData.Scope(nScope).hGraph(k,PrgData.Scope(nScope).colorIdx),'YData',PrgData.Scope(nScope).yScopeVec(k,:),'Color',PrgData.Scope(nScope).ScopeColorArray(k,:));
        end
    else
        for k = 1:PrgData.Scope(nScope).graphNum
            set(PrgData.Scope(nScope).hGraph(k,PrgData.Scope(nScope).colorIdx),'YData',PrgData.Scope(nScope).yScopeVec(k,:));
        end
    end    
end