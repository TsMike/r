%--------------------------------------------------------------------------
function ophir1
    a = 1.4;
    M = 12000;
    nMax = log(M)/log(a);
    floor(nMax)
    nVec = 1:floor(nMax);
    focusCmd = getFocusCmd(nVec,a)
    plot(nVec,focusCmd);
    grid;
    return;


    fov = 19200:100:33000;
    maxFocus = getMaxFocus(fov);
    plot(fov,maxFocus);
    grid;
end

%--------------------------------------------------------------------------
function [maxFocus] = getMaxFocus(fov)
    maxFocus = 5.88e-9*fov.^3 - 3.44e-4*fov.^2 + 6.79*fov - 4.51e4;
end

%--------------------------------------------------------------------------
function [focusCmd] = getFocusCmd(n,a)
    focusCmd = floor(a.^n);
end

