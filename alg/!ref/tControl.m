%--------------------------------------------------------------------------
function tControl
    clc;
    close all;
    clear all;

    %---
    TestMode        = 0;
    PortId          = 'COM7';
    N               = 400;
    WaitAnswerDelay = 0.05;
    TiRequest       = 'ti';
    
    global PrgData;
    PrgData = struct( ...
                      'isFinished', 0 ...
                     ); 
    
    PrgData.hs = createUi;
    
    %---
    if(~TestMode)
        sPort = serial(PortId);
        set(sPort,'Timeout',1,'Terminator','CR','InputBufferSize',1024);
        fopen(sPort);
    end
    
    n = 0; 
    while(n < N)
        if(PrgData.isFinished)
            break;
        end    
        if(TestMode)
            pause(2*WaitAnswerDelay);
            
            y0 = n/100;
            y = rem(n,6)+y0;
            yData1 = [y y+3];
            yData2 = [y y+3 y-2];
            scopePlot(1,yData1);
            scopePlot(2,yData2);
            n = n + 1;
            fprintf(1,'n: %4d\n',n);
        else
            %---
            cmd = rem(n,8);
            
            %--- send 'ti n' and read answer
            txCmd = sprintf('%s %d',TiRequest,cmd);
            fprintf(sPort,'%s\n',txCmd);
            pause(WaitAnswerDelay);
            [rxStatus] = readAnswer(sPort, txCmd);
            if(rxStatus <= 0)
                fprintf(1,'[ERROR] rxStatus (1): %d\n',rxStatus);
                break;
            end
        
            %--- send 'ti' and read answer
            txCmd = sprintf('%s',TiRequest);
            fprintf(sPort,'%s\n',txCmd);
            pause(WaitAnswerDelay);
            [rxStatus,devAnswer] = readAnswer(sPort, txCmd);
            if(rxStatus <= 0)
                fprintf(1,'[ERROR] rxStatus (2): %d\n',rxStatus);
                break;
            end
            
            %---
            sentValue = cmd;
            rvdValue  = str2double([devAnswer{1}]);
            gVec1 = [sentValue 2];
            gVec2 = [rvdValue sentValue-1 NaN];
            scopePlot(1,gVec1);
            scopePlot(2,gVec2);
        
            %---
            n = n + 1;
            if(rem(n,100) == 0)
                fprintf(1,'[INFO] transactions number: %4d\n',n);
                %fprintf(1,'n: %4d, rxStatus: %d, answer: %s\n',n,rxStatus,[devAnswer{:}]);
            end    
        end    
        drawnow;
    end

    if(~TestMode)
        fclose(sPort);
        delete(sPort);
    end
end

%--------------------------------------------------------------------------
function [status,devAnswer] = readAnswer(sPort, txCmd)
    status = 0;
    devAnswer = {};
    bytesRvd = get(sPort,'BytesAvailable');
    if(bytesRvd)
        [rawAnswer] = fread(sPort,bytesRvd);
        strAnswer = sprintf('%s',rawAnswer);
        parsedTxCmd =  strsplit(txCmd);
        parsedAnswer = strsplit(strAnswer);
        sizeTxCmd  = numel(parsedTxCmd);
        sizeAnswer = numel(parsedAnswer);
        
        if(sizeAnswer < 2*sizeTxCmd)
                status = -1;
                return;
        end    
        for n = 1:sizeTxCmd
            if(~strcmp(parsedTxCmd{n},parsedAnswer{n}) || ~strcmp(parsedTxCmd{n},parsedAnswer{n+sizeTxCmd}))
                status = -2;
                return;
            end    
        end
        devAnswer = parsedAnswer(2*sizeTxCmd+1:end);
        validAnsweridx = ~strcmp(devAnswer,'');
        devAnswer = devAnswer(validAnsweridx);
        if(isempty(devAnswer))
            status = 2;
        else    
            status = 1;
        end    
    end    
end

%--------------------------------------------------------------------------
function hs = createUi
    global PrgData;
    
    %---
    hs.hUiFig = figure('Name','Heater Controller','NumberTitle','off','Visible','off','DeleteFcn',{@fFigDelete});

    %---
    PrgData.Scope(1).graphNum  = 2; 
    PrgData.Scope(1).xScopeVec = 1:100;
    PrgData.Scope(1).ScopeColorArray = [ 0.0 1.0 0.0; 1.0 0.0 0.0; 0.0 0.0 1.0; ];
    PrgData.Scope(1).hAxes = axes('Parent',hs.hUiFig,'OuterPosition',[0.0 0.5 1.0 0.5 ]);
    set(PrgData.Scope(1).hAxes,'Color',[0.1 0.4 0.2],'YLim',[0 10]);
    
    %---
    PrgData.Scope(2).graphNum  = 3; 
    PrgData.Scope(2).xScopeVec = 1:200;
    PrgData.Scope(2).ScopeColorArray = [ 1.0 1.0 0.0; 1.0 0.0 1.0; 0.0 1.0 1.0; ];
    PrgData.Scope(2).hAxes = axes('Parent',hs.hUiFig,'OuterPosition',[0.0 0.0 1.0 0.5]);
    set(PrgData.Scope(2).hAxes,'Color',[0.1 0.4 0.8],'YLim',[-5 15]);

    %---
    PrgData.NScopes = numel(PrgData.Scope);
    for n = 1:PrgData.NScopes
        PrgData.Scope(n).idx = 1;
        PrgData.Scope(n).range = numel(PrgData.Scope(n).xScopeVec);
        PrgData.Scope(n).colorIdx = 1;
        PrgData.Scope(n).yScopeVec = ones(PrgData.Scope(n).graphNum,PrgData.Scope(n).range)*NaN;
        hold(PrgData.Scope(n).hAxes,'on');
        PrgData.Scope(n).hGraph = zeros(PrgData.Scope(n).graphNum,2);
        for k = 1:PrgData.Scope(n).graphNum
            for j = 1:2
                PrgData.Scope(n).hGraph(k,j) = plot(PrgData.Scope(n).hAxes,PrgData.Scope(n).xScopeVec,PrgData.Scope(n).yScopeVec(k,:));
                set(PrgData.Scope(n).hGraph(k,j),'Color',PrgData.Scope(n).ScopeColorArray(k,:));
            end    
        end
        set(PrgData.Scope(n).hAxes,'XLim',[PrgData.Scope(n).xScopeVec(1) PrgData.Scope(n).xScopeVec(end)]);
        grid(PrgData.Scope(n).hAxes);
    end    
    
    %---
    guidata(hs.hUiFig,struct('hs',hs));
    set(hs.hUiFig,'Visible','on');
    drawnow;
end

%--------------------------------------------------------------------------
function fFigDelete(~,~)
    global PrgData;
    PrgData.isFinished = 1;
    closeFigures;
end

%--------------------------------------------------------------------------
function closeFigures
    figHandles = findall(0,'Type','figure');
    delete(figHandles);
end

%--------------------------------------------------------------------------
function scopePlot(nScope, vecY)
    global PrgData;
    if(PrgData.isFinished)
        return;
    end    

    for k = 1:PrgData.Scope(nScope).graphNum
        PrgData.Scope(nScope).yScopeVec(k,PrgData.Scope(nScope).idx) = vecY(k);
    end
    PrgData.Scope(nScope).idx = PrgData.Scope(nScope).idx + 1;
    if(PrgData.Scope(nScope).idx > PrgData.Scope(nScope).range)
        PrgData.Scope(nScope).idx = 1;
        
        colorIdx = PrgData.Scope(nScope).colorIdx;
        if(colorIdx == 1)
            PrgData.Scope(nScope).colorIdx = 2;
        else
            PrgData.Scope(nScope).colorIdx = 1;
        end
        for k = 1:PrgData.Scope(nScope).graphNum
            set(PrgData.Scope(nScope).hGraph(k,colorIdx),'Color',PrgData.Scope(nScope).ScopeColorArray(k,:)*0.5,'YData',PrgData.Scope(nScope).yScopeVec(k,:));
            PrgData.Scope(nScope).yScopeVec(k,:) = PrgData.Scope(nScope).yScopeVec(k,:) + NaN;
            set(PrgData.Scope(nScope).hGraph(k,PrgData.Scope(nScope).colorIdx),'YData',PrgData.Scope(nScope).yScopeVec(k,:),'Color',PrgData.Scope(nScope).ScopeColorArray(k,:));
        end
    else
        for k = 1:PrgData.Scope(nScope).graphNum
            set(PrgData.Scope(nScope).hGraph(k,PrgData.Scope(nScope).colorIdx),'YData',PrgData.Scope(nScope).yScopeVec(k,:));
        end
    end    
end