function findSharpFocus
    fileName = 'FuncFocus001[TENG].mat';
    load(fileName);

    %---
    DF = 4;
    N  = 9;
    
    rawBestFocusIdx = find(y == max(y),1);
     x0 = x(1:DF:end);
     y0 = y(1:DF:end);

    
    splinetool(x0,y0);
    %---
    figure(1);
    hAxes = gca;
    hold on;
    set(hAxes,'XLim',[min(x) max(x)]);
    plot(hAxes,x,y,'color','blue');
    grid on;
end

%     x0 = x(1:DF:end);
%     y0 = y(1:DF:end);
%     p = polyfit(x0,y0,N);
%     x1 = x(1):100:x(end);
%     y1 = polyval(p,x1);
%     hold on;
%     plot(x,y,'color','blue');
%     plot(x0,y0,'color','red','linestyle','none','marker','+');
%     plot(x1,y1,'color','green');
%     grid on;
%     bestFocusIdx = find(y1 == max(y1),1);
%     fprintf(1,'[from approx] best focus at: focus = %5d, funcFocus = %7.0f\n',x1(bestFocusIdx),y1(bestFocusIdx));
