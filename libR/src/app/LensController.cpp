//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/LensController.h"
#include "app/libRImpl.h"
#include <numeric>

#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/highgui/highgui.hpp>

#if defined(FRAME_IP_PIPE)
    #include "ip_pipe/IP_PipeLib.h"

    static const TCHAR*   FramePipeFile      = TEXT("LibRFramePipe");
    static const TCHAR*   FramePipeView      = TEXT("LibRFramePipe_tx");
    static const uint32_t FramePipeChunkSize = TBaseLensController::MaxRoiSize*TBaseLensController::MaxRoiSize*sizeof(TRawFrame::TPixel) + 4096;
    static const uint32_t FramePipeChunkNum  = 64;
    static const int32_t  FramePipeTimeout   = 10;
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

const uint16_t TBaseLensController::RoiSize[RoiSizeNum] = { 32, 64, 128, 256 };

//-----------------------------------------------------------------------------
TBaseLensController::TBaseLensController(TLibR* parent, const std::wstring& threadName) :
                                    TThread(threadName),
                                    mParent(parent),
                                    mSemaphore(),
                                    mFrameQueue(),
                                    mPreconditioner(0),
                                    mFuncFocusAlg(0),
                                    mAfInitializer(0),
                                    mMotionController(0),
									#if defined(AF_TEST_INFO)
										mAfTrigger(false),
									#endif
                                    mState(DirectControl),
                                    mAvgMat(RoiSize[RoiSizeNum-1],RoiSize[RoiSizeNum-1],CV_16UC1)
{
	mParams.mode           = RLibLensDirectMode;
	mParams.afCallbackMode = RLibAfCallbackNormal;

	mFuncFocusAlgSet[TENG] = new TTengAlg(RoiSizeNum,RoiSize);
	mFuncFocusAlgSet[GLVA] = new TGlvaAlg;

    #if defined(FRAME_IP_PIPE)
        bool status = IP_pipe::createPipeView(FramePipeFile,TEXT("tx"),FramePipeChunkSize,FramePipeChunkNum);
        qDebug() << "[INFO] IP_Pipe " << qPrintable(QString::fromWCharArray(FramePipeFile)) << "(tx) creation status: " << status;
        IP_pipe::setRdy(FramePipeView);
    #endif
}

//-----------------------------------------------------------------------------
TBaseLensController::~TBaseLensController()
{
    delete mPreconditioner;
    mPreconditioner = 0;
	for(unsigned n = 0; n < FuncFocusAlgNum; ++n) {
		delete mFuncFocusAlgSet[n];
		mFuncFocusAlgSet[n] = 0;
	}
    delete mAfInitializer;
    mAfInitializer = 0;
    delete mMotionController;
    mMotionController = 0;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::setLensControlMode(const TAfParams* params)
{
    if(!checkParams(params)) {
        return false;
    }

    volatile TWinCsGuard::TLocker lock(mGuard);
    if((params->mode == RLibLensAfMode) && (mParams.mode == RLibLensAfMode)) {
        return false;
    }

    //--- ???
    TDrvBufPtr cmd;
    while(mCmdReplyQueue.get(cmd)) {}

	mParams = *params;

	#if 0
		if(channelId() == CfgDefs::ThermoStreamId) {
			mParams.afAlgType = TENG;
		}
		if(channelId() == CfgDefs::VideoStreamId) {
			mParams.afAlgType = TENG;
		}
	#endif

	if(params->afAlgType >= FuncFocusAlgNum) {
		mParams.afAlgType = 0;
	}
	mFuncFocusAlg = mFuncFocusAlgSet[mParams.afAlgType];

    if(mParams.mode == RLibLensDirectMode) {
        mState = DirectControl;
    } else {
		#if defined(AF_TEST_INFO)
			mAfTrigger = false;
		#endif
        mAfInitializer->reset();
    }

    return true;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::getLensControlMode(TAfParams* params)
{
    volatile TWinCsGuard::TLocker lock(mGuard);
    *params = mParams;
    return true;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::isPacketBypassed(const uint8_t* cmd, unsigned cmdLen) const
{
    return (isDirectLensControlMode() || !isLensControlCmd(cmd,cmdLen));
}

//-----------------------------------------------------------------------------
bool TBaseLensController::putCmd(TDrvBufPtr cmd, int cmdQueueLimit)
{
    if(mCmdReplyQueue.size() >= cmdQueueLimit)
        return false;
    mCmdReplyQueue.put(cmd);
    return true;
}

//-----------------------------------------------------------------------------
unsigned TBaseLensController::getReply(uint8_t* reply, unsigned maxReplyLen)
{
    TDrvBufPtr drvBufPtr;
    if(!mCmdReplyQueue.get(drvBufPtr) || !drvBufPtr)
        return 0;
    TRawBuf* drvBuf = checkMsg<TRawBuf>(drvBufPtr);
    if(!drvBuf)
        return 0;
    unsigned replyLen = drvBuf->dataLen<uint8_t>();
    if(replyLen > maxReplyLen)
        return 0;

    uint8_t* srcCmdBuf = drvBuf->getDataBuf<uint8_t>();
    memcpy(reply,srcCmdBuf,replyLen);
    return replyLen;
}

//-----------------------------------------------------------------------------
void TBaseLensController::receiveFrameSlot(TBaseMsgWrapperPtr framePtr)
{
    mFrameQueue.put(framePtr);
    mSemaphore.release();
}

//-----------------------------------------------------------------------------
//#define LENS_PARAMS_TEST
bool TBaseLensController::afStartAf()
{
    mMotionController->reset();
	mFocusSteadyDelay = focusSteadyDelay();
    mFocusFuncAvgNum  = mParams.afAvgNum;
	//qDebug() << "slon 1" << mParams.afAvgNum;
	cv::Rect roi(0,0,mParams.roiSize,mParams.roiSize);
    mAvgRoiMat = mAvgMat(roi);
    mAvgRoiMat = 0;
    mFocusPoint   = 0;
    computeFocusRange();
	#if defined(FRAME_IP_PIPE)
		mParams.afParam1 = mFocusPointNum;
	#endif

	#if defined(LENS_PARAMS_TEST)
        mParams.mode = RLibLensDirectMode;
    #else
        mMotionController->moveTo(mDesiredFocusArray[0]);
        mState = FocusPositioning;
    #endif
    return false;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::afComputeNextStep(TAfStatus& afStatus, cv::Mat& roiMat)
{
    //---
    if(mFocusSteadyDelay) {
        --mFocusSteadyDelay;
        //qDebug() << "focus:" << mMotionController->getFocus() << "delay:" << mFocusSteadyDelay;
        return false;
    }

    //---
	roiMat = mPreconditioner->compute(roiMat);
	mAvgRoiMat += roiMat;

	//---
	if(--mFocusFuncAvgNum == 0) {
		mAvgRoiMat = mAvgRoiMat/mParams.afAvgNum;
		#if defined(FRAME_IP_PIPE)
			mAvgRoiMat.copyTo(roiMat);
		#endif
		int32_t focus    = mMotionController->getFocus();
		double funcFocus = mFuncFocusAlg->compute(mAvgRoiMat);
		mFocusFuncAvgNum  = mParams.afAvgNum;
		mFocusSteadyDelay = focusSteadyDelay();
		mAvgRoiMat = 0;
		return globalSearchAlg(afStatus, focus, funcFocus);
	} else {
		//qDebug() << "focus:" << mMotionController->getFocus() << "avg" << mFocusFuncAvgNum;
		return false;
	}
}

//-----------------------------------------------------------------------------
bool TBaseLensController::globalSearchAlg(TAfStatus& afStatus, int32_t focus,  double funcFocus)
{
	//---
	mFocusArray[mFocusPoint]     = focus;
	mFocusFuncArray[mFocusPoint] = funcFocus;
	afStatus.focusValue          = focus;
	afStatus.focusFuncValue      = funcFocus;

	//---
	if(mFocusPoint == mFocusPointNum) {
		if(mParams.afCallbackMode >= RLibAfCallbackNormal) {
			afStatus.afState = RLibAfBestFocusPointState;
			afCallback(&afStatus);
		}
		//mParams.mode = RLibLensDirectMode;
		#if defined(AF_TEST_INFO)
			printf("[INFO] [Af end][GS] ch: %1d, focus: %5d, focusFunc: %8.1f\n",channelId(),afStatus.focusValue,afStatus.focusFuncValue);
		#endif
		return false;
	}

	//---
	if(mFocusPoint == 0) {
		afStatus.afState = RLibAfStartPointState;
	} else {
		if(mFocusPoint == mFocusPointNum-1) {
			afStatus.afState = RLibAfEndPointState;
		} else {
			afStatus.afState = RLibAfReperPointState;
		}
	}
	if(mParams.afCallbackMode > RLibAfCallbackNormal) {
		afCallback(&afStatus);
	}

	//---
	if(++mFocusPoint == mFocusPointNum) {
		// compute best focus
		#if 0
			/*TEST*/ mDesiredFocusArray[mFocusPoint] = mDesiredFocusArray[1];
		#else
			unsigned bestFocusIdx;
			mDesiredFocusArray[mFocusPoint] = computeBestFocus(bestFocusIdx);
		#endif
	}
	if(mFocusPoint == mFocusPointNum) {
		mMotionController->moveTo(mDesiredFocusArray[mFocusPoint],TBaseMotionController::FinalSpeed);
	} else {
		mMotionController->moveTo(mDesiredFocusArray[mFocusPoint]);
	}
	mState = FocusPositioning;
	return false;
}


//-----------------------------------------------------------------------------
int32_t TBaseLensController::computeBestFocus(unsigned& bestFocusIdx)
{
	int32_t bestFocusFunc = -1;
	#if defined(AF_TEST_INFO)
		printf("\n[INFO] [GS] ---------------------\n");
	#endif
	for(unsigned idx = 0; idx < mFocusPointNum; ++idx) {
		#if defined(AF_TEST_INFO)
			printf("[%2d] focus: %5d, funcFocus: %8.1f\n",idx,mFocusArray[idx],mFocusFuncArray[idx]);
		#endif
		if(mFocusFuncArray[idx] > bestFocusFunc) {
			bestFocusIdx = idx;
			bestFocusFunc = mFocusFuncArray[bestFocusIdx];
		}
	}
	#if defined(AF_TEST_INFO)
		printf("\n[INFO] [GS] ch: %1d, best focus at idx: %2d, focus: %5d, focusFunc: %8.1f\n",channelId(),bestFocusIdx,mFocusArray[bestFocusIdx],mFocusFuncArray[bestFocusIdx]);
		printf("[INFO] [GS] ---------------------\n\n");
	#endif
	return mFocusArray[bestFocusIdx];
}

//-----------------------------------------------------------------------------
bool TBaseLensController::afFsm(TAfStatus& afStatus, cv::Mat& roiMat)
{
    switch (mState) {
        case DirectControl:
			//qDebug() << "DirectControl" << channelId();
            return false;

        case InitAf:
			//qDebug() << "InitAf" << channelId();
            return mAfInitializer->init(afStatus,roiMat);

        case StartAf:
			//qDebug() << "StartAf" << channelId();
            return afStartAf();

        case FocusPositioning:
			//qDebug() << "FocusPositioning" << channelId();
            return mMotionController->keepMotion(afStatus,roiMat);

        case ComputeNextStep:
			//qDebug() << "ComputeNextStep" << channelId();
            return afComputeNextStep(afStatus,roiMat);

        default:
            break;
    }
    return false;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::onExec()
{
    TBaseMsgWrapperPtr framePtr;
    TAfStatus afStatus;
    cv::Mat roiMat;

    mSemaphore.acquire();
    if(threadExit())
        return threadExit();

    if(mFrameQueue.get(framePtr) && getRoiMat(framePtr,roiMat)) {
		/*TEST*/ //roiMat = 1;
		/*TEST INFO*/ // qDebug() << "slon" << framePtr->netDst();
		volatile TWinCsGuard::TLocker lock(mGuard); // ???

        //---
        afStatus.params     = mParams;
        afStatus.frameNum   = framePtr->msgId();
        afStatus.afState    = RLibAfOffState;
        afStatus.focusValue = -1;

        //---
        if(mParams.mode == RLibLensDirectMode) {
            mState = DirectControl;
        }
		#if defined(AF_TEST_INFO)
			if(mParams.mode == RLibLensAfMode) {
				if(mAfTrigger == false) {
					mAfTrigger = true;
					mStartAfFrame = afStatus.frameNum;
				}
			} else {
				if(mAfTrigger == true) {
					mAfTrigger = false;
					uint32_t afTime = afStatus.frameNum - mStartAfFrame;
					printf("[INFO] [Af end] ch: %1d, time (frames): %4d\n",channelId(),afTime);
				}
			}
		#endif

		//---
		while(afFsm(afStatus,roiMat)) {}

        //---
		bool enaModeCond = (afStatus.afState == RLibAfOffState) || (mParams.mode == RLibLensDirectMode);
		bool enaCallbackCond = (mParams.afCallbackMode == RLibAfCallbackAlways);


		if(enaModeCond && enaCallbackCond) {
			roiMat = mPreconditioner->compute(roiMat);
			afStatus.focusFuncValue = mFuncFocusAlg->compute(roiMat);
            afCallback(&afStatus);
		}

        #if defined(FRAME_IP_PIPE)
			//qDebug() << cv::sum(roiMat)[0];
			if(enaModeCond && !enaCallbackCond) {
				roiMat = mPreconditioner->compute(roiMat);
				afStatus.focusFuncValue = mFuncFocusAlg->compute(roiMat);
			}
            int status = sendSubFrameToPipe(framePtr->netDst(),roiMat,afStatus);
            if(status < 0) {
                qDebug() << "[ERROR] FRAME_IP_PIPE" << status;
            }
        #endif
		if(afStatus.afState == RLibAfBestFocusPointState) {
			mParams.mode = RLibLensDirectMode;
		}

    }
    return threadExit();
}

//-----------------------------------------------------------------------------
#if defined(FRAME_IP_PIPE)

//-----------------------------------------------------------------------------
int TBaseLensController::sendSubFrameToPipe(uint32_t channelId, cv::Mat& subFrame, TAfStatus& status)
{
    if(!IP_pipe::isPipeReady(FramePipeView)) {
        return 0;
    }
    if(!IP_pipe::isBufFull(FramePipeView)) {
        uint32_t* bufSizePtr;
        uint8_t*  chunk;
        IP_pipe::TStatus trStatus = IP_pipe::chunkAccess(FramePipeView,bufSizePtr,chunk,FramePipeTimeout);
        if(trStatus != IP_pipe::Ok) {
            return -(10 + trStatus);
        }

        if((trStatus == IP_pipe::Ok) && serializeSubFrame(channelId,subFrame,status,chunk,bufSizePtr)) {
            if(IP_pipe::advanceIdx(FramePipeView) == IP_pipe::Ok) {
                return 1;
            } else {
                return -2;
            }
        } else {
            return -3;
        }
    } else {
        return -4;
    }
}

//-----------------------------------------------------------------------------
bool TBaseLensController::serializeSubFrame(uint32_t channelId, cv::Mat& subFrame, TAfStatus& status, uint8_t* dst, uint32_t* streamLen)
{
    if(subFrame.empty()) {
        *streamLen = 0;
        return false;
    }

    TSerializer serializer(dst);
    serializer.write<uint32_t>(channelId);                 //  4
    serializer.write<uint32_t>(status.frameNum);           //  8
	serializer.write<int32_t>(status.afState);             // 12
	serializer.write<uint16_t>(status.params.roiX0);       // 14
	serializer.write<uint16_t>(status.params.roiY0);       // 16
	serializer.write<uint16_t>(status.params.roiSize);     // 18
	serializer.write<uint16_t>(status.params.afAlgType);   // 20
	serializer.write<uint16_t>(status.params.afAvgNum);    // 22
	serializer.write<uint16_t>(status.params.afParam1);    // 24
	serializer.write<uint16_t>(status.params.afParam2);    // 26
	serializer.write<int32_t>(status.focusValue);          // 30
	serializer.write<double>(status.focusFuncValue);       // 38
	serializer.write<uint16_t>(status.params.mode);        // 42
	*streamLen = serializer.streamLen();

    cv::MatConstIterator_<TRawFrame::TPixel> srcBegin = subFrame.begin<TRawFrame::TPixel>();
    cv::MatConstIterator_<TRawFrame::TPixel> srcEnd   = subFrame.end<TRawFrame::TPixel>();
    TRawFrame::TPixel* dstBegin = reinterpret_cast<TRawFrame::TPixel*>(dst + *streamLen);
    TRawFrame::TPixel* dstEnd = std::copy(srcBegin,srcEnd,dstBegin);
    *streamLen += (dstEnd - dstBegin)*sizeof(TRawFrame::TPixel);

    #if 0
        if(channelId == 1) {
            double s1 = cv::sum(subFrame)[0];
            double s2 = std::accumulate(dstBegin,dstEnd,0.0);

            if(s1 != s2) {
                qDebug() << s1 << s2;
            }
        }
    #endif

    return true;
}
#endif

//-----------------------------------------------------------------------------
bool TBaseLensController::checkRoi(const TAfParams* params)
{
    bool roiSizeOk = false;
    for(unsigned k = 0; k < RoiSizeNum; ++k) {
        if((params->roiSize == RoiSize[k]) && (params->roiSize <= MaxRoiSize)) {
            roiSizeOk = true;
            break;
        }
    }
    if(!roiSizeOk)
        return false;

    uint16_t xSize;
    uint16_t ySize;
    getFrameSize(xSize,ySize);

    if((params->roiX0 + params->roiSize) > xSize)
        return false;
    if((params->roiY0 + params->roiSize) > ySize)
        return false;
    return true;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::checkParams(const TAfParams* params)
{
    if((params->mode >= ModeNum) || (params->afCallbackMode >= CallbackModeNum))
        return false;

    if((params->mode == RLibLensDirectMode) && (params->afCallbackMode != RLibAfCallbackAlways))
        return true;

    if(!checkRoi(params))
        return false;

    //--- check afAlgType
    //--- check afAvgNum

    return true;
}

//-----------------------------------------------------------------------------
bool TBaseLensController::getRoiMat(TBaseMsgWrapperPtr framePtr, cv::Mat& roiMat)
{
    const int RestricdetPixSize = 2;
    TBaseFrame* frame;
    TRawFrame::TPixel* pixelBuf;

    if(framePtr && (frame = checkMsg<TBaseFrame>(framePtr)) && (pixelBuf = frame->getPixelBuf<TRawFrame>()) && (frame->pixelSize() == RestricdetPixSize)) {
        cv::Mat fullMat(frame->height(),frame->width(),CV_16UC1,pixelBuf);
        cv::Rect roi(mParams.roiX0,mParams.roiY0,mParams.roiSize,mParams.roiSize);
        roiMat = fullMat(roi);
        return true;
    } else {
        return false;
    }
}

//-----------------------------------------------------------------------------
AfReplyNotify TBaseLensController::getAfCallback()
{
    return mParent->getParams()->afReplyNotify;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
uint8_t TThermoLensController::checkSum(const uint8_t* buf, unsigned bufLen)
{
    uint16_t sum = 0;
	for(unsigned k = 0; k < (bufLen-1); ++k) {
        sum += buf[k];
    }
    return (sum & 0xFF);
}

//-----------------------------------------------------------------------------
TThermoLensController::TThermoLensController(TLibR* parent) :
                    TBaseLensController(parent, L"ThermoLensControllerThread" )
{
    //---
	mParams.mode           = RLibLensDirectMode;
	mParams.afCallbackMode = RLibAfCallbackNormal;
	mParams.roiSize = 128;
    mParams.roiX0 = CfgDefs::ThermoScreenFrameWidth/2  - mParams.roiSize/2;
    mParams.roiY0 = CfgDefs::ThermoScreenFrameHeight/2 - mParams.roiSize/2;
	mParams.afAlgType = TENG;
    mParams.afAvgNum  = 1;
    mParams.afParam1  = 0;
    mParams.afParam2  = 0;
    //---

    mPreconditioner   = new TBasePreconditioner;
	mFuncFocusAlg     = mFuncFocusAlgSet[mParams.afAlgType];
    mAfInitializer    = new TThermoAfInitializer(this);
    mMotionController = new TThermoLensMotionController(this);

    #if 1
        qDebug() << "TThermoAfController" << mParams.roiX0 << mParams.roiY0;
    #endif
}

//-----------------------------------------------------------------------------
TThermoLensController::~TThermoLensController()
{
    threadFinish();
}

//-----------------------------------------------------------------------------
void TThermoLensController::afCallback(TAfStatus* afStatus)
{
    AfReplyNotify afReplyNotify = getAfCallback();
    if(afReplyNotify /*&& (mParams.afCallbackMode == RLibAfCallbackAlways)*/) {
        (*afReplyNotify)(CfgDefs::ThermoStreamId,afStatus);
    }
}

//-----------------------------------------------------------------------------
void TThermoLensController::computeFocusRange()
{
	uint16_t fov = lensInfo().fov;
	if(fov < 19000) {
		#if defined(AF_TEST_INFO)
			qDebug() << "[OPHIR][ERROR] Bad Fov (too low):" << fov;
		#endif
		fov = 19000;
	}
	if(fov > 32005) {
		#if defined(AF_TEST_INFO)
			qDebug() << "[OPHIR][ERROR] Bad Fov (too high):" << fov;
		#endif
		fov = 32005;
	}

	#if (WORK_MODE == WORK_APPLICATION)
		const double MaxFocus = 5.88e-9*fov*fov*fov - 3.44e-4*fov*fov + 6.79*fov - 4.51e4;
	#else
		const double MaxFocus = 1000;
	#endif
	#if 0
		const double a = 1.3;
		const int nPoints = log(MaxFocus)/log(a);
		const int startN = 1;
	#else
		const double a  = (MaxFocus > 6000) ? 1.1 : 1.2; // default 8000
		const int StartN = (a < 1.2) ? 50 : 21;
		const int nPoints = log(MaxFocus)/log(a);
		const int startN = (nPoints > (StartN+5)) ? StartN : 1;
	#endif
	mFocusPointNum = 0;
	mDesiredFocusArray[mFocusPointNum] = 0;
	for(int n = startN; n <= nPoints; ++n) {
		int32_t focusCmd = pow(a,n);
		if(focusCmd != mDesiredFocusArray[mFocusPointNum]) {
			++mFocusPointNum;
			mDesiredFocusArray[mFocusPointNum] = focusCmd;
		}
	}
	mDesiredFocusArray[++mFocusPointNum] = MaxFocus;

	#if defined(AF_TEST_INFO)
		qDebug() << "";
		qDebug() << "[INFO] [TThermoLensController::computeFocusRange] +++";
		qDebug() << "fov:" << fov;
		qDebug() << "MaxFocus:"  << MaxFocus;
		qDebug() << "a:" << a;
		qDebug() << "nPoints:"  << nPoints;
		qDebug() << "startN:" << startN;
		for(unsigned n = 0; n < mFocusPointNum; ++n) {
            qDebug() << "n:" << n << "cmdFocus:" << mDesiredFocusArray[n];
        }
		qDebug() << "[INFO] [TThermoLensController::computeFocusRange] ---";
		qDebug() << "";
	#endif
}

//-----------------------------------------------------------------------------
void TThermoLensController::setLensInfo(const uint8_t* buf, int /*cmdNum*/)
{
    mLensInfo.status      = buf[2];
    mLensInfo.fov         = (static_cast<uint16_t>(buf[4]) << 8) + buf[3];
    mLensInfo.focus       = (static_cast<uint16_t>(buf[6]) << 8) + buf[5];
    mLensInfo.state       = buf[7];
    mLensInfo.temperature = buf[8] - 50;
    mLensInfo.error       = buf[9];
    mLensInfo.opTime      = (static_cast<uint16_t>(buf[11]) << 8) + buf[10];
    mLensInfo.subRev      = buf[12];
}

//-----------------------------------------------------------------------------
void TThermoLensController::debugPrintLensInfo(QString header)
{
	qDebug() << header << "channel" << CfgDefs::ThermoStreamId;
    if(mLensInfo.status != 0x0E) {
        qDebug() << "[WARN!] status:"  << hex << mLensInfo.status;
    } else {
        qDebug() << "status:"  << hex << mLensInfo.status;
    }
    qDebug() << "fov:"     << dec << mLensInfo.fov;
    qDebug() << "focus:"   << dec << mLensInfo.focus;
    qDebug() << "state:"   << hex << mLensInfo.state;
    qDebug() << "temp:"    << dec << mLensInfo.temperature;
    qDebug() << "error:"   << hex << mLensInfo.error;
    qDebug() << "op time:" << dec << mLensInfo.opTime;
    qDebug() << "sub rev:" << dec << mLensInfo.subRev;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
uint8_t TVideoLensController::checkSum(const uint8_t* buf, unsigned bufLen)
{
	uint16_t sum = 0;
	for(unsigned k = 0; k < (bufLen-2); ++k) {
		sum += buf[k];
	}
	return (sum & 0xFF);
}

//-----------------------------------------------------------------------------
TVideoLensController::TVideoLensController(TLibR* parent) :
                    TBaseLensController(parent, L"VideoLensControllerThread" )
{
    //---
	mParams.mode           = RLibLensDirectMode;
	mParams.afCallbackMode = RLibAfCallbackNormal;
	mParams.roiSize = 128;
    mParams.roiX0 = CfgDefs::VideoScreenFrameWidth/2  - mParams.roiSize/2;
    mParams.roiY0 = CfgDefs::VideoScreenFrameHeight/2 - mParams.roiSize/2;
	mParams.afAlgType = TENG;
    mParams.afAvgNum  = 1;
    mParams.afParam1  = 0;
    mParams.afParam2  = 0;
    //---

    mPreconditioner   = new TVideoPreconditioner(MaxRoiSize);
	mFuncFocusAlg     = mFuncFocusAlgSet[mParams.afAlgType];
	mAfInitializer    = new TVideoAfInitializer(this);
    mMotionController = new TVideoLensMotionController(this);

    #if 1
        qDebug() << "TVideoAfController" << mParams.roiX0 << mParams.roiY0;
    #endif
}

//-----------------------------------------------------------------------------
TVideoLensController::~TVideoLensController()
{
    threadFinish();
}

//-----------------------------------------------------------------------------
bool TVideoLensController::isLensControlCmd(const uint8_t* cmd, unsigned cmdLen) const
{
    return (cmdLen >= (DeviceFieldPos+1)) && (cmd[DeviceFieldPos] == LensId);
}

//-----------------------------------------------------------------------------
void TVideoLensController::afCallback(TAfStatus* afStatus)
{
    AfReplyNotify afReplyNotify = getAfCallback();
    if(afReplyNotify /*&& (mParams.afCallbackMode == RLibAfCallbackAlways)*/) {
        (*afReplyNotify)(CfgDefs::VideoStreamId,afStatus);
	}
}

//-----------------------------------------------------------------------------
void TVideoLensController::computeFocusRange()
{
	const uint16_t FocusMidPoint = (MaxFocus + MinFocus)/2;
	const bool FocusScanFromZero = (mLensInfo.focus <= FocusMidPoint) ? true : false;
	const uint16_t FocusInfinityStart = focusInfinityStart();

	/*TEST*/ //qDebug() << "[TVideoLensController::computeFocusRange]" << mLensInfo.focus << FocusMidPoint << FocusScanFromZero;

	mFocusPointNum = 40;
	int32_t dF = (MaxFocus - MinFocus)/mFocusPointNum;
	for(unsigned n = 0; n < mFocusPointNum; ++n) {
		mDesiredFocusArray[n] = MinFocus + n*dF;
	}
}

//-----------------------------------------------------------------------------
uint16_t TVideoLensController::focusInfinityStart() const
{
	if(mLensInfo.fov > 3000)
		return 700;
	else
		return 300;
}

//-----------------------------------------------------------------------------
void TVideoLensController::setLensInfo(const uint8_t* buf, int cmdNum)
{	if(cmdNum == 1) {
		mLensInfo.focusRef = (static_cast<uint16_t>(buf[10]) << 8) + buf[9];
		mLensInfo.focus    = (static_cast<uint16_t>(buf[12]) << 8) + buf[11];
		mLensInfo.fovRef   = (static_cast<uint16_t>(buf[6]) << 8) + buf[5];
		mLensInfo.fov      = (static_cast<uint16_t>(buf[8]) << 8) + buf[7];
		mLensInfo.status   = buf[4];
	} else {
		if(cmdNum == 2)
			mLensInfo.focus = (static_cast<uint16_t>(buf[5]) << 8) + buf[4];
		else
			mLensInfo.focus = (static_cast<uint16_t>(buf[11]) << 8) + buf[10];
	}
}

//-----------------------------------------------------------------------------
void TVideoLensController::debugPrintLensInfo(QString header)
{
	qDebug() << header << "channel" << CfgDefs::VideoStreamId;
	qDebug() << "status:" << mLensInfo.status;
	qDebug() << "fov:"    << mLensInfo.fov << "fovRef:" << mLensInfo.fovRef;
	qDebug() << "focus:"  << mLensInfo.focus << "focusRef:" << mLensInfo.focusRef;
}
