//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QtGlobal>

#define RLIB_EXPORT
#include "libR.h"
#include "app/libRImpl.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

RLibStatus       libRinit(const TRLibDevParams* params) { return  TLibR::init(params); }
RLibStatus       libRcleanUp() { return  TLibR::cleanUp(); }
RLibStatus       libRgetStatus() { return TLibR::getStatus(); }
bool             libRgetFrame(unsigned channelId, TRLibFrame* frame, int timeout) { return TLibR::getFrame(channelId,frame,timeout); }
bool             libRstreamCmd(unsigned channelId, uint16_t cmd)  { return TLibR::streamCmd(channelId,cmd); }
void             libRsetVideoStreamMode(RVideoStreamMode mode) { TLibR::setVideoStreamMode(mode); }
RVideoStreamMode libRgetVideoStreamMode() { return TLibR::getVideoStreamMode(); }
bool             libRChannelSendCmd(unsigned channelId, uint8_t* cmd, unsigned cmdLen) { return TLibR::channelSendCmd(channelId, cmd, cmdLen, false); }
unsigned         libRChannelGetReply(unsigned channelId, uint8_t* reply, unsigned maxReplyLen) { return TLibR::channelGetReply(channelId,reply, maxReplyLen); }
bool			 libRSetLensControlMode(unsigned channelId, const TAfParams* params) { return TLibR::setLensControlMode(channelId, params); }
bool			 libRGetLensControlMode(unsigned channelId, TAfParams* params) { return TLibR::getLensControlMode(channelId, params); }
bool             libRPlatformSendCmd(uint8_t* cmd, unsigned cmdLen) { return TLibR::platformSendCmd(cmd,cmdLen); }
unsigned         libRPlatformGetReply(uint8_t* reply, unsigned maxReplyLen) { return TLibR::platformGetReply(reply,maxReplyLen); }

