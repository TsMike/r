//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/TestStreamGen.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TChannelStreamGen::TChannelStreamGen(const TChannelStreamGen::TParams& params) :
	mChannelNum(params.ChannelNum),
	mWidth(params.Width),
	mHeight(params.Height),
	mGenFunc(params.GenFunc)
{
	reset();
}

//-----------------------------------------------------------------------------
void TChannelStreamGen::reset()
{
	mCounterX     = 0;
	mCounterY     = 0;
	mFrameCounter = 0;
	mLineCounter  = 0;
	mPixCounter   = 0;
	mState        = SOF1;
    #if defined(INSERT_DFM_TEST)
        mTestDFMState = 0;
    #endif
}

//#define INSERT_ERROR_TEST
//-----------------------------------------------------------------------------
quint16 TChannelStreamGen::genNextData()
{
    #if defined(INSERT_ERROR_TEST)
        static unsigned counter;
        const unsigned ErrPeriod = 50000;
    #endif

	TData data = 0x0000;

	switch (mState)
	 {
	  case SOF1:
          #if !defined(INSERT_DFM_TEST)
            data   = (VFM | SOF);
            mState = SOF2;
          #else
            if(mTestDFMState == 0) {
                data = genDFM();
                mTestDFMState = 1;
            } else {
                mTestDFMState = 0;
                data   = (VFM | SOF);
                mState = SOF2;
            }
          #endif
		  break;

	  case SOF2:
		  data         = (mFrameCounter & CntrMask);
		  mLineCounter = 0;
		  mState       = SOL1;
		  break;

	  case SOL1:
          #if !defined(INSERT_DFM_TEST)
            data   = (VFM | SOL);
            mState = SOL2;
            #if defined(INSERT_ERROR_TEST)
                if(++counter == ErrPeriod) {
                    counter = 0;
                    data = 0x5A;
                }
            #endif
          #else
            if(mTestDFMState == 0) {
                data = genDFM();
                mTestDFMState = 1;
            } else {
                mTestDFMState = 0;
                data   = (VFM | SOL);
                mState = SOL2;
                #if defined(INSERT_ERROR_TEST)
                    if(++counter == ErrPeriod) {
                        counter = 0;
                        data = 0x5B;
                    }
                #endif
            }
          #endif
          break;

	  case SOL2:
		  data        = mLineCounter;
		  mPixCounter = 0;
		  mState      = PIX;
		  break;

	  case PIX:
		  data = (this->*mGenFunc)();
		  if(++mPixCounter >= mWidth) {
			  if(++mLineCounter >= mHeight) {
				  if(++mCounterX > mWidth)
				   mCounterX = 0;
				  ++mCounterY;
				  mFrameCounter = (mFrameCounter + 1) & CntrMask;
				  mState = SOF1;
			  } else {
				  mState = SOL1;
			  }
		  }
		  break;

	  default:
	   break;
	 }
	return data;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TDevStreamGen::TDevStreamGen(const TChannelStreamGen::TParams* channelParams, const unsigned* channelWorkTime, unsigned channelNum) :
	ChannelNum(channelNum),
	mChannelStreamGen(0),
	mChannelCumSumWorkTime(0),
	mChunkCounter(0)

{
	mChannelCumSumWorkTime = new unsigned [ChannelNum];
	mChannelStreamGen = new TChannelStreamGen* [ChannelNum];
	unsigned channelCumSumWorkTime = 0;

	for(unsigned i = 0; i < ChannelNum; ++i) {
		mChannelStreamGen[i] = new TChannelStreamGen(channelParams[i]);
		channelCumSumWorkTime += channelWorkTime[i];
		mChannelCumSumWorkTime[i] = channelCumSumWorkTime;
	}

	reset();
	#if 1
		qDebug() << qPrintable(QString("[DevStreamGen]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
	#endif
}

//-----------------------------------------------------------------------------
TDevStreamGen::~TDevStreamGen()
{
	for(unsigned i = 0; i < ChannelNum; ++i) {
		delete mChannelStreamGen[i];
	}
	delete [] mChannelStreamGen;

	delete [] mChannelCumSumWorkTime;
	#if 1
		qDebug() << qPrintable(QString("[DevStreamGen]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
	#endif
}

//-----------------------------------------------------------------------------
void TDevStreamGen::reset()
{
	for(unsigned k = 0; k < ChannelNum; ++k)
		mChannelStreamGen[k]->reset();
	mChunkCounter = 0;
}


//-----------------------------------------------------------------------------
unsigned TDevStreamGen::currChannel()
{
	unsigned channel;
	for(channel = 0; channel < ChannelNum; ++channel) {
		if(mChunkCounter < mChannelCumSumWorkTime[channel])
			break;
	}
	if(++mChunkCounter >= mChannelCumSumWorkTime[ChannelNum-1])
		mChunkCounter = 0;
	return channel;
}

//-----------------------------------------------------------------------------
void TDevStreamGen::fillChunk(TData* chunk, unsigned chunkLen)
{
	unsigned channel = currChannel();
	*chunk++ =  mChannelStreamGen[channel]->genDFM();
	while(--chunkLen) {
		*chunk++ = mChannelStreamGen[channel]->genNextData();
	}
}

//-----------------------------------------------------------------------------
bool TDevStreamGen::fillBuf(TData* buf, unsigned bufLen, unsigned chunkLen)
{
	unsigned chunkNum = bufLen/chunkLen;

	if(chunkNum*chunkLen == bufLen) {
		while(chunkNum--) {
			fillChunk(buf,chunkLen);
			buf += chunkLen;
		}
		return true;
	} else {
		qDebug() << "[WARN] [TDevStreamGen::fillBuf]";
		std::memset(buf, 0, bufLen*sizeof(TData));
		return false;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TDevBundleStreamGen::~TDevBundleStreamGen()
{
	while(!mDevList.isEmpty()) {
		delete mDevList.takeFirst().mDevGen;
	}
	qDebug() << "~TDevBundleStreamGen";
}

//-----------------------------------------------------------------------------
void TDevBundleStreamGen::insertDevGen(
									   TRoutingPolicy::TNetAddr srcDev,
									   TRoutingPolicy::TNetAddr dstDev,
									   TMsgWrapperPoolQueue* bufPool,
									   unsigned devWorkTime,
									   const TChannelStreamGen::TParams* channelParams,
									   const unsigned* channelWorkTime,
									   unsigned channelNum,
									   unsigned chunkLen
									   )
{
	TDevParams devParams = {
							srcDev,
							dstDev,
							bufPool,
							devWorkTime,
							new TDevStreamGen(channelParams, channelWorkTime, channelNum),
							chunkLen
						   };
	mDevList.append(devParams);
}

//-----------------------------------------------------------------------------
void TDevBundleStreamGen::fillDrvBuf(TDrvBufPtr& drvBufPtr)
{
	if(bundleSize()) {
		TDevParams devParams = mDevList.at(mCurrDev);
		drvBufPtr->setNetSrc(devParams.mSrcDev);
		drvBufPtr->setNetDst(devParams.mDstDev);
		TRawBuf* drvBufProxy = getMsg<TRawBuf>(drvBufPtr);
		devParams.mDevGen->fillBuf(drvBufProxy->getDataBuf<TDevBundleStreamGen::TData>(),drvBufProxy->bufSize<TDevBundleStreamGen::TData>(),devParams.mChunkLen);
		drvBufProxy->setDataLen<TDevBundleStreamGen::TData>(drvBufProxy->bufSize<TDevBundleStreamGen::TData>());
		//qDebug() << "slon dev:" << mCurrDev << "slot:" << mCurrWorkSlot << "src:" << devParams.mSrcDev;
		if(++mCurrWorkSlot >= devParams.mDevWorkTime) {
			mCurrWorkSlot = 0;
			if(++mCurrDev >= bundleSize()) {
				mCurrDev = 0;
			}
		}
	}
}

//-----------------------------------------------------------------------------
TDrvBufPtr TDevBundleStreamGen::genDrvBuf()
{
	TDrvBufPtr drvBufPtr;
	if(bundleSize()) {
		TDevParams devParams = mDevList.at(mCurrDev);
		#if 0
			static int PrevBufNum = 0;
			int bufNum = devParams.mBufPool->size();
			if((bufNum - PrevBufNum > 2) || (PrevBufNum == 0) || (bufNum < PrevBufNum)) {
				qDebug() << "[TDevBundleStreamGen::genDrvBuf] free buffers (1):" << PrevBufNum << bufNum;
				PrevBufNum = bufNum;
				}
		#endif
		if(devParams.mBufPool->get(drvBufPtr)) {
			drvBufPtr->setNetSrc(devParams.mSrcDev);
			drvBufPtr->setNetDst(devParams.mDstDev);
			TRawBuf* drvBufProxy = getMsg<TRawBuf>(drvBufPtr);
			devParams.mDevGen->fillBuf(drvBufProxy->getDataBuf<TDevBundleStreamGen::TData>(),drvBufProxy->bufSize<TDevBundleStreamGen::TData>(),devParams.mChunkLen);
			drvBufProxy->setDataLen<TDevBundleStreamGen::TData>(drvBufProxy->bufSize<TDevBundleStreamGen::TData>());
			//qDebug() << "[genDrvBuf] dev:" << mCurrDev << "slot:" << mCurrWorkSlot << "srcAddr:" << devParams.mSrcDev;
			if(++mCurrWorkSlot >= devParams.mDevWorkTime) {
				mCurrWorkSlot = 0;
				if(++mCurrDev >= bundleSize()) {
					mCurrDev = 0;
				}
			}
		} else {
			qDebug() << "[WARN] TDevBundleStreamGen::genDrvBuf - no free buffer";
		}
	}
	return drvBufPtr;
}

