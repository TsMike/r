#if !defined(LIB_R_H)
#define LIB_R_H

#include "libRDefs.h"

#ifdef __cplusplus
    extern "C" {
#endif

#ifdef RLIB_EXPORT
    #define RLIB_DLL_API Q_DECL_EXPORT         //__declspec(dllexport)
#else
    #define RLIB_DLL_API __declspec(dllimport) //  Q_DECL_IMPORT
#endif

//-----------------------------------------------------------------------------
RLIB_DLL_API RLibStatus       libRinit(const TRLibDevParams* params);
RLIB_DLL_API RLibStatus       libRcleanUp();
RLIB_DLL_API RLibStatus       libRgetStatus();
RLIB_DLL_API bool             libRgetFrame(unsigned channelId, TRLibFrame* frame, int timeout = -1);
RLIB_DLL_API bool             libRstreamCmd(unsigned channelId, uint16_t cmds);
RLIB_DLL_API void             libRsetVideoStreamMode(RVideoStreamMode mode);
RLIB_DLL_API RVideoStreamMode libRgetVideoStreamMode();
RLIB_DLL_API bool             libRChannelSendCmd(unsigned channelId, uint8_t* cmd, unsigned cmdLen);
RLIB_DLL_API unsigned         libRChannelGetReply(unsigned channelId, uint8_t* reply, unsigned maxReplyLen);
RLIB_DLL_API bool			  libRSetLensControlMode(unsigned channelId, const TAfParams* params);
RLIB_DLL_API bool			  libRGetLensControlMode(unsigned channelId, TAfParams* params);
RLIB_DLL_API bool             libRPlatformSendCmd(uint8_t* cmd, unsigned cmdLen);
RLIB_DLL_API unsigned         libRPlatformGetReply(uint8_t* reply, unsigned maxReplyLen);
//-----------------------------------------------------------------------------

#ifdef __cplusplus
    }
#endif

#endif // LIB_R_H

