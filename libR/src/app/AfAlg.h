#if !defined(AF_ALG_H)
#define AF_ALG_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/tthread.h"
#include "app/Frame.h"
#include "app/libR.h"
#include "app/DrvBuf.h"
#include <opencv2/core/core.hpp>


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBasePreconditioner
{
    public:
		TBasePreconditioner() {}
        virtual ~TBasePreconditioner() {}
		virtual cv::Mat compute(const cv::Mat& inMat) {return inMat; }
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TVideoPreconditioner : public TBasePreconditioner
{
    public:
        TVideoPreconditioner(unsigned maxSize);
        virtual ~TVideoPreconditioner() {}
		virtual cv::Mat compute(const cv::Mat& inMat);

    private:
        cv::Mat mOutMat;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseFuncFocusAlg
{
    public:
        TBaseFuncFocusAlg() {}
        virtual ~TBaseFuncFocusAlg() {}
		virtual double compute(const cv::Mat& inMat) { return cv::sum(inMat)[0]; }
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TTengAlg : public TBaseFuncFocusAlg
{
	public:
		static const int KernelSize = 3;

		TTengAlg(unsigned roiSizeNum, const uint16_t* roiSize);
		virtual ~TTengAlg();
		virtual double compute(const cv::Mat& inMat);

	private:
		cv::Mat getRoiFrame(unsigned roiSize, int& idx);

		unsigned mRoiSizeNum;
		unsigned mMaxRoiSize;
		cv::Mat* mRoiFrame;
		cv::Mat  mGx;
		cv::Mat  mGy;
		cv::Mat  mFm;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TGlvaAlg : public TBaseFuncFocusAlg
{
	public:
		TGlvaAlg() : TBaseFuncFocusAlg() {}
		virtual ~TGlvaAlg() {}
		virtual double compute(const cv::Mat& inMat);
};

//http://stackoverflow.com/questions/21236373/to-apply-sharpness-function-over-a-certain-region-of-image-opencv
//http://docs.opencv.org/doc/tutorials/imgproc/imgtrans/sobel_derivatives/sobel_derivatives.html

#endif // AF_ALG_H


