#if !defined(DRV_LAYER_H)
#define DRV_LAYER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"

#include "app/tthread.h"
#include "app/DrvBuf.h"
#include "udp_lib/udp_defs.h"
#include "libRDefs.h"

#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
	#include <QtCore/QTimer>
	#include "app/TestStreamGen.h"
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TLibR;

class TDrvLayer : public TThread
{
 Q_OBJECT

 signals:
	void sendDrvBufSignal(TDrvBufPtr);
	void sendDrvBufAuxSignal(TDrvBufPtr);
    void sendChannelCmdSignal(TDrvBufPtr);

 protected:
	virtual bool onExec();
	void threadFinish() { setThreadExit(); mSemaphore.release();  TThread::threadFinish(); }

 public:
    explicit TDrvLayer(TLibR* parent);
	~TDrvLayer();
	void activate();

 private:
	#if (WORK_MODE == WORK_APPLICATION)
		static const unsigned UdpLibTimeout = INFINITE;

		bool configThermoStream();
        bool configVideoStream();
        bool configThermoLens();
		bool configVideoDevGroup();
        bool configPlatform();
    #endif
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
        void configThermoTestGen();
        void configVideoTestGen();

		TDevBundleStreamGen mDevBundleStreamGen;
	#endif
    static CmdReplyNotify getCmdReplyNotify();
	static void thermoCmdReplyCallback(const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer, UDP_LIB::TDirection dir);
	static void videoDevGroupCmdReplyCallback(const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer, UDP_LIB::TDirection dir);
	static void platformCmdReplyCallback(const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer, UDP_LIB::TDirection dir);

    TLibR*     mParent;
    QSemaphore mSemaphore;
};

#endif // DRV_LAYER_H


