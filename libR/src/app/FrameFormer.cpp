//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/FrameFormer.h"
#include "libRDefs.h"
#include "libRImpl.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
int TBaseFrameFormer::formFrames()
{
	if(!mInQueue)
		return 0;

	int nFrames = 0;
	while(mInQueue->size()) {
		TBaseMsgWrapperPtr inFramePtr;
		mInQueue->get(inFramePtr);
		if(mFramePool && mOutQueue) {
			TBaseMsgWrapperPtr outFramePtr;
			if(mFramePool->get(outFramePtr)) {
				TBaseFrame* inFrame  = checkMsg<TBaseFrame>(inFramePtr);
				TBaseFrame* outFrame = checkMsg<TBaseFrame>(outFramePtr);
				if(checkFrameCompatibility(inFrame,outFrame)) {
					if(formFrame(inFrame, outFrame)) {
						outFramePtr->setNetPoints(inFramePtr->netSrc(),inFramePtr->netDst());
                        //qDebug() << "slon" << inFramePtr->netSrc() << inFramePtr->netDst();
						outFramePtr->setMsgId(inFramePtr->msgId());
						mOutQueue->put(outFramePtr);
						++nFrames;
					} else {
						#if !defined(MSG_SELF_RELEASE)
							releaseMsg(outFramePtr);
						#endif
					}
				} else {
					#if !defined(MSG_SELF_RELEASE)
						releaseMsg(outFramePtr);
					#endif
				}
			}
		}
		#if !defined(MSG_SELF_RELEASE)
			releaseMsg(inFramePtr);
		#endif
	}
	return nFrames;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TThermoFrameFormer::formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame)
{
	const int FrameSize = inFrame->size();

	TRawFrame::TPixel* inPixelBuf  = inFrame->getPixelBuf<TRawFrame>();
    TScreenFrameGray::TPixel* outPixelBuf = outFrame->getPixelBuf<TScreenFrameGray>();
	if(!inPixelBuf || !outPixelBuf)
		return false;

	for(int n = 0; n < FrameSize; ++n) {
        *outPixelBuf++ = (*inPixelBuf++) >> 2;
	}

	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

TVideoFrameFormer::TVideoFrameFormer(TMsgWrapperPoolQueue* framePool, TMsgWrapperPoolQueue* inQueue, TMsgWrapperPoolQueue* outQueue) :
                                                                                    TBaseFrameFormer(framePool, inQueue, outQueue),
                                                                                    #if defined(USE_TEST_DEMOSAIC_IMG)
                                                                                        mFrameNum(0),
                                                                                        mTestImg(),
                                                                                    #endif
																					mGrayFrameMat(CfgDefs::VideoScreenFrameHeight,CfgDefs::VideoScreenFrameWidth,CV_16UC1),
                                                                                    mOutFrameMat(CfgDefs::VideoScreenFrameHeight,CfgDefs::VideoScreenFrameWidth,CV_16UC4)

{
    #if defined(USE_TEST_DEMOSAIC_IMG)
        cv::Mat srcImg = cv::imread("mandi.tif",CV_LOAD_IMAGE_ANYDEPTH);
        if(srcImg.data) {
           srcImg.convertTo(mTestImg,CV_16UC1);
           mTestImg *= 4;
        }
    #endif
}

#if defined(USE_TEST_DEMOSAIC_IMG)
//-----------------------------------------------------------------------------
void TVideoFrameFormer::genBayerTestFrame(TRawFrame::TPixel* inFrame, cv::Mat& testFrame, unsigned pixNum)
{
    if(pixNum != testFrame.cols*testFrame.rows)
        return;

    for(int nRow = 0; nRow < testFrame.rows; ++nRow) {
        TRawFrame::TPixel* srcRow = testFrame.ptr<TRawFrame::TPixel>(nRow);
        TRawFrame::TPixel* dstRow = inFrame + nRow*CfgDefs::VideoScreenFrameWidth;
        memcpy(dstRow,srcRow,CfgDefs::VideoScreenFrameWidth*sizeof(TRawFrame::TPixel));
    }
}
#endif

//-----------------------------------------------------------------------------
//#define TEST_PERFORMANCE
bool TVideoFrameFormer::formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame)
{
    TRawFrame::TPixel* inPixelBuf  = inFrame->getPixelBuf<TRawFrame>();
    TScreenFrame::TPixel* outPixelBuf = outFrame->getPixelBuf<TScreenFrame>();
    if(!inPixelBuf || !outPixelBuf)
        return false;

    #if defined(USE_TEST_DEMOSAIC_IMG)
        const int FrameSize = inFrame->size();
        const unsigned FramePeriod = 1000;
        //double tStart = (double)cv::getTickCount();
        cv::Mat testFrame(mTestImg,cv::Rect(mFrameNum+600,500/*mFrameNum*/,CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight));
        genBayerTestFrame(inPixelBuf,testFrame,FrameSize);
        //double tFinish = (double)cv::getTickCount();
        //double tExpired = (tFinish - tStart)/cv::getTickFrequency();
        mFrameNum += 2;
        if(mFrameNum == FramePeriod) {
            mFrameNum = 0;
        }
        //qDebug() << "***" << mFrameNum << testFrame.cols << testFrame.rows << testFrame.depth() << testFrame.u->refcount << tExpired*1000.0;
    #endif

    RVideoStreamMode mode = TLibR::getVideoStreamMode();
    #if defined(TEST_PERFORMANCE)
        static double tTotal = 0.0;
        static unsigned FrameNum = 0;
        double tStart = (double)cv::getTickCount();
    #endif
    //---
    cv::Mat outScreenMat(CfgDefs::VideoScreenFrameHeight,CfgDefs::VideoScreenFrameWidth,CV_8UC4,outPixelBuf);
    cv::Mat inFrameMat(CfgDefs::VideoScreenFrameHeight,CfgDefs::VideoScreenFrameWidth,CV_16UC1,inPixelBuf);
    switch(mode) {
        case RLibGrayScaleVideo:
            #if defined(USE_TEST_DEMOSAIC_IMG)
				cv::cvtColor(inFrameMat,mGrayFrameMat,CV_BayerRG2GRAY);
            #else
				cv::cvtColor(inFrameMat,mGrayFrameMat,CV_BayerGR2GRAY);
            #endif
			cv::cvtColor(mGrayFrameMat,mOutFrameMat,CV_GRAY2BGRA,4);
            break;
        case RLibColorVideo:
            #if defined(USE_TEST_DEMOSAIC_IMG)
                cv::cvtColor(inFrameMat,mOutFrameMat,CV_BayerRG2BGR,4);
            #else
                cv::cvtColor(inFrameMat,mOutFrameMat,CV_BayerGR2BGR,4);
            #endif
            break;
        case RLibRawVideo:
        default:
            cv::cvtColor(inFrameMat,mOutFrameMat,CV_GRAY2BGRA,4);
            break;
    }

    #if 0 // TEST
        typedef cv::Vec<uint16_t,4> TPixel;
        cv::MatIterator_<TPixel> it    = mOutFrameMat.begin<TPixel>();
        cv::MatIterator_<TPixel> itEnd = mOutFrameMat.end<TPixel>();

        for( ; it != itEnd; ++it) {
            TPixel& pix = *it;
            //pix[0] *= .7);
            pix[1] *= 0.8;
            pix[2] *= 0.8;
        }
    #endif

    mOutFrameMat.convertTo(outScreenMat,CV_8UC4,0.25);
    //---
    #if defined(TEST_PERFORMANCE)
        double tFinish = (double)cv::getTickCount();
        double tExpired = (tFinish - tStart)/cv::getTickFrequency();
        tTotal = tTotal + tExpired;
        ++FrameNum;
        printf("avg time: %10.4f\n",tTotal/FrameNum);
    #endif

    return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

bool TInfoFrameFormer::PaletteInitialized = false;

const QRgb TInfoFrameFormer::BasePalette[BasePaletteSize] =
{
	qRgba(0x00,0x00,0x00,0x00), //  0: transparent
	qRgba(0xff,0x00,0x00,0xff), //  1: red
	qRgba(0x00,0xff,0x00,0xff), //  2: green
	qRgba(0x00,0x00,0xff,0xff), //  3: blue
	qRgba(0xff,0xff,0x00,0xff), //  4: yellow
	qRgba(0xff,0x00,0xff,0xff), //  5: magenta
	qRgba(0x00,0xff,0xff,0xff), //  6: cyan
	qRgba(0xff,0xff,0xff,0xff), //  7: white
	qRgba(0xff,0x7f,0x00,0xff), //  8: light brown
	qRgba(0xff,0x00,0x7f,0xff), //  9: dark pink
	qRgba(0x00,0x7f,0xff,0xff), // 10: dark blue
	qRgba(0x7f,0x00,0xff,0xff), // 11: purple
	qRgba(0x7f,0xff,0x00,0xff), // 12: acid
	qRgba(0x00,0xff,0x7f,0xff), // 13: light green
	qRgba(0x00,0x00,0x00,0xff), // 14: reserved
	qRgba(0x00,0x00,0x00,0xff)  // 15: reserved
};

QRgb TInfoFrameFormer::Palette[PaletteSize];

//-----------------------------------------------------------------------------
bool TInfoFrameFormer::formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame)
{
	const int FrameSize = inFrame->size();

	TRawFrame::TPixel* inPixelBuf  = inFrame->getPixelBuf<TRawFrame>();
	TScreenFrame::TPixel* outPixelBuf = outFrame->getPixelBuf<TScreenFrame>();
	if(!inPixelBuf || !outPixelBuf)
		return false;

	for(int n = 0; n < FrameSize; ++n) {
		unsigned colorIdx = *inPixelBuf++ & 0xFF;
		*outPixelBuf++ = Palette[colorIdx];
	}

	return true;
}

//-----------------------------------------------------------------------------
void TInfoFrameFormer::genPalette()
{
	for(unsigned idx = 0; idx < PaletteSize; ++idx) {
		QRgb baseColor = BasePalette[idx & ColorMask];
		unsigned intensity = (idx >> IntensityPos) & IntensityMask;
		unsigned alpha = genAlpha(idx);
		Palette[idx] = genColor(baseColor,intensity,alpha);
	}
}

//-----------------------------------------------------------------------------
unsigned TInfoFrameFormer::genAlpha(unsigned idx)
{
	if((idx & ColorMask) == 0)
		return 0x00;

	if(idx & AlphaMask)
		return 0x7f;

	return 0xff;
}

//-----------------------------------------------------------------------------
QRgb TInfoFrameFormer::genColor(QRgb baseColor, unsigned intensity, int alpha)
{
	if(intensity)
		intensity += 1;

	int red   = qRed(baseColor)*intensity/8;
	int green = qGreen(baseColor)*intensity/8;
	int blue  = qBlue(baseColor)*intensity/8;

	return qRgba(red,green,blue,alpha);
}
