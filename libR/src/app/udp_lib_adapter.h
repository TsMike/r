#if !defined(UDP_LIB_ADAPTER_H)
#define UDP_LIB_ADAPTER_H

#include <QtCore/QObject>
//#include <QtCore/QMap>
//#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"

#include "app/msg.h"
#include "app/tqueue.h"
#include "app/DrvBuf.h"
#include "udp_lib/udp_lib.h"

//#define TEST_UDP_STREAM

//-----------------------------------------------------------------------------
//------------------------------------------------------------------------------
class TStreamTester
{
	public:
        typedef uint16_t TData;

		TStreamTester(TData  increment = 1, TData startValue = 0) : mIncrement(increment), mValue(startValue) {}
		void fillBuf(uint8_t* buf, unsigned len);
		unsigned checkBuf(uint8_t* buf, unsigned len);

	private:
		const TData mIncrement;
		TData       mValue;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

class T_UDP_RxChannel
{
	friend class T_UDP_LibAdapter;

	public:
		struct TParams
		{
			UDP_LIB::TParams      params;
			TMsgWrapperPoolQueue* msgBufPool;
		};

	private:
		T_UDP_RxChannel(unsigned long hostAddr, unsigned hostPort, const T_UDP_RxChannel::TParams* params) :
															mHostAddr(hostAddr),
															mHostPort(hostPort),
															#if defined(TEST_UDP_STREAM)
																mStreamTester(),
															#endif
															mParams(*params) {}
		unsigned getTransferLen(const TRawBuf* buf, const UDP_LIB::Transfer& transfer) const;
		void receiveData();

		const unsigned long mHostAddr;
		const unsigned      mHostPort;
		#if defined(TEST_UDP_STREAM)
			TStreamTester   mStreamTester;
		#endif
		TParams             mParams;
};

//-----------------------------------------------------------------------------
class T_UDP_TxChannel
{
	friend class T_UDP_LibAdapter;

	public:
		struct TParams
		{
			UDP_LIB::TParams params;
			unsigned         txQueueLimit;
		};


	private:
		typedef TQueue<TBaseMsgWrapperPtr,TQueueSl,TWinCsGuard> TMsgQueue;

		T_UDP_TxChannel(unsigned long hostAddr, unsigned hostPort, const T_UDP_TxChannel::TParams* params) :
															mHostAddr(hostAddr),
															mHostPort(hostPort),
															#if defined(TEST_UDP_STREAM)
																mStreamGen(),
															#endif
															mParams(*params),
															mMsgQueue(),
															mGuard() {}
		CfgDefs::TNetAddr getPeerNetAddr() const;
		bool putToQueue(TBaseMsgWrapperPtr msg);
		bool tryTransfer(int callerId);
		unsigned getTransferLen(TRawBuf* buf) const;

		const unsigned long mHostAddr;
		const unsigned      mHostPort;
		#if defined(TEST_UDP_STREAM)
			TStreamTester   mStreamGen;
		#endif
		TParams             mParams;
		TMsgQueue           mMsgQueue;
		TWinCsGuard		    mGuard;
};

//-----------------------------------------------------------------------------
class T_UDP_LibAdapter : public QObject
{
	Q_OBJECT

	public:
		signals:
			void sendMsgSignal(TDrvBufPtr);

		public slots:
			void receiveMsgSlot(TBaseMsgWrapperPtr msg);

	public:
		typedef uint8_t TEthData;

		static unsigned long getIpAddr(CfgDefs::TNetAddr netAddr);
		static unsigned getPort(CfgDefs::TNetAddr netAddr);
		static CfgDefs::TNetAddr makeNetAddr(unsigned long ipAddr, unsigned portAddr);
		static CfgDefs::TNetAddr makeNetAddr(const UDP_LIB::TNetAddr& netAddr);
		static bool init();
		static void cleanUp();
		static T_UDP_LibAdapter* getInstance() { return mInstance; }
        T_UDP_RxChannel* getRxChannel(CfgDefs::TNetAddr hostAddr);
        T_UDP_TxChannel* getTxChannel(CfgDefs::TNetAddr hostAddr, CfgDefs::TNetAddr peerAddr);
		static UDP_LIB::TStatus createSocket(unsigned long hostAddr, unsigned hostPort, const T_UDP_RxChannel::TParams* rxParams, const T_UDP_TxChannel::TParams* txParams);
		static bool isValid()  { return getInstance() && getInstance()->mValid; }
        static void rxNotify(const UDP_LIB::TNetAddr& hostAddr, const UDP_LIB::TNetAddr& peerAddr, UDP_LIB::TDirection dir);
        static void txNotify(const UDP_LIB::TNetAddr& hostAddr, const UDP_LIB::TNetAddr& peerAddr, UDP_LIB::TDirection dir);

	private:
		typedef QMap<CfgDefs::TNetAddr,T_UDP_RxChannel*> T_USB_RxChannelMap;
		typedef QMap<CfgDefs::TNetAddr,T_UDP_TxChannel*> T_USB_TxChannelMap;

		explicit T_UDP_LibAdapter() : mRxChannelMap(), mTxChannelMap(), mValid(UDP_LIB::init() == UDP_LIB::Ok) {}
		~T_UDP_LibAdapter();

		static T_UDP_LibAdapter* mInstance;
		T_USB_RxChannelMap       mRxChannelMap;
		T_USB_TxChannelMap       mTxChannelMap;
		bool                     mValid;
};

#endif // UDP_LIB_ADAPTER_H

