//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#include <QtCore/qmath.h>
#include <QtCore/QDebug>

//#include "app/CfgDefs.h"
//#include "app/SysUtils.h"
#include "app/libRImpl.h"
#include "app/LensController.h"
#include "app/FrameFormerLayer.h"
#include "app/DrvLayer.h"
#include "app/DrvBuf.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
static const int FrameQueueLimits[CfgDefs::DevNum] = { int(CfgDefs::ThermoScreenFrameNum*0.75), int(CfgDefs::VideoScreenFrameNum*0.75) };
static const int CmdQueueLimits[CfgDefs::DevNum]   = { 8, 8 };

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TLibR* TLibR::mInstance = 0;
TWinCsGuard    TLibR::mInstanceGuard;

//-----------------------------------------------------------------------------
RLibStatus TLibR::init(const TRLibDevParams* params)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(!params) {
        return RLibBadParams;
    }

    mInstance = new TLibR(params);
    return getStatus();
}

//-----------------------------------------------------------------------------
RLibStatus TLibR::cleanUp()
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    delete mInstance;
    mInstance = 0;
    return RLibNotInitialized;
}

//-----------------------------------------------------------------------------
RLibStatus TLibR::getStatus()
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return RLibNotInitialized;
    else
        return getInstance()->getStatusImpl();
}

//-----------------------------------------------------------------------------
void TLibR::setStatus(RLibStatus status)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance()) {
        getInstance()->setStatusImpl(status);
    }
}

//-----------------------------------------------------------------------------
bool TLibR::getFrame(unsigned channelId, TRLibFrame* frame, int timeout)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0) {
        if(frame) {
            frame->status = RLibNotInitialized;
        }
        return false;
    } else {
        return getInstance()->getFrameImpl(channelId,frame,timeout);
    }
}

//-----------------------------------------------------------------------------
bool TLibR::streamCmd(unsigned channelId, TStreamCmd cmd)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return false;
    return getInstance()->streamCmdImpl(channelId,cmd);
}

//-----------------------------------------------------------------------------
void TLibR::setVideoStreamMode(RVideoStreamMode mode)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return;
    getInstance()->setVideoStreamModeImpl(mode);
}

//-----------------------------------------------------------------------------
RVideoStreamMode TLibR::getVideoStreamMode()
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return RLibVideoError;
    return getInstance()->getVideoStreamModeImpl();
}

//-----------------------------------------------------------------------------
bool TLibR::channelSendCmd(unsigned channelId, uint8_t* cmd, unsigned cmdLen, bool directControl)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return false;

	return getInstance()->channelSendCmdImpl(channelId,cmd,cmdLen,0,directControl);
}

//-----------------------------------------------------------------------------
void TLibR::channelGetReplyCallback(unsigned channelId, const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer)
{
	#if defined(LIBR_THREAD_SAFETY)
		TWinCsGuard::TLocker lock(mInstanceGuard);
	#endif
	if(getInstance()) {
		getInstance()->channelGetReplyCallbackImpl(channelId,host,peer);
	}
}


//-----------------------------------------------------------------------------
unsigned TLibR::channelGetReply(unsigned channelId, uint8_t* reply, unsigned maxReplyLen)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return 0;
	return getInstance()->channelGetReplyImpl(channelId,reply,maxReplyLen);
}

//-----------------------------------------------------------------------------
bool TLibR::setLensControlMode(unsigned channelId, const TAfParams* params)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return false;
	return getInstance()->setLensControlModeImpl(channelId,params);
}

//-----------------------------------------------------------------------------
bool TLibR::getLensControlMode(unsigned channelId, TAfParams* params)
{
	#if defined(LIBR_THREAD_SAFETY)
		TWinCsGuard::TLocker lock(mInstanceGuard);
	#endif
	if(getInstance() == 0)
		return false;
	return getInstance()->getLensControlModeImpl(channelId,params);
}

//-----------------------------------------------------------------------------
bool TLibR::platformSendCmd(uint8_t* cmd, unsigned cmdLen)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return false;
    return getInstance()->platformSendCmdImpl(cmd,cmdLen,0);
}

//-----------------------------------------------------------------------------
unsigned TLibR::platformGetReply(uint8_t* reply, unsigned maxReplyLen)
{
    #if defined(LIBR_THREAD_SAFETY)
        TWinCsGuard::TLocker lock(mInstanceGuard);
    #endif
    if(getInstance() == 0)
        return 0;
    return getInstance()->platformGetReplyImpl(reply,maxReplyLen,0);
}

//-----------------------------------------------------------------------------
TLibR::TLibR(const TRLibDevParams* params) : QObject(),
                                                mStatus(RLibNotInitialized),
                                                mFrameFormerLayer(0),
                                                mDrvLayer(0),
                                                mBufPool(),
                                                mParams(*params),
                                                mVideoStreamMode(RLibColorVideo)

{
    //---
    mBufPool.insertPool(new TThermoScreenFrame::TFramePool(CfgDefs::ThermoScreenFrameNum,TThermoScreenFrame::TCreator()));
    mBufPool.insertPool(new TVideoScreenFrame::TFramePool(CfgDefs::VideoScreenFrameNum,TVideoScreenFrame::TCreator()));
    mBufPool.insertPool(new TThermoRawFrame::TFramePool(CfgDefs::ThermoRawFrameNum,TThermoRawFrame::TCreator()));
    mBufPool.insertPool(new TVideoRawFrame::TFramePool(CfgDefs::VideoRawFrameNum,TVideoRawFrame::TCreator()));
	mBufPool.insertPool(new TDrvBufStreamETH::TDrvBufPool(CfgDefs::EthStreamBufNum,TDrvBufStreamETH::TCreator()));
    mBufPool.insertPool(new TDrvBufCmdETH::TDrvBufPool(CfgDefs::EthCmdBufNum,TDrvBufCmdETH::TCreator()));

    #if 1
        qDebug() << "\n*** class Id's (1) ***";
        qDebug() << "TThermoScreenFrame::TFramePool classId:"  << SysUtils::TTypeEnumerator<TThermoScreenFrame::TFramePool>::classId();
        qDebug() << "TVideoScreenFrame::TFramePool classId: "  << SysUtils::TTypeEnumerator<TVideoScreenFrame::TFramePool>::classId();
        qDebug() << "TThermoRawFrame::TFramePool classId:   "  << SysUtils::TTypeEnumerator<TThermoRawFrame::TFramePool>::classId();
        qDebug() << "TVideoRawFrame::TFramePool classId:    "  << SysUtils::TTypeEnumerator<TVideoRawFrame::TFramePool>::classId();
        qDebug() << "TDrvBufStreamETH::TDrvBufPool classId: "  << SysUtils::TTypeEnumerator<TDrvBufStreamETH::TDrvBufPool>::classId();
        qDebug() << "TDrvBufCmdETH::TDrvBufPool classId:    "  << SysUtils::TTypeEnumerator<TDrvBufCmdETH::TDrvBufPool>::classId();

        qDebug() << "\n*** class Id's (2) ***";
        qDebug() << "TThermoScreenFrame classId:" << SysUtils::TTypeEnumerator<TThermoScreenFrame>::classId();
        qDebug() << "TVideoScreenFrame classId: " << SysUtils::TTypeEnumerator<TVideoScreenFrame>::classId();
        qDebug() << "TThermoRawFrame classId:   " << SysUtils::TTypeEnumerator<TThermoRawFrame>::classId();
        qDebug() << "TVideoRawFrame classId:    " << SysUtils::TTypeEnumerator<TVideoRawFrame>::classId();
        qDebug() << "TDrvBufStreamETH classId:  " << SysUtils::TTypeEnumerator<TDrvBufStreamETH>::classId();
        qDebug() << "TDrvBufCmdETH classId:     " << SysUtils::TTypeEnumerator<TDrvBufCmdETH>::classId();

        qDebug() << "\n*** class Id's (3) ***";
        qDebug() << "TRawBuf classId:          " << SysUtils::TTypeEnumerator<TRawBuf>::classId();
        qDebug() << "TBaseFrame classId:       " << SysUtils::TTypeEnumerator<TBaseFrame>::classId();
    #endif

    //---
    qRegisterMetaType<TDrvBufPtr>("TDrvBufPtr");
    qRegisterMetaType<TScreenFramePtr>("TScreenFramePtr");

    //---
	mLensController[CfgDefs::ThermoStreamId] = new TThermoLensController(this);
	mLensController[CfgDefs::VideoStreamId]  = new TVideoLensController(this);

	mFrameFormerLayer = new TFrameFormerLayer(this);
    mDrvLayer         = new TDrvLayer(this);

    //---
    connect(mDrvLayer,&TDrvLayer::sendDrvBufSignal,mFrameFormerLayer,&TFrameFormerLayer::receiveDrvBufSlot,Qt::DirectConnection);
    connect(mFrameFormerLayer,&TFrameFormerLayer::sendFrameSignal,this,&TLibR::receiveFrameSlot,Qt::DirectConnection);
	connect(mFrameFormerLayer,&TFrameFormerLayer::sendFrameToThermoLensControllerSignal,mLensController[CfgDefs::ThermoStreamId],&TBaseLensController::receiveFrameSlot,Qt::DirectConnection);
	connect(mFrameFormerLayer,&TFrameFormerLayer::sendFrameToVideoLensControllerSignal,mLensController[CfgDefs::VideoStreamId],&TBaseLensController::receiveFrameSlot,Qt::DirectConnection);

    //---
	mLensController[CfgDefs::ThermoStreamId]->start(QThread::InheritPriority);
	mLensController[CfgDefs::VideoStreamId]->start(QThread::InheritPriority);
	mFrameFormerLayer->start(QThread::InheritPriority);
    mDrvLayer->activate();

    if(getStatusImpl() == RLibNotInitialized) {
        setStatusImpl(RLibOk);
        qDebug() << "[INFO] [RLib init] Ok";
	} else {
        qDebug() << "[ERROR] [RLib init] status:" << mStatus;
    }
	#if 0
		qDebug() << "sizeof(TRLibDevParams)" << sizeof(TRLibDevParams);
		qDebug() << "sizeof(TRLibFrame)"     << sizeof(TRLibFrame);
		qDebug() << "sizeof(unsigned long)"  << sizeof(unsigned long);
		qDebug() << "sizeof(unsigned)"       << sizeof(unsigned);
		qDebug() << "sizeof(uint8_t*)"       << sizeof(uint8_t*);
	#endif
}

//-----------------------------------------------------------------------------
TLibR::~TLibR()
{
    delete mDrvLayer;
    mDrvLayer = 0;
    delete mFrameFormerLayer;
    mFrameFormerLayer = 0;
	for(unsigned dev = 0; dev < CfgDefs::DevNum; ++dev) {
		delete mLensController[dev];
		mLensController[dev] = 0;
	}
}

//-----------------------------------------------------------------------------
void TLibR::receiveFrameSlot(TScreenFramePtr framePtr)
{
	if(framePtr && (framePtr->netDst() < CfgDefs::TNetAddr(CfgDefs::DevNum))) {
        CfgDefs::NetAddrWordType dst = framePtr->netDst();

        TBaseFrame* frame = checkMsg<TBaseFrame>(framePtr);
        if(!frame) {
            qDebug() << "[ERROR] [TLibR::receiveFrameSlot]";
            return;
        }


        unsigned frameByteSize = frame->byteSize(); // frame->size()*sizeof(TScreenFrame::TPixel);
        {
            volatile TWinCsGuard::TLocker lock(mGuard[dst]); /*???*/
            mFrameQueue[dst].put(framePtr);
            if(mFrameQueue[dst].size() > FrameQueueLimits[dst]) {
                //mSemaphore[dst].acquire();
                mFrameQueue[dst].get(framePtr);
            } else {
                mSemaphore[dst].release();
            }
        }
        if(mParams.frameReadyNotify) {
			(*mParams.frameReadyNotify)(dst,frameByteSize,mFrameQueue[dst].size());
        }
        /*TEST*/ //qDebug() << "slon" << dst << mSemaphore[dst].available();
    }
}

//-----------------------------------------------------------------------------
bool TLibR::sendCmdImpl(unsigned long hostIP, unsigned hostPort, uint8_t* buf, unsigned bufLen, int timeout)
{
    bool funcStatus = false;
    UDP_LIB::Transfer transfer;

    UDP_LIB::TStatus status = UDP_LIB::getTransfer(hostIP,hostPort,UDP_LIB::Transmit,transfer,timeout);
    if(status == UDP_LIB::Ok) {
        if(bufLen <= unsigned(transfer.bufLength)) {
            ::memcpy(transfer.buf,buf,bufLen);
            transfer.length = bufLen;
            funcStatus = true;
        }
        status = UDP_LIB::submitTransfer(hostIP,hostPort,UDP_LIB::Transmit,transfer);
        funcStatus = funcStatus && (status == UDP_LIB::Ok);
    }
    return funcStatus;
}

//-----------------------------------------------------------------------------
unsigned TLibR::getReplyImpl(unsigned long hostIP, unsigned hostPort, uint8_t* reply, unsigned maxReplyLen, int timeout)
{
    UDP_LIB::Transfer transfer;
    unsigned replyLen = 0;

    UDP_LIB::TStatus status = UDP_LIB::getTransfer(hostIP,hostPort,UDP_LIB::Receive,transfer,timeout);
    if(status == UDP_LIB::Ok) {
        if((maxReplyLen >= unsigned(transfer.length) && (transfer.length))) {
            ::memcpy(reply,transfer.buf,transfer.length);
            replyLen = transfer.length;
        }
        status = UDP_LIB::submitTransfer(hostIP,hostPort,UDP_LIB::Receive,transfer);
        replyLen = (replyLen && (status == UDP_LIB::Ok)) ? replyLen : 0;
    }
    return replyLen;
}

//-----------------------------------------------------------------------------
bool TLibR::getFrameImpl(unsigned channelId, TRLibFrame* frame, int timeout)
{
    //---
	if((channelId > (CfgDefs::DevNum-1)) || !frame) {
        frame->status = RLibBadParams;
        return false;
    }

    //---
    if(mSemaphore[channelId].tryAcquire(1,timeout) == false) {
        frame->status = RLibTimeout;
        return false;
    }

    //---
    TScreenFramePtr libFramePtr;
    TBaseFrame* libFrame;

    volatile TWinCsGuard::TLocker lockChannel(mGuard[channelId]); /*???*/
    if(mFrameQueue[channelId].get(libFramePtr) && libFramePtr && (libFramePtr->netDst() == channelId) && (libFrame = checkMsg<TBaseFrame>(libFramePtr))) {
        unsigned libFrameByteSize = libFrame->byteSize(); //sizeof(TScreenFrame::TPixel)*libFrame->size();
        if(1 /*libFrameByteSize <= frame->byteBufSize*/) {
            frame->channelId   = channelId;
            frame->frameNum    = libFramePtr->msgId();
            if(frame->buf) {
                //std::memcpy(frame->buf,libFrame->getPixelBuf<TScreenFrame>(),libFrameByteSize);
                std::memcpy(frame->buf,libFrame->getPixelBuf(),libFrameByteSize);
            }
            frame->byteBufSize = libFrameByteSize;
            frame->pixelSize   = libFrame->pixelSize(); //sizeof(TScreenFrame::TPixel);
            frame->frameWidth  = libFrame->width();
            frame->frameHeight = libFrame->height();
            frame->status      = RLibOk;
        } else {
            frame->status = RLibFrameTooLarge;
        }
    } else {
        frame->status = RLibInternalError;
    }
    return (frame->status == RLibOk);
}

//-----------------------------------------------------------------------------
bool TLibR::streamCmdImpl(unsigned channelId, TStreamCmd cmd)
{
    //---
	if(channelId > (CfgDefs::DevNum-1)) {
        return false;
    }

    TDrvBufPtr drvBufPtr;
    TRawBuf* drvBuf;
    if(getBufPool().getBuf<TDrvBufCmdETH::TDrvBufPool>(drvBufPtr) && drvBufPtr && (drvBuf = checkMsg<TRawBuf>(drvBufPtr))) {
        if(channelId == CfgDefs::ThermoStreamId) {
            drvBufPtr->setNetSrc(T_UDP_LibAdapter::makeNetAddr(mParams.hostIP,mParams.thermoChannelStreamPort));
            drvBufPtr->setNetDst(T_UDP_LibAdapter::makeNetAddr(mParams.thermoChannelIP,mParams.thermoChannelStreamPort));
        }
        if(channelId == CfgDefs::VideoStreamId) {
            drvBufPtr->setNetSrc(T_UDP_LibAdapter::makeNetAddr(mParams.hostIP,mParams.videoChannelStreamPort));
            drvBufPtr->setNetDst(T_UDP_LibAdapter::makeNetAddr(mParams.videoChannelIP,mParams.videoChannelStreamPort));
        }
        TStreamCmd* dataBuf = drvBuf->getDataBuf<TStreamCmd>();
        dataBuf[0] = cmd;
        drvBuf->setDataLen<TStreamCmd>(StreamCmdLen);
        emit mDrvLayer->sendChannelCmdSignal(drvBufPtr);
        return true;
    } else {
        qDebug() << "[ERROR] [TLibR::streamCmdImpl]";
    }
    return false;
}

//-----------------------------------------------------------------------------
bool TLibR::channelSendCmdImpl(unsigned channelId, uint8_t* cmd, unsigned cmdLen, int timeout, bool directControl)
{
	if(channelId > (CfgDefs::DevNum-1) || (cmdLen == 0)) {
		return false;
	}
	if(directControl || mLensController[channelId]->isPacketBypassed(cmd,cmdLen)) {
		unsigned channelPort = (channelId == CfgDefs::ThermoStreamId) ? getParams()->thermoChannelLensPort : getParams()->videoChannelDevGroupPort;
		return sendCmdImpl(getParams()->hostIP,channelPort,cmd,cmdLen,timeout);
	}
	return false;
}

//-----------------------------------------------------------------------------
void TLibR::channelGetReplyCallbackImpl(unsigned channelId, const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer)
{
	const unsigned MaxCmdSize = CfgDefs::EthCmdPacketSize*CfgDefs::EthCmdPacketsInBuf;
	const int UdpTimeout = 0;
	uint8_t srcCmdBuf[MaxCmdSize];

	//	1. read UDP packet from UDP adapter level
	unsigned cmdLen = getReplyImpl(host.ipAddr,host.port,srcCmdBuf,MaxCmdSize,UdpTimeout);
	if(cmdLen == 0) {
		return;
	}

	// 2. UDP packet -> DrvBuf
	TDrvBufPtr drvBufPtr;
	TRawBuf* drvBuf;
	if(getBufPool().getBuf<TDrvBufCmdETH::TDrvBufPool>(drvBufPtr) && drvBufPtr && (drvBuf = checkMsg<TRawBuf>(drvBufPtr))) {
		/*is it need?*/ drvBufPtr->setNetSrc(T_UDP_LibAdapter::makeNetAddr(peer));
		/*is it need?*/ drvBufPtr->setNetDst(T_UDP_LibAdapter::makeNetAddr(host));
		uint8_t* dstCmdBuf = drvBuf->getDataBuf<uint8_t>();
		memcpy(dstCmdBuf,srcCmdBuf,cmdLen);
		drvBuf->setDataLen<uint8_t>(cmdLen);
	} else {
		return;
	}

	// 3. LenSController lock ?

	// 4. check - is this packet have to be bypassed 'up' or to LensController
	if(mLensController[channelId]->isPacketBypassed(srcCmdBuf,cmdLen)) {
		// 5. if(bypassed 'up') { put to 'up' queue(with queue limit contro), 'up' callback }
		if((mCmdReplyQueue[channelId].size() < CmdQueueLimits[channelId]) && (mParams.cmdReplyNotify)) {
			mCmdReplyQueue[channelId].put(drvBufPtr);
			(*mParams.cmdReplyNotify)(peer.ipAddr,peer.port);
		}
	} else {
		// 6. if(to LensController) { put to queue, LensController callback }
		mLensController[channelId]->putCmd(drvBufPtr,CmdQueueLimits[channelId]);
	}
}

//-----------------------------------------------------------------------------
unsigned TLibR::channelGetReplyImpl(unsigned channelId, uint8_t* reply, unsigned maxReplyLen)
{
	TDrvBufPtr drvBufPtr;
	if(!mCmdReplyQueue[channelId].get(drvBufPtr) || !drvBufPtr)
		return 0;
	TRawBuf* drvBuf = checkMsg<TRawBuf>(drvBufPtr);
	if(!drvBuf)
		return 0;
	unsigned replyLen = drvBuf->dataLen<uint8_t>();
	if(replyLen > maxReplyLen)
		return 0;

	uint8_t* srcCmdBuf = drvBuf->getDataBuf<uint8_t>();
	memcpy(reply,srcCmdBuf,replyLen);
	return replyLen;
}

//-----------------------------------------------------------------------------
bool TLibR::setLensControlModeImpl(unsigned channelId, const TAfParams* params)
{
    //---
	if(channelId > (CfgDefs::DevNum-1)) {
        return false;
    }
	return mLensController[channelId]->setLensControlMode(params);
}

//-----------------------------------------------------------------------------
bool TLibR::getLensControlModeImpl(unsigned channelId, TAfParams* params)
{
	//---
	if(channelId > (CfgDefs::DevNum-1)) {
		return false;
	}
	return mLensController[channelId]->getLensControlMode(params);
}

//-----------------------------------------------------------------------------
bool TLibR::platformSendCmdImpl(uint8_t* cmd, unsigned cmdLen, int timeout)
{
    //---
    if(cmdLen == 0) {
        return false;
    }

    //---
    return sendCmdImpl(getParams()->hostIP,getParams()->platformPort,cmd,cmdLen,timeout);
}

//-----------------------------------------------------------------------------
unsigned TLibR::platformGetReplyImpl(uint8_t* reply, unsigned maxReplyLen, int timeout)
{
    return getReplyImpl(getParams()->hostIP,getParams()->platformPort,reply,maxReplyLen,timeout);
}

