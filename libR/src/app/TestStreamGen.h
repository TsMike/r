#if !defined(TEST_STREAM_GEN_H)
#define TEST_STREAM_GEN_H

#include <QtCore/QtMath>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/msg.h"
#include "app/DrvBuf.h"


//#define INSERT_DFM_TEST
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TChannelStreamGen
{
	public:
		typedef quint16 TData;
		typedef TData (TChannelStreamGen::* TGenFunc)() const;

		struct TParams
		{
			const unsigned ChannelNum;
			const unsigned Width;
			const unsigned Height;
			const TGenFunc GenFunc;
		};

		static const TData DFM      = 0x0800;
		static const TData VFM      = 0x0400;
		static const TData SOF      = 1023;
		static const TData SOL      = 1022;
		static const TData CntrMask = 0x03FF;

		TChannelStreamGen(const TParams& params);
		void reset();
		unsigned channelNum() const { return mChannelNum; }
		unsigned width() const { return mWidth; }
		unsigned height() const { return mHeight; }
		TData genDFM() const { return (DFM | mChannelNum);}
		TData genNextData();

		//--- for FFT test
		inline TData TVS_testFFT() const {
			#if 0 // TEST
				unsigned const Size = 128;
				unsigned const Left = CfgDefs::ThermoScreenFrameWidth/2 - Size/2;
				unsigned const Top  = CfgDefs::ThermoScreenFrameHeight/2 - Size/2;
				if((mPixCounter >= Left) && (mPixCounter < Left+Size) && (mLineCounter >= Top) && (mLineCounter < Top+Size)) {
					return 0;
				} else {
					return 300;
				}
				#endif
			const double A0       = 512.0;
			const double A1       = 500.0;
			const double Freq     = 0;
			const unsigned LenFFT = 512;
			return A0 + A1*sin(2*M_PI*(Freq + (mFrameCounter%(LenFFT/2)))*mPixCounter/LenFFT);
		}

		//--- gray X gradient
		inline TData TVS_GrayGradientX() const {
			return mPixCounter;
		}

		//--- moving vertical line
		inline TData TVS_MovingVertLine() const {
			#if 0 // TEST
				unsigned const Size = 256;
				unsigned const Left = CfgDefs::VideoScreenFrameWidth/2 - Size/2;
				unsigned const Top  = CfgDefs::VideoScreenFrameHeight/2 - Size/2;
				if((mPixCounter >= Left) && (mPixCounter < Left+Size) && (mLineCounter >= Top) && (mLineCounter < Top+Size)) {
					return 0;
				} else {
					return 600;
				}
			#endif
			return (mPixCounter != mCounterX) ? (mPixCounter & 0x1F)*20 : 1023;
		}

		//--- const
		inline TData IS_Const() const {
			return 0x00F8;
		}

		//--- pattern 1
		inline TData IS_TestPattern() const;

		//--- zero pattern
		inline TData ZeroPattern() const {
			return 0x0000;
		}

	private:
        enum TState { SOF1, SOF2, SOL1, SOL2, PIX };

		const unsigned mChannelNum;
		const unsigned mWidth;
		const unsigned mHeight;

		TGenFunc mGenFunc;
		unsigned mCounterX;
		unsigned mCounterY;
		unsigned mFrameCounter;
		unsigned mLineCounter;
		unsigned mPixCounter;
		TState   mState;
        #if defined(INSERT_DFM_TEST)
            int mTestDFMState;
        #endif
};

//-----------------------------------------------------------------------------
inline TChannelStreamGen::TData TChannelStreamGen::IS_TestPattern() const {
	const unsigned CounterY = mCounterY & 0x3FF;
	const unsigned Color    = (mCounterY & 0x3C0) >> 6;
	const unsigned X2Y2     = (mLineCounter - CounterY)*(mLineCounter - CounterY) + (mPixCounter - CounterY)*(mPixCounter - CounterY);

	if(X2Y2 < (2500 + (Color*Color*100))) {
		return 0x00F0 | Color; // 0x00F9
	}

	const unsigned Mask = 0x7F;
	if(((mLineCounter & Mask) == (mCounterY & Mask)) || ((mPixCounter & Mask) == (mCounterY & Mask))) {
		return 0x00F5; // 0x00F7
	}

	if((mLineCounter < 120) && (mPixCounter > 400)) {
		return 0x00F3;
	}

	if(mLineCounter < 20) {
		return 0x007C;
	}

	return 0x0000;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TDevStreamGen
{
	public:
		typedef TChannelStreamGen::TData TData;

		TDevStreamGen(const TChannelStreamGen::TParams* channelParams, const unsigned* channelWorkTime, unsigned channelNum = 3);
		~TDevStreamGen();
		void reset();
		bool fillBuf(TData* buf, unsigned bufLen, unsigned chunkLen = 8192);

	private:
		unsigned currChannel();
		void fillChunk(TData* chunk, unsigned chunkLen);
		const unsigned      ChannelNum;
		TChannelStreamGen** mChannelStreamGen;
		unsigned*           mChannelCumSumWorkTime;
		unsigned            mChunkCounter;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TDevBundleStreamGen
{
	public:
		typedef TDevStreamGen::TData TData;

		TDevBundleStreamGen() : mCurrDev(0), mCurrWorkSlot(0) {}
		~TDevBundleStreamGen();
		void insertDevGen(
						  TRoutingPolicy::TNetAddr srcDev,
						  TRoutingPolicy::TNetAddr dstDev,
						  TMsgWrapperPoolQueue* bufPool,
						  unsigned devWorkTime,
						  const TChannelStreamGen::TParams* channelParams,
						  const unsigned* channelWorkTime,
						  unsigned channelNum,
						  unsigned chunkLen = 8192
						 );
		int bundleSize() const { return mDevList.size(); }
		void fillDrvBuf(TDrvBufPtr& drvBufPtr);
		TDrvBufPtr genDrvBuf();

	private:
		struct TDevParams
		{
			TRoutingPolicy::TNetAddr     mSrcDev;
			TRoutingPolicy::TNetAddr     mDstDev;
			TMsgWrapperPoolQueue*        mBufPool;
			unsigned                     mDevWorkTime;
			TDevStreamGen*               mDevGen;
			unsigned                     mChunkLen;
		};

		typedef QList<TDevParams> TDevGenList;

		TDevGenList mDevList;
		int         mCurrDev;
		unsigned    mCurrWorkSlot;
};



#endif // TEST_STREAM_GEN_H


