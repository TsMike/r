//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/LensMotionController.h"
#include "app/LensController.h"
#include "app/libRImpl.h"
#include <numeric>

#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/highgui/highgui.hpp>

#define VIDEO_CHANNEL_CHECK_SUM

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void TBaseAfInitializer::reset()
{
    mParent->mState = TBaseLensController::InitAf;
    mState = txPhase;
	mErrRetry = ErrRetryNum;
}

//-----------------------------------------------------------------------------
bool TBaseAfInitializer::init(TAfStatus& afStatus, cv::Mat& roiMat)
{
	unsigned replyLen;

	switch (mState) {
		case txPhase:
			#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
				mState = rxPhase;
				return false;
			#endif

			setLensStatusRequest();
			mState = rxPhase;
			mTimer = TimeOut;
			return false;

		case rxPhase:
			#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
				mState = resOk;
				return true;
			#endif

			replyLen = mParent->getReply(mReplyBuf,ReplyBufLen);
			if(replyLen) {
				if(checkReply(mReplyBuf,replyLen)) {
					mState = resOk;
				} else {
					if(--mErrRetry) {
						mState = txPhase;
					} else {
						qDebug() << "[ERROR] [AfInitializer::init] [ErrRetry]" << mParent->channelId();
						mState = resError;
					}
				}
				return true;
			} else {
				if(--mTimer) {
				} else {
					qDebug() << "[ERROR] [AfInitializer::init] [Timeout]"  << mParent->channelId();
					mState = resError;
					return true;
				}
			}
			return false;

		case resError:
			afStatus.afState = RLibAfErrorState;
			roiMat = mParent->mPreconditioner->compute(roiMat);
			afStatus.focusFuncValue = mParent->mFuncFocusAlg->compute(roiMat);
			mParent->afCallback(&afStatus);
			mParent->mParams.mode = RLibLensDirectMode;
			qDebug() << "[ERROR] [AfInitializer::init]"  << mParent->channelId();
			return false;

		case resOk:
			mParent->mState = TBaseLensController::StartAf;

			#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
				return true;
			#endif

			mParent->setLensInfo(mReplyBuf);
			#if defined(AF_TEST_INFO)
				mParent->debugPrintLensInfo("[INFO] [AfInitializer::init] ok");
			#endif
			return true;

		default:
			break;
	}
	return false;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TThermoAfInitializer::setLensStatusRequest()
{
	const unsigned CmdLen    = 12;
	uint8_t CmdBuf[CmdLen] = { 0xE6, 0xE6, 0x0C, 0, 0xE0, 0, 0, 0, 0, 0, focusSpeed(), 0 };

	CmdBuf[CmdLen-1] = TThermoLensController::checkSum(CmdBuf,CmdLen);
	if(TLibR::channelSendCmd(CfgDefs::ThermoStreamId,CmdBuf,CmdLen,true)) {
		return true;
	} else {
		return false;
	}
}

//-----------------------------------------------------------------------------
bool TThermoAfInitializer::checkReply(const uint8_t* buf,unsigned bufLen)
{
	const unsigned CmdReplyLen = 14;
	if(bufLen != CmdReplyLen)
		return false;
	if(TThermoLensController::checkSum(buf,CmdReplyLen) != buf[CmdReplyLen-1])
		return false;

	if((buf[0] != 0xE5) && (buf[1] != 0xE5))
		return false;

	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TVideoAfInitializer::setLensStatusRequest()
{

	#if defined(VIDEO_CHANNEL_CHECK_SUM)
		const unsigned CmdLen    = 6;
		uint8_t CmdBuf[CmdLen] = { CmdLen, 0x00, 0x80, 0x0A, 0x00, 0x00 };
		CmdBuf[CmdLen-2] = TVideoLensController::checkSum(CmdBuf,CmdLen);
	#else
		const unsigned CmdLen    = 4;
		uint8_t CmdBuf[CmdLen] = { CmdLen, 0x00, 0x80, 0x0A };
	#endif

	if(TLibR::channelSendCmd(CfgDefs::VideoStreamId,CmdBuf,CmdLen,true)) {
		return true;
	} else {
		return false;
	}
}

//-----------------------------------------------------------------------------
bool TVideoAfInitializer::checkReply(const uint8_t* buf,unsigned bufLen)
{
	#if defined(VIDEO_CHANNEL_CHECK_SUM)
		const unsigned CmdReplyLen = 16;
		if((TVideoLensController::checkSum(buf,CmdReplyLen) != buf[CmdReplyLen-2]) || (buf[CmdReplyLen-1] != 0))
			return false;
	#else
		const unsigned CmdReplyLen = 14;
	#endif
	if(bufLen != CmdReplyLen)
		return false;
	if((buf[0] != CmdReplyLen) || (buf[1] != 0) || (buf[2] != 0x80) || (buf[3] != 10) || (buf[13] != 0))
		return false;
	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void TBaseMotionController::moveTo(int32_t focus, TMotionSpeed speed)
{
	mCmdError = false;
	mCmdFocus = focus;

	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		return;
	#endif

	mTotalTime = 0;
	mTimeOut   = 0;
	mTimer     = 0;

	mFocus = mParent->focus();
	mPrevFocus = mFocus;

	mErrRetry  = ErrRetryNum;
	mIter      = IterNum;
	mCmdError  = moveToImpl(focus,speed);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TThermoLensMotionController::moveToImpl(int32_t focus, TMotionSpeed speed)
{
	uint8_t lensFocusSpeed;

	switch(speed)
	{
		case DefaultSpeed:
			lensFocusSpeed = 0;
			break;
		case InitSpeed:
			lensFocusSpeed = 0;
			break;
		case CoarseSpeed:
			lensFocusSpeed = 0;
			break;
		case FineSpeed:
			lensFocusSpeed = 0;
			break;
		case FinalSpeed:
			lensFocusSpeed = 8;
			break;
		default:
			lensFocusSpeed = 0;
			break;
	}
    const unsigned CmdLen    = 12;
	uint8_t CmdBuf[CmdLen] = { 0xE6, 0xE6, 0x0C, 0, 0xE0, 0, 0, 0, 0, 0, lensFocusSpeed, 0 };
    CmdBuf[5] = static_cast<uint8_t>(focus & 0xFF);
    CmdBuf[6] = static_cast<uint8_t>((focus >> 8) & 0xFF);

	mTotalTime += (mTimeOut - mTimer);
	mTimeOut = TThermoLensMotionController::TimeOut;
	mTimer = mTimeOut;
	//qDebug() << "[thermo] moveToImpl:" << focus << speed;
	CmdBuf[CmdLen-1] = TThermoLensController::checkSum(CmdBuf,CmdLen);
    return !TLibR::channelSendCmd(CfgDefs::ThermoStreamId,CmdBuf,CmdLen,true);
}

//-----------------------------------------------------------------------------
bool TThermoLensMotionController::motionComplete()
{
	return abs(mFocus-mCmdFocus) < 1;
}

//-----------------------------------------------------------------------------
bool TThermoLensMotionController::keepMotion(TAfStatus& afStatus, cv::Mat& roiMat)
{
    #if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
        mFocus = mCmdFocus;
        mParent->mState = TBaseLensController::ComputeNextStep;
        return true;
    #endif

    //---
    bool bError = mCmdError;
    if(!bError) {
        unsigned replyLen = mParent->getReply(mReplyBuf,ReplyBufLen);
        if(replyLen) {
			if(checkReply(mReplyBuf,replyLen)) {
				mParent->setLensInfo(mReplyBuf);
				mFocus = mParent->focus();
                if(motionComplete()) {
                    mParent->mState = TBaseLensController::ComputeNextStep;
					mTotalTime += (mTimeOut - mTimer);
					printf("[INFO] [thermo] move to: %6d :%6d, from: %6d, dF: %6d, time: %4d\n",mCmdFocus,mFocus,mPrevFocus,(mFocus-mPrevFocus),mTotalTime);
					return true;
                } else {
                    if(--mIter) {
						//qDebug() << "[WARN] [TThermoLensMotionController::keepMotion] [moveTo]" << mCmdFocus << mFocus << (IterNum - mIter);
                        mErrRetry = ErrRetryNum;
                        bError = moveToImpl(mCmdFocus);
                    } else {
						qDebug() << "[ERROR] [TThermoLensMotionController::keepMotion] [moveTo]" << mCmdFocus << mFocus;
                        bError = true;
                    }
                }
            } else {
                if(--mErrRetry) {
					//qDebug() << "[WARN] [TThermoLensMotionController::keepMotion] retry(bad reply):" << mErrRetry;
                    bError = moveToImpl(mCmdFocus);
                } else {
					qDebug() << "[ERROR] [TThermoLensMotionController::keepMotion] [ErrRetry]";
                    bError = true;
                }
            }
        } else {
            if(--mTimer) {
				//qDebug() << "[INFO] [TThermoLensMotionController::keepMotion] timer:" << (mTimeOut - mTimer);
            } else {
				if(--mErrRetry) {
					qDebug() << "[WARN] [TThermoLensMotionController::keepMotion] retry(timeout):" << mErrRetry;
					bError = moveToImpl(mCmdFocus);
				} else {
					qDebug() << "[ERROR] [TThermoLensMotionController::keepMotion] [ErrRetry]";
					bError = true;
				}
            }
        }
    }

    //---
    if(bError) {
        afStatus.afState = RLibAfErrorState;
        roiMat = mParent->mPreconditioner->compute(roiMat);
        afStatus.focusFuncValue = mParent->mFuncFocusAlg->compute(roiMat);
        mParent->afCallback(&afStatus);
        mParent->mParams.mode = RLibLensDirectMode;
        qDebug() << "[ERROR] [TThermoLensMotionController::keepMotion]";
        return false;
    }
    return false;
}

//-----------------------------------------------------------------------------
bool TThermoLensMotionController::checkReply(const uint8_t* buf,unsigned bufLen, int /*replyNum*/)
{
	const unsigned CmdReplyLen = 14;
	if(bufLen != CmdReplyLen)
		return false;
	if(TThermoLensController::checkSum(buf,CmdReplyLen) != buf[CmdReplyLen-1])
		return false;

	if((buf[0] != 0xE5) && (buf[1] != 0xE5))
		return false;

	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
bool TVideoLensMotionController::moveToImpl(int32_t focus, TMotionSpeed /*speed*/)
{
	#if defined(VIDEO_CHANNEL_CHECK_SUM)
		const unsigned CmdLen = 8;
		uint8_t CmdBuf[CmdLen] = { CmdLen, 0x00, 0x80, 0x84, 0x00, 0x00, 0x00, 0x00 };
		CmdBuf[4] = static_cast<uint8_t>(focus & 0xFF);
		CmdBuf[5] = static_cast<uint8_t>((focus >> 8) & 0xFF);
		CmdBuf[CmdLen-2] = TVideoLensController::checkSum(CmdBuf,CmdLen);
	#else
		const unsigned CmdLen = 6;
		uint8_t CmdBuf[CmdLen] = { CmdLen, 0x00, 0x80, 0x84, 0x00, 0x00 };
		CmdBuf[4] = static_cast<uint8_t>(focus & 0xFF);
		CmdBuf[5] = static_cast<uint8_t>((focus >> 8) & 0xFF);
	#endif

	mTotalTime += (mTimeOut - mTimer);
	mTimeOut = TVideoLensMotionController::AckTimeOut;
	mTimer = mTimeOut;

	mReplyNum = 1;
	//qDebug() << "[video] moveToImpl:" << focus;
	return !TLibR::channelSendCmd(CfgDefs::VideoStreamId,CmdBuf,CmdLen,true);
}

//-----------------------------------------------------------------------------
bool TVideoLensMotionController::keepMotion(TAfStatus& afStatus, cv::Mat& roiMat)
{
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		mFocus = mCmdFocus;
		mParent->mState = TBaseLensController::ComputeNextStep;
		return true;
	#endif

	#if 1
	//---
	bool bError = mCmdError;
	if(!bError) {
		unsigned replyLen = mParent->getReply(mReplyBuf,ReplyBufLen);
		if(replyLen) {
			if(checkReply(mReplyBuf,replyLen,mReplyNum)) {
				if(mReplyNum == 1) {
					mTotalTime += (mTimeOut - mTimer);
					int32_t dFocus = qAbs(mCmdFocus-mPrevFocus);
					mTimeOut = dFocus/20 + 5; // TVideoLensMotionController::CmdTimeOut;
					mTimer = mTimeOut;
					++mReplyNum;
				} else {
					mParent->setLensInfo(mReplyBuf,mReplyNum);
					mFocus = mParent->focus();
					mParent->mState = TBaseLensController::ComputeNextStep;
					mTotalTime += (mTimeOut - mTimer);
					printf("[INFO] [video] move to: %6d :%6d, from: %6d, dF: %6d, time: %4d\n",mCmdFocus,mFocus,mPrevFocus,(mFocus-mPrevFocus),mTotalTime);
					return true;
				}
			} else {
				if(--mErrRetry) {
					qDebug() << "[WARN] [TVideoLensMotionController::keepMotion] retry(bad reply):" << mErrRetry;
					bError = moveToImpl(mCmdFocus);
				} else {
					qDebug() << "[ERROR] [TVideoLensMotionController::keepMotion] [ErrRetry]";
					bError = true;
				}
			}
		} else {
			if(--mTimer) {
				//qDebug() << "[INFO] [TVideoLensMotionController::keepMotion] timer:" << (mTimeOut - mTimer);
			} else {
				if(--mErrRetry) {
					qDebug() << "[WARN] [TVideoLensMotionController::keepMotion] retry(timeout):" << mErrRetry;
					bError = moveToImpl(mCmdFocus);
				} else {
					qDebug() << "[ERROR] [TVideoLensMotionController::keepMotion] [ErrRetry]";
					bError = true;
				}
			}
		}
	}
	//---
	if(bError) {
		afStatus.afState = RLibAfErrorState;
		roiMat = mParent->mPreconditioner->compute(roiMat);
		afStatus.focusFuncValue = mParent->mFuncFocusAlg->compute(roiMat);
		mParent->afCallback(&afStatus);
		mParent->mParams.mode = RLibLensDirectMode;
		qDebug() << "[ERROR] [TVideoLensMotionController::keepMotion]";
		return false;
	}
	return false;
	#else
		mFocus = mCmdFocus;
		mParent->mState = TBaseLensController::ComputeNextStep;
		return true;
	#endif
}

//-----------------------------------------------------------------------------
bool TVideoLensMotionController::checkReply(const uint8_t* buf,unsigned bufLen, int replyNum)
{
	#if 0
		qDebug() << "replyNum" << replyNum << bufLen;
		qDebug() << "buf[0:5]:"  << hex << buf[0] << buf[1] << buf[2] << buf[3] << buf[4] << buf[5];
		qDebug() << "buf[6:11]:" << hex << buf[6] << buf[7] << buf[8] << buf[9] << buf[10] << buf[11];
	#endif

	//qDebug() << "slon1";

	if(replyNum == 1) {
		#if defined(VIDEO_CHANNEL_CHECK_SUM)
			const unsigned CmdReplyLen1 = 8;
			const unsigned CmdReplyLen2 = 16;
		#else
			const unsigned CmdReplyLen1 = 6;
			const unsigned CmdReplyLen2 = 12;
		#endif

		if(bufLen == CmdReplyLen1) {
			#if defined(VIDEO_CHANNEL_CHECK_SUM)
				if(((TVideoLensController::checkSum(buf,CmdReplyLen1) != buf[CmdReplyLen1-2]) || (buf[CmdReplyLen1-1] != 0)))
					return false;
			#endif
			if((buf[0] != CmdReplyLen1) || (buf[1] != 0x00) || (buf[2] != 0x80) || (buf[3] != 0x04))
				return false;
			uint16_t cmdFocus = (static_cast<uint16_t>(buf[5]) << 8) + buf[4];
			if(cmdFocus != mCmdFocus)
				return false;
			return true;
		}
		if(bufLen == CmdReplyLen2) {
			#if defined(VIDEO_CHANNEL_CHECK_SUM)
				if(((TVideoLensController::checkSum(buf,CmdReplyLen1) != buf[CmdReplyLen1-2]) || (buf[CmdReplyLen1-1] != 0)))
					return false;
				if(((TVideoLensController::checkSum(buf+CmdReplyLen1,CmdReplyLen1) != buf[CmdReplyLen2-2]) || (buf[CmdReplyLen2-1] != 0)))
					return false;
			#endif
			if((buf[0] != CmdReplyLen1) || (buf[1] != 0x00) || (buf[2] != 0x80) || (buf[3] != 0x04))
				return false;
			uint16_t cmdFocus = (static_cast<uint16_t>(buf[5]) << 8) + buf[4];
			if(cmdFocus != mCmdFocus)
				return false;
			if((buf[8] != CmdReplyLen1) || (buf[9] != 0x00) || (buf[10] != 0x80) || (buf[11] != 0x05))
				return false;
			mReplyNum = 3;
			return true;
		}
		return false;
	} else {
		#if defined(VIDEO_CHANNEL_CHECK_SUM)
			const unsigned CmdReplyLen = 8;
		#else
			const unsigned CmdReplyLen = 6;
		#endif
		if(bufLen != CmdReplyLen)
			return false;
		#if defined(VIDEO_CHANNEL_CHECK_SUM)
			if(((TVideoLensController::checkSum(buf,CmdReplyLen) != buf[CmdReplyLen-2]) || (buf[CmdReplyLen-1] != 0)))
				return false;
		#endif
		if((buf[0] != CmdReplyLen) || (buf[1] != 0x00) || (buf[2] != 0x80) || (buf[3] != 0x05))
			return false;
		return true;
	}
}
