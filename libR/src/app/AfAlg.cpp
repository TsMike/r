//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/AfAlg.h"
#include "app/libRImpl.h"
#include <numeric>

#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/highgui/highgui.hpp>


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TVideoPreconditioner::TVideoPreconditioner(unsigned maxSize) :
                            TBasePreconditioner(),
                            mOutMat(maxSize,maxSize,CV_16UC1)
{
    //cv::namedWindow("slon",cv::WINDOW_AUTOSIZE);
}

//-----------------------------------------------------------------------------
cv::Mat TVideoPreconditioner::compute(const cv::Mat& inMat)
{
	cv::Rect inRect(0,0,inMat.cols,inMat.rows);
    cv::Mat outMat = mOutMat(inRect);
    #if defined(USE_TEST_DEMOSAIC_IMG)
        cv::cvtColor(inMat,outMat,CV_BayerRG2GRAY);
    #else
        cv::cvtColor(inMat,outMat,CV_BayerGR2GRAY);
    #endif
    /*TEST*/  // outMat = inMat;
    //cv::imshow("slon",outMat*64);
    return outMat;
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TTengAlg::TTengAlg(unsigned roiSizeNum, const uint16_t* roiSize) : TBaseFuncFocusAlg(),
													mRoiSizeNum(roiSizeNum),
													mMaxRoiSize(roiSize[roiSizeNum-1]),
													mGx(mMaxRoiSize,mMaxRoiSize,CV_64FC1),
													mGy(mMaxRoiSize,mMaxRoiSize,CV_64FC1),
													mFm(mMaxRoiSize,mMaxRoiSize,CV_64FC1)
{
	mRoiFrame = new cv::Mat [roiSizeNum];
	for(unsigned n = 0; n < roiSizeNum; ++n) {
		mRoiFrame[n].create(roiSize[n],roiSize[n],CV_16UC1);
	}
}

//-----------------------------------------------------------------------------
TTengAlg::~TTengAlg()
{
	delete [] mRoiFrame;
}

//-----------------------------------------------------------------------------
cv::Mat TTengAlg::getRoiFrame(unsigned roiSize, int& idx)
{
	idx = -1;
	for(unsigned n = 0; n < mRoiSizeNum; ++n) {
		if(mRoiFrame[n].rows == roiSize) {
			idx = n;
			return mRoiFrame[n];
		}
	}
	return cv::Mat();
}

//#define ALG_TIME_MEASURE
//-----------------------------------------------------------------------------
double TTengAlg::compute(const cv::Mat& inMat)
{
	//---
	const double Scale = 1.0;
	const double Delta = 0.0;

	//*****
	#if defined(AF_TEST_INFO) & defined(ALG_TIME_MEASURE)
		double tStart = (double)cv::getTickCount();
	#endif

	//---
	const unsigned RoiSize = inMat.rows;
	cv::Rect roi(0,0,RoiSize,RoiSize);

	//---
	int idx;
	cv::Mat roiFrame = getRoiFrame(RoiSize,idx);
	if(roiFrame.empty())
		return -13; // magic number :)
	inMat.copyTo(roiFrame);

	//qDebug() << "[SLON]" << roiFrame.isContinuous() << roiFrame.rows << idx;

	//---
	cv::Mat gx = mGx(roi);
	cv::Mat gy = mGy(roi);
	cv::Mat fm = mFm(roi);

	//---
	cv::Sobel(roiFrame,gx,CV_64FC1,1,0,KernelSize,Scale,Delta,cv::BORDER_REPLICATE);
	cv::Sobel(roiFrame,gy,CV_64FC1,0,1,KernelSize,Scale,Delta,cv::BORDER_REPLICATE);
	fm = gx.mul(gx) + gy.mul(gy);
	double focusFuncVal = cv::mean(fm).val[0];

	#if defined(AF_TEST_INFO) & defined(ALG_TIME_MEASURE)
		double tStop = (double)cv::getTickCount();
		double tAlg = (tStop - tStart)/cv::getTickFrequency();
		qDebug() << "time:" << tAlg*1000.0;
	#endif

	return focusFuncVal;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
double TGlvaAlg::compute(const cv::Mat& inMat)
{
	cv::Scalar mean;
	cv::Scalar std;

	cv::meanStdDev(inMat,mean,std);
	return std.val[0];
}
