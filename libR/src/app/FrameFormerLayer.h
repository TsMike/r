#if !defined(FRAME_FORMER_LAYER_H)
#define FRAME_FORMER_LAYER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/tthread.h"
#include "app/Frame.h"
#include "app/DrvBuf.h"
#include "app/StreamParser.h"
#include "app/StreamSwitch.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseFrameFormer;
class TLibR;

class TFrameFormerLayer : public TThread
{
 Q_OBJECT

 public:

 signals:
    void sendFrameSignal(TScreenFramePtr);
	void sendFrameToThermoLensControllerSignal(TBaseMsgWrapperPtr);
	void sendFrameToVideoLensControllerSignal(TBaseMsgWrapperPtr);

 public slots:
    void receiveDrvBufSlot(TDrvBufPtr);

 protected:
    virtual bool onExec();
    void threadFinish() { setThreadExit(); mSemaphore.release();  TThread::threadFinish(); }

 public:
    explicit TFrameFormerLayer(TLibR* parent);
    ~TFrameFormerLayer();

 private:
    TLibR*                mParent;
    bool getFrame(TScreenFramePtr& testFrame, unsigned devId);
	void sendFrameToAf(unsigned channel, TBaseMsgWrapperPtr& framePtr);
	#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
        bool genTestFrame(TScreenFramePtr& testFrame, unsigned devId, unsigned testPattern, bool frameResize = false);
    #endif
    QSemaphore            mSemaphore;
    TMsgWrapperPoolQueue  mDrvBufQueue;
    TDevBundleParser      mDevBundleParser;
    TStreamSwitch         mStreamSwitch;
    TBaseFrameFormer*     mThermoFrameFormer;
    TBaseFrameFormer*     mVideoFrameFormer;
	TMsgWrapperPoolQueue  mScreenFrameQueue;
	TMsgWrapperPoolQueue* mAfFrameQueue[CfgDefs::DevNum];

	#if (WORK_MODE != TESTGEN_AT_FRAME_FORMER_LAYER)
		void frameFilter(TScreenFramePtr& framePtr);
	#endif
};

#endif // FRAME_FORMER_LAYER_H


