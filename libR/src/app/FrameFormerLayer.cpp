//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/FrameFormer.h"
#include "app/FrameFormerLayer.h"
#include "app/libRImpl.h"
#include "app/udp_lib_adapter.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
	static uint32_t  mFrameCounter[CfgDefs::DevNum] = { 0, 0 };
    static const unsigned MovingBands = 0;
    static const unsigned MovingCross = 1;
	static const unsigned PatternId[CfgDefs::DevNum] = { MovingBands, MovingCross };
    //#define FRAME_RESIZE_TEST			// CONFIG - work only when (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TFrameFormerLayer::TFrameFormerLayer(TLibR* parent) :
									 TThread(L"FrameFormerThread"),
                                     mParent(parent),
                                     mSemaphore(),
                                     mDrvBufQueue(),
                                     mDevBundleParser(),
									 mStreamSwitch(),
                                     mThermoFrameFormer(0),
                                     mVideoFrameFormer(0),
                                     mScreenFrameQueue()
{

    mThermoFrameFormer = new TThermoFrameFormer(mParent->getBufPool().getPool<TThermoScreenFrame::TFramePool>());
    mVideoFrameFormer  = new TVideoFrameFormer(mParent->getBufPool().getPool<TVideoScreenFrame::TFramePool>());

    #if ((WORK_MODE == TESTGEN_AT_DRV_LAYER) || (WORK_MODE == WORK_APPLICATION))
        const TRLibDevParams* devParams = parent->getParams();
        //---
		mStreamSwitch.addChannelBranch(CfgDefs::ThermoStreamId,2);
		mStreamSwitch.addChannelBranch(CfgDefs::VideoStreamId,2);
        mThermoFrameFormer->setInQueue(mStreamSwitch.connectOutflowQueue(CfgDefs::ThermoStreamId, 0));
        mThermoFrameFormer->setOutQueue(&mScreenFrameQueue);
        mVideoFrameFormer->setInQueue(mStreamSwitch.connectOutflowQueue(CfgDefs::VideoStreamId, 0));
        mVideoFrameFormer->setOutQueue(&mScreenFrameQueue);

		for(unsigned dev = 0; dev < CfgDefs::DevNum; ++dev) {
			mAfFrameQueue[dev] = mStreamSwitch.connectOutflowQueue(dev,1);
		}

        //---
		TDevParser* devParser[CfgDefs::DevNum] = { new TDevParser, new TDevParser };
        devParser[0]->attachChannelParser(CfgDefs::ThermoChannelId/*CfgDefs::ThermoStreamId*/, new TFrameParser(CfgDefs::ThermoScreenFrameWidth,CfgDefs::ThermoScreenFrameHeight,mParent->getBufPool().getPool<TThermoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::ThermoStreamId)));
        devParser[1]->attachChannelParser(CfgDefs::VideoChannelId/*CfgDefs::VideoStreamId*/, new TFrameParser(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mParent->getBufPool().getPool<TVideoRawFrame::TFramePool>(),mStreamSwitch.connectInflowQueue(CfgDefs::VideoStreamId)));
        mDevBundleParser.attachDevParser(T_UDP_LibAdapter::makeNetAddr(devParams->thermoChannelIP,devParams->thermoChannelStreamPort),devParser[0]);
        mDevBundleParser.attachDevParser(T_UDP_LibAdapter::makeNetAddr(devParams->videoChannelIP,devParams->videoChannelStreamPort),devParser[1]);
    #endif

    #if 1
        qDebug() << qPrintable(QString("[FrameFormerLayer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
TFrameFormerLayer::~TFrameFormerLayer()
{
    threadFinish();
    delete mThermoFrameFormer;
    delete mVideoFrameFormer;

    #if 1
        qDebug() << qPrintable(QString("[FrameFormerLayer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
void TFrameFormerLayer::sendFrameToAf(unsigned channel, TBaseMsgWrapperPtr& framePtr)
{
	if(channel == CfgDefs::ThermoStreamId) {
		emit sendFrameToThermoLensControllerSignal(framePtr);
	}
	if(channel == CfgDefs::VideoStreamId) {
		emit sendFrameToVideoLensControllerSignal(framePtr);
	}
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::onExec()
{
    #if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
        ::Sleep(CfgDefs::TestFramePeriod);
        mSemaphore.release();
    #endif
    mSemaphore.acquire();
    if(threadExit())
        return threadExit();

    #if 0
        qDebug() << qPrintable(QString("[FrameFormerLayer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("onExec()").leftJustified(SysUtils::JobNameJustify));
    #endif

	TScreenFramePtr framePtr;

	#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
		for(unsigned devId = 0; devId < CfgDefs::DevNum; ++devId) {
            if(genTestFrame(framePtr,devId,PatternId[devId],false)) {
                framePtr->setNetDst(devId);
                framePtr->setMsgId(mFrameCounter[devId]++);
                emit sendFrameSignal(framePtr);
            } else {
                qDebug() << "[ERROR] no free frame for dev" << devId;
            }
        }
    #endif

	#if ((WORK_MODE == TESTGEN_AT_DRV_LAYER) || (WORK_MODE == WORK_APPLICATION))
        TDrvBufPtr drvBufPtr;
		if(mDrvBufQueue.get(drvBufPtr) && drvBufPtr) {
			//qDebug() << "[drvBuf received] srcAddr:" << qPrintable(QString("[drvBuf received] src: %1").arg(drvBufPtr->netSrc(),8,16,QLatin1Char('0'))) << "msgClassId:" << drvBufPtr->msgClassId();
			mDevBundleParser.parseDrvBuf(drvBufPtr);
            /* int nDispatchedMsgs = */ mStreamSwitch.dispatchMessages();
            /* int nThermoFrames   = */ mThermoFrameFormer->formFrames();
            /* int nVideoFrames    = */ mVideoFrameFormer->formFrames();
            #if 0 /*TEST*/
                   if(nThermoFrames || nVideoFrames)
                       qDebug() << "thermo frames formed:" << nThermoFrames << "video frames formed:" << nVideoFrames;
            #endif

            if(mScreenFrameQueue.get(framePtr)) {
                frameFilter(framePtr);
            }
			#if 1
			TBaseMsgWrapperPtr frameAfPtr;
			for(unsigned dev = 0; dev < CfgDefs::DevNum; ++dev) {
				if(mAfFrameQueue[dev] && mAfFrameQueue[dev]->get(frameAfPtr)) {
					sendFrameToAf(dev,frameAfPtr);
				}
			}
			#endif

            #if 0 /*TEST*/
                if(nDispatchedMsgs) {
                    mSystemAf->getBufPool().bufPoolInfo();
                }
            #endif
        }
    #endif
    return threadExit();
}

//-----------------------------------------------------------------------------
void TFrameFormerLayer::receiveDrvBufSlot(TDrvBufPtr drvBufPtr)
{
	//qDebug() << "[TFrameFormer::receiveDrvBufSlot]" << SysUtils::getThreadId();
    mDrvBufQueue.put(drvBufPtr);
    mSemaphore.release();
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::getFrame(TScreenFramePtr& testFrame, unsigned devId)
{
    if(devId == CfgDefs::ThermoStreamId) {
        return mParent->getBufPool().getBuf<TThermoScreenFrame::TFramePool>(testFrame);
    }
    if(devId == CfgDefs::VideoStreamId) {
        return mParent->getBufPool().getBuf<TVideoScreenFrame::TFramePool>(testFrame);
    }
    return false;
}

//-----------------------------------------------------------------------------
#if (WORK_MODE != TESTGEN_AT_FRAME_FORMER_LAYER)
void TFrameFormerLayer::frameFilter(TScreenFramePtr& framePtr)
{
    const TRLibDevParams* devParams = mParent->getParams();
    CfgDefs::TNetAddr validSrc0 = T_UDP_LibAdapter::makeNetAddr(devParams->thermoChannelIP,devParams->thermoChannelStreamPort);
    CfgDefs::TNetAddr validSrc1 = T_UDP_LibAdapter::makeNetAddr(devParams->videoChannelIP,devParams->videoChannelStreamPort);

    if((framePtr->netSrc() == validSrc0) || (framePtr->netSrc() == validSrc1)) {
		emit sendFrameSignal(framePtr);
		//qDebug() << "frame transmitted to UI Layer" << framePtr->netDst();
	} else {
		qDebug() << "frame rejected" << framePtr->netSrc() << framePtr->netDst();
		#if !defined(MSG_SELF_RELEASE)
			releaseMsg(framePtr);
		#endif
	}
}
#endif

#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)

//-----------------------------------------------------------------------------
template<typename T,int FNum> static inline typename T::TPixel testPixGen(int, int, int);

//-----------------------------------------------------------------------------
typedef TScreenFrame     TScreenFrameProxy;
typedef TScreenFrameGray TScreenFrameGrayProxy;

//-----------------------------------------------------------------------------
template<> static inline typename TScreenFrameProxy::TPixel testPixGen<TScreenFrameProxy,MovingBands>(int,int col, int bufNum)
{
	quint8 pixVal = (col + bufNum) & 0xFF;
    return qRgba(0,pixVal,pixVal,200);
}

//-----------------------------------------------------------------------------
template<> static inline typename TScreenFrameProxy::TPixel testPixGen<TScreenFrameProxy,MovingCross>(int cond,int brightness, int)
{
	return (cond ? qRgba(200,0,0,255) : qRgba(brightness,0,brightness,255));
}

//-----------------------------------------------------------------------------
template<> static inline typename TScreenFrameGrayProxy::TPixel testPixGen<TScreenFrameGrayProxy,MovingBands>(int,int col, int bufNum)
{
    quint8 pixVal = (col + bufNum) & 0xFF;
    return pixVal;
}

//-----------------------------------------------------------------------------
template<> static inline typename TScreenFrameGrayProxy::TPixel testPixGen<TScreenFrameGrayProxy,MovingCross>(int cond,int brightness, int)
{
    return cond ? 200 : brightness;
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::genTestFrame(TScreenFramePtr& testFrame, unsigned devId, unsigned testPattern, bool frameResize)
{
    if(getFrame(testFrame,devId)) {
        TBaseFrame* testFrameProxy = checkMsg<TBaseFrame>(testFrame);
        /*TEST*/ //TBaseFrame* testFrameProxy = checkMsg<TVideoScreenFrame>(testFrame);
        if(!testFrameProxy) {
            #if !defined(MSG_SELF_RELEASE)
                releaseMsg(testFrame);
            #endif
            qDebug() << "[ERROR] TFrameFormer::genTestFrame";
            return false;
        }

        if(frameResize) {
            static int width = testFrameProxy->width();
            static int height = testFrameProxy->height();
            if(width < CfgDefs::VideoScreenFrameWidth/8) {
                width = CfgDefs::VideoScreenFrameWidth;
            } else {
                width -= 4;
            }
            if(height < CfgDefs::VideoScreenFrameHeight/8) {
                height = CfgDefs::VideoScreenFrameHeight;
            } else {
                height -= 4;
            }
            testFrameProxy->resizeImg(width,height);
        }

        static quint8 bufNum = 0;
        TScreenFrame::TPixel*         frameBuf = testFrameProxy->getPixelBuf<TScreenFrame>();
        TScreenFrameGray::TPixel* frameBufGray = testFrameProxy->getPixelBuf<TScreenFrameGray>();

        //---
        if(testPattern == MovingBands) {
            for(int row = 0; row < testFrameProxy->height(); ++row) {
                for(int col = 0; col < testFrameProxy->width(); ++col) {
                    if(devId == CfgDefs::ThermoStreamId) {
                        *frameBufGray++ = testPixGen<TScreenFrameGrayProxy,MovingBands>(row,col,bufNum);
                    }
                    else {
                        *frameBuf++ = testPixGen<TScreenFrameProxy,MovingBands>(row,col,bufNum);
                    }
                }
            }
        }

        //---
        if(testPattern == MovingCross) {
            const int crossWidthX = 10;
            const int crossWidthY = 10;
            static int xCross     = 0;
            static int yCross     = 0;

            for(int row = 0; row < testFrameProxy->height(); ++row) {
                for(int col = 0; col < testFrameProxy->width(); ++col) {
                    int isCross = ((row >= yCross) && (row <= yCross + crossWidthY)) || ((col >= xCross) && (col <= xCross + crossWidthX)) ? 1 : 0;
                    if(devId == CfgDefs::ThermoStreamId) {
                        *frameBufGray++ = testPixGen<TScreenFrameGrayProxy,MovingCross>(isCross,bufNum,0);
                    }
                    else {
                        *frameBuf++ = testPixGen<TScreenFrameProxy,MovingCross>(isCross,bufNum,0);
                    }
                }
            }
            if(++yCross >= testFrameProxy->height())
                yCross = 0;
            if(++xCross >= testFrameProxy->width())
                xCross = 0;
        }
        ++bufNum;
        return true;
    } else {
        return false;
    }
}
#endif

