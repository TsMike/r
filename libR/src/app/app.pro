#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets 

TEMPLATE    = lib
TARGET      = libR
//CONFIG     += console
DEPENDPATH += .
TOPDIR      = ../..

include($$TOPDIR/common.pri)
include($$TOPDIR/build.pri)

HEADERS    += \
               libRDefs.h                \
               libR.h                    \
               libRImpl.h                \
               BufPool.h                 \
               tqueue.h                  \
               tthread.h                 \
               msg.h                     \
               RawBuf.h                  \
               DrvBuf.h                  \
               SysUtils.h                \
               Frame.h                   \
               StreamParser.h            \
               StreamSwitch.h            \
               FrameFormer.h             \
               FrameFormerLayer.h        \
               AfAlg.h                   \
               LensMotionController.h    \
               LensController.h          \
               DrvLayer.h                \
               TestStreamGen.h           \
               udp_lib_adapter.h         \
               CfgDefs.h
SOURCES    += \
               libR.cpp                  \
               libRImpl.cpp              \
               StreamParser.cpp          \
               StreamSwitch.cpp          \
               FrameFormer.cpp           \
               FrameFormerLayer.cpp      \
               AfAlg.cpp                 \
               LensMotionController.cpp  \
               LensController.cpp        \
               BufPool.cpp               \
               DrvLayer.cpp              \
               TestStreamGen.cpp         \
               udp_lib_adapter.cpp
QT         += testlib

win32 {
    LIBS += -lsetupapi -luuid -ladvapi32 -lws2_32 -ludp_lib -lopencv_world320
}

#LIBS += -lapr -llibapr -laprapp -llibaprapp -lusbadapter -lufo

#------------------------------------------------------------------------------------------------
# notes:
#
# QT += core gui - not need because core and gui are included by default


# FORMS += \
