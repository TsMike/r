#if !defined(LENS_MOTION_CONTROLLER_H)
#define LENS_MOTION_CONTROLLER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/tthread.h"
#include "app/Frame.h"
#include "app/libR.h"
#include "app/DrvBuf.h"
#include "app/AfAlg.h"
#include <opencv2/core/core.hpp>


//-----------------------------------------------------------------------------
class TBaseLensController;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseAfInitializer
{
    public:
        TBaseAfInitializer(TBaseLensController* parent) : mParent(parent) {}
        virtual ~TBaseAfInitializer() {}
		virtual uint16_t focusSpeed() { return 0; }
		void reset();
		bool init(TAfStatus&, cv::Mat&);

    protected:
        typedef enum
        {
            txPhase,
            rxPhase,
            resError,
            resOk
        } TState;

		static const unsigned TimeOut       = 10;
		static const unsigned ErrRetryNum   = 5;
		static const unsigned ReplyBufLen   = CfgDefs::EthCmdPacketSize;

		virtual bool setLensStatusRequest() = 0;
		virtual bool checkReply(const uint8_t* buf,unsigned bufLen) = 0;

		TBaseLensController* mParent;
        TState               mState;
		unsigned			 mTimer;
		unsigned			 mErrRetry;
		uint8_t				 mReplyBuf[ReplyBufLen];
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TThermoAfInitializer : public TBaseAfInitializer
{
    public:
		TThermoAfInitializer(TBaseLensController* parent) : TBaseAfInitializer(parent) {}
        virtual ~TThermoAfInitializer() {}

	private:
		virtual bool setLensStatusRequest();
		virtual bool checkReply(const uint8_t* buf,unsigned bufLen);
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TVideoAfInitializer : public TBaseAfInitializer
{
	public:
		TVideoAfInitializer(TBaseLensController* parent) : TBaseAfInitializer(parent) {}
		virtual ~TVideoAfInitializer() {}

	private:
		virtual bool setLensStatusRequest();
		virtual bool checkReply(const uint8_t* buf,unsigned bufLen);
};


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseMotionController
{
    public:
		typedef enum
		{
			DefaultSpeed,
			InitSpeed,
			CoarseSpeed,
			FineSpeed,
			FinalSpeed
		} TMotionSpeed;

		TBaseMotionController(TBaseLensController* parent) : mParent(parent), mCmdError(false)  {}
        virtual ~TBaseMotionController() {}
        void reset() { mCmdError = false; }
        int32_t getFocus() { return mFocus; }
		void moveTo(int32_t focus, TMotionSpeed speed = DefaultSpeed);
		virtual bool moveToImpl(int32_t focus, TMotionSpeed speed = DefaultSpeed) = 0;
		virtual bool keepMotion(TAfStatus& afStatus, cv::Mat& roiMat) = 0;

    protected:
		static const unsigned ErrRetryNum = 5;
        static const unsigned IterNum     = 50;
        static const unsigned ReplyBufLen = CfgDefs::EthCmdPacketSize;

		virtual bool checkReply(const uint8_t* buf,unsigned bufLen, int replyNum = 1) = 0;

		unsigned             mTimeOut;
		unsigned             mTotalTime;
		unsigned             mTimer;
        unsigned             mErrRetry;
        unsigned             mIter;
        TBaseLensController* mParent;
        int32_t              mCmdFocus;
        int32_t              mFocus;
		int32_t              mPrevFocus;
		bool                 mCmdError;
        uint8_t              mReplyBuf[ReplyBufLen];
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TThermoLensMotionController : public TBaseMotionController
{
    public:
        TThermoLensMotionController(TBaseLensController* parent) : TBaseMotionController(parent) {}
        ~TThermoLensMotionController() {}
		virtual bool moveToImpl(int32_t focus, TMotionSpeed speed = DefaultSpeed);
		virtual bool keepMotion(TAfStatus& afStatus, cv::Mat& roiMat);

    private:
		static const unsigned TimeOut = 3;

		virtual bool checkReply(const uint8_t* buf,unsigned bufLen, int replyNum = 1);
		bool motionComplete();
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TVideoLensMotionController : public TBaseMotionController
{
    public:
        TVideoLensMotionController(TBaseLensController* parent) : TBaseMotionController(parent) {}
        ~TVideoLensMotionController() {}
		virtual bool moveToImpl(int32_t focus, TMotionSpeed speed = DefaultSpeed);
        virtual bool keepMotion(TAfStatus& afStatus, cv::Mat& roiMat);

	private:
		static const unsigned AckTimeOut = 3;
		static const unsigned CmdTimeOut = 200;

		virtual bool checkReply(const uint8_t* buf,unsigned bufLen, int replyNum = 1);

		int mReplyNum;
};

#endif // LENS_MOTION_CONTROLLER_H


