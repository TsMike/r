//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#if !defined(LIB_R_DEFS_H)
#define LIB_R_DEFS_H

#ifdef __cplusplus
    #include <cstdint>
#else
    #include <stdint.h>
#endif

//-----------------------------------------------------------------------------
typedef enum
{
    RLibOk                     =  0,
    RLibNotInitialized         = -1,
    RLibBadParams              = -2,
    RLibInitError              = -3,
    RLibTimeout                = -4,
    RLibInternalError          = -5,
    RLibFrameTooLarge          = -6,
    RLibThermoStreamInitError  = -7,
    RLibVideoStreamInitError   = -8,
    RLibThermoLensInitError    = -9,
    RLibVideoLensInitError     = -10,
    RLibPlatformInitError      = -11,
    RLibInfoInitError          = -12
} RLibStatus;

//-----------------------------------------------------------------------------
typedef enum
{
    RLibVideoError             = -1,
    RLibRawVideo               =  0,
    RLibGrayScaleVideo         =  1,
    RLibColorVideo             =  2
} RVideoStreamMode;

//-----------------------------------------------------------------------------
typedef enum
{
    RLibLensModeError          = -1,
    RLibLensDirectMode         =  0,
    RLibLensAfMode             =  1
} RLensMode;

//-----------------------------------------------------------------------------
typedef enum
{
	RLibAfCallbackDisabled     = 0, // not used
	RLibAfCallbackNormal       = 1,
	RLibAfCallbackDebug        = 2, // not used
	RLibAfCallbackAlways       = 3
} RAfCallbackMode;

//-----------------------------------------------------------------------------
typedef enum
{
	RLibAfErrorState          = -1,
	RLibAfOffState            =  0,
	RLibAfStartPointState     =  1,
	RLibAfReperPointState     =  2,
	RLibAfEndPointState       =  3,
	RLibAfBestFocusPointState =  4
} RAfState;


//-----------------------------------------------------------------------------
typedef void (*FrameReadyNotify)(unsigned channelId, unsigned byteFrameSize, unsigned framesInQueue);
typedef void (*CmdReplyNotify)(unsigned long peerIP, unsigned peerPort);

//-----------------------------------------------------------------------------
struct TRLibFrame
{
    unsigned   channelId;
    uint32_t   frameNum;
    uint8_t*   buf;
    unsigned   byteBufSize;
    unsigned   pixelSize;
    unsigned   frameWidth;
    unsigned   frameHeight;
    RLibStatus status;
};

//-----------------------------------------------------------------------------
#pragma pack(push, 8)
struct TAfParams
{
	int8_t		mode;
	uint8_t		afCallbackMode;
	uint16_t	roiX0;
	uint16_t	roiY0;
	uint16_t	roiSize;
	uint8_t		afAlgType;
	uint8_t		afAvgNum;
	uint16_t	afParam1;
	uint16_t	afParam2;
};
#pragma pack(pop)

//-----------------------------------------------------------------------------
#pragma pack(push, 8)
struct TAfStatus
{
	TAfParams	params;
	uint8_t	    afState;
	uint32_t    frameNum;
	int32_t     focusValue;
	double      focusFuncValue;
};
#pragma pack(pop)

//-----------------------------------------------------------------------------
typedef void (*AfReplyNotify)(unsigned channelId, TAfStatus* status);

//-----------------------------------------------------------------------------
struct TRLibDevParams
{
	//---
	unsigned long    hostIP;
	FrameReadyNotify frameReadyNotify;
	CmdReplyNotify   cmdReplyNotify;
	AfReplyNotify    afReplyNotify;

	//---
	unsigned long    thermoChannelIP;
	unsigned         thermoChannelTestPort;
	unsigned         thermoChannelLensPort;
	unsigned         thermoChannelStreamPort;

	//---
	unsigned long    videoChannelIP;
	unsigned         videoChannelTestPort;
	unsigned         videoChannelDevGroupPort;
	unsigned         videoChannelStreamPort;

	//---
	unsigned long    platformIP;
	unsigned         platformPort;
};


#endif // LIB_R_DEFS_H

