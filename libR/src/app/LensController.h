#if !defined(LENS_CONTROLLER_H)
#define LENS_CONTROLLER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/tthread.h"
#include "app/Frame.h"
#include "app/libR.h"
#include "app/DrvBuf.h"
#include "app/AfAlg.h"
#include "app/LensMotionController.h"
#include <opencv2/core/core.hpp>


//-----------------------------------------------------------------------------
class TLibR;
class TBaseLensController;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseLensController : public TThread
{
    Q_OBJECT

    friend class TBaseAfInitializer;
    friend class TThermoAfInitializer;
    friend class TThermoLensMotionController;
    friend class TVideoLensMotionController;

    public:
		typedef enum
		{
			TENG    = 0,
			GLVA    = 1
		} TFuncFocusAlg;


		typedef enum
        {
            DirectControl,
            InitAf,
            StartAf,
            FocusPositioning,
            ComputeNextStep
        } TState;

    public slots:
        void receiveFrameSlot(TBaseMsgWrapperPtr);

    public:
        static const uint16_t MaxRoiSize = 256;

		virtual ~TBaseLensController();
		virtual bool isLensControlCmd(const uint8_t* cmd, unsigned cmdLen) const = 0;
		virtual unsigned channelId() const = 0;
		virtual void setLensInfo(const uint8_t* buf, int cmdNum = 1) = 0;
		virtual uint16_t focus() const = 0;
		virtual uint16_t fov() const = 0;

		bool setLensControlMode(const TAfParams* params);
        bool getLensControlMode(TAfParams* params);
        bool isDirectLensControlMode() const { return (mParams.mode == RLibLensDirectMode); }
        bool isPacketBypassed(const uint8_t* cmd, unsigned cmdLen) const;
        bool putCmd(TDrvBufPtr cmd, int cmdQueueLimit);
        unsigned getReply(uint8_t* reply, unsigned maxReplyLen);

    protected:
        static const unsigned RoiSizeNum = 4;
        static const uint16_t RoiSize[RoiSizeNum];
        static const unsigned MaxFocusPointsNum = 201;

        TBaseLensController(TLibR* parent, const std::wstring& threadName);
		bool onExec();
		virtual void getFrameSize(uint16_t& xSize, uint16_t& ySize) = 0;
		virtual void afCallback(TAfStatus* afStatus) = 0;
		virtual void computeFocusRange() = 0;
		virtual int32_t computeBestFocus(unsigned& bestFocusIdx);
		virtual unsigned focusSteadyDelay() const = 0;
		virtual void debugPrintLensInfo(QString header) = 0;

		void threadFinish() { setThreadExit(); mSemaphore.release();  TThread::threadFinish(); }
        #if defined(FRAME_IP_PIPE)
            int sendSubFrameToPipe(uint32_t channelId, cv::Mat& subFrame, TAfStatus& status);
            bool serializeSubFrame(uint32_t channelId, cv::Mat& subFrame, TAfStatus& status, uint8_t* dst, uint32_t* streamLen);
        #endif
        bool checkRoi(const TAfParams* params);
        bool checkParams(const TAfParams* params);
        bool getRoiMat(TBaseMsgWrapperPtr framePtr, cv::Mat& roiMat);
        AfReplyNotify getAfCallback();
        bool afStartAf();
		bool afComputeNextStep(TAfStatus& afStatus, cv::Mat& roiMat);
		bool globalSearchAlg(TAfStatus& afStatus, int32_t focus, double funcFocus);
		bool afFsm(TAfStatus& afStatus, cv::Mat& roiMat);

        static const int8_t     ModeNum         = 2;
        static const uint8_t    CallbackModeNum = 4;
		static const unsigned   FuncFocusAlgNum = 2;

        TAfParams               mParams;
        TBasePreconditioner*    mPreconditioner;
        TBaseFuncFocusAlg*      mFuncFocusAlg;
		TBaseFuncFocusAlg*      mFuncFocusAlgSet[FuncFocusAlgNum];
		TBaseAfInitializer*     mAfInitializer;
        TBaseMotionController*  mMotionController;
        TWinCsGuard             mGuard;
        TMsgWrapperPoolQueue    mCmdReplyQueue;
        TState                  mState;
        unsigned                mFocusPoint;
        unsigned                mFocusPointNum;
        unsigned                mFocusSteadyDelay;
        unsigned                mFocusFuncAvgNum;
        int32_t                 mDesiredFocusArray[MaxFocusPointsNum];
        int32_t                 mFocusArray[MaxFocusPointsNum];
		double                  mFocusFuncArray[MaxFocusPointsNum];
        cv::Mat                 mAvgMat;
        cv::Mat                 mAvgRoiMat;

		#if defined(AF_TEST_INFO)
			bool				mAfTrigger;
			uint32_t			mStartAfFrame;
		#endif

    private:
        TLibR*                  mParent;
        QSemaphore              mSemaphore;
        TMsgWrapperPoolQueue    mFrameQueue;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TThermoLensController : public TBaseLensController
{
    public:
        struct TLensInfo
        {
            uint8_t  status;
            uint16_t fov;
            uint16_t focus;
            uint8_t  state;
            uint8_t  temperature;
            uint8_t  error;
            uint16_t opTime;
            uint8_t  subRev;
        };

        static uint8_t checkSum(const uint8_t* buf, unsigned bufLen);

        TThermoLensController(TLibR* parent);
        ~TThermoLensController();
		virtual unsigned channelId() const { return CfgDefs::ThermoStreamId; }
		virtual void getFrameSize(uint16_t& xSize, uint16_t& ySize) {
            xSize = CfgDefs::ThermoScreenFrameWidth;
            ySize = CfgDefs::ThermoScreenFrameHeight;
        }

	protected:
		static const unsigned FocusSteadyDelay = 0;
		virtual unsigned focusSteadyDelay() const { return FocusSteadyDelay; }

		virtual bool isLensControlCmd(const uint8_t*, unsigned) const { return true; }
        virtual void afCallback(TAfStatus* afStatus);
        virtual void computeFocusRange();
		virtual void setLensInfo(const uint8_t* buf, int cmdNum = 1);
		virtual uint16_t focus() const { return lensInfo().focus; }
		virtual uint16_t fov() const { return lensInfo().fov; }
		virtual void debugPrintLensInfo(QString header);

		TLensInfo lensInfo() const { return mLensInfo; }

    private:
        TLensInfo mLensInfo;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TVideoLensController : public TBaseLensController
{
    public:
		struct TLensInfo
		{
			uint16_t fovRef;
			uint16_t fov;
			uint16_t focusRef;
			uint16_t focus;
			uint8_t  status;
		};

		static const uint16_t MinFov   =  265;
		static const uint16_t MaxFov   = 3600;
		static const uint16_t MinFocus =  170; // {1} = 140,  {2} = 160, {3} = 170
		static const uint16_t MaxFocus = 3919; // {1} = 3940, {2} = 3919, {3} = 3928

		static const unsigned DeviceFieldPos = 2;
        static const uint8_t  LensId         = 0x80;

		static uint8_t checkSum(const uint8_t* buf, unsigned bufLen);

		TVideoLensController(TLibR* parent);
        ~TVideoLensController();
		virtual unsigned channelId() const { return CfgDefs::VideoStreamId; }

	protected:
		static const unsigned FocusSteadyDelay = 0;
		virtual unsigned focusSteadyDelay() const { return FocusSteadyDelay; }

		virtual void getFrameSize(uint16_t& xSize, uint16_t& ySize) {
            xSize = CfgDefs::VideoScreenFrameWidth;
            ySize = CfgDefs::VideoScreenFrameHeight;
        }
        virtual bool isLensControlCmd(const uint8_t* cmd, unsigned cmdLen) const;
        virtual void afCallback(TAfStatus* afStatus);
        virtual void computeFocusRange();
		virtual void setLensInfo(const uint8_t* buf, int cmdNum = 1);
		virtual uint16_t focus() const { return lensInfo().focus; }
		virtual uint16_t fov() const { return lensInfo().fov; }
		virtual void debugPrintLensInfo(QString header);

		TLensInfo lensInfo() const { return mLensInfo; }

	private:
		uint16_t focusInfinityStart() const;

		TLensInfo mLensInfo;
};

#endif // LENS_CONTROLLER_H


