#if !defined(LIB_R_IMPL_H)
#define LIB_R_IMPL_H

#include <QtCore/QObject>
#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/BufPool.h"
#include "app/udp_lib_adapter.h"

#include "libRDefs.h"

#define LIBR_THREAD_SAFETY

//-----------------------------------------------------------------------------
class TFrameFormerLayer;
class TDrvLayer;
class TBaseLensController;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TLibR : public QObject
{
    Q_OBJECT

    public:
        typedef uint16_t TStreamCmd;
        static const unsigned StreamCmdLen = 1;

        static RLibStatus init(const TRLibDevParams* params);
        static RLibStatus cleanUp();
        static RLibStatus getStatus();
        static void setStatus(RLibStatus status);
        static bool getFrame(unsigned channelId, TRLibFrame* frame, int timeout = -1);
        static bool streamCmd(unsigned channelId, TStreamCmd cmd);
        static void setVideoStreamMode(RVideoStreamMode mode);
        static RVideoStreamMode getVideoStreamMode();
		static bool channelSendCmd(unsigned channelId, uint8_t* cmd, unsigned cmdLen, bool directControl = false);
		static unsigned channelGetReply(unsigned channelId, uint8_t* reply, unsigned maxReplyLen);
		static void channelGetReplyCallback(unsigned channelId, const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer);
		static bool setLensControlMode(unsigned channelId, const TAfParams* params);
		static bool getLensControlMode(unsigned channelId, TAfParams* params);
		static bool platformSendCmd(uint8_t* cmd, unsigned cmdLen);
        static unsigned platformGetReply(uint8_t* reply, unsigned maxReplyLen);

        const TRLibDevParams* getParams() const { return &mParams; }
        TBufPool& getBufPool() { return mBufPool; }
        RLibStatus getStatusImpl() const { return mStatus; }
        void setStatusImpl(RLibStatus status) { mStatus = status; }
        static TLibR* getInstance() { return mInstance; }

    protected slots:
        void receiveFrameSlot(TScreenFramePtr);

    private:
        static TLibR*         mInstance;
        static TWinCsGuard    mInstanceGuard;

        TLibR(const TRLibDevParams* params);
        ~TLibR();
        bool sendCmdImpl(unsigned long hostIP, unsigned hostPort, uint8_t* buf, unsigned bufLen, int timeout);
		unsigned getReplyImpl(unsigned long hostIP, unsigned hostPort, uint8_t* reply, unsigned maxReplyLen, int timeout);
        bool getFrameImpl(unsigned channelId, TRLibFrame* frame, int timeout = -1);
        bool streamCmdImpl(unsigned channelId, TStreamCmd cmd);
        void setVideoStreamModeImpl(RVideoStreamMode mode) { mVideoStreamMode = mode; }
        RVideoStreamMode getVideoStreamModeImpl() const { return mVideoStreamMode; }
		//---
		bool channelSendCmdImpl(unsigned channelId, uint8_t* cmd, unsigned cmdLen, int timeout, bool directControl);
		unsigned channelGetReplyImpl(unsigned channelId, uint8_t* reply, unsigned maxReplyLen);
		void channelGetReplyCallbackImpl(unsigned channelId, const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer);
		//---
		bool setLensControlModeImpl(unsigned channelId, const TAfParams* params);
		bool getLensControlModeImpl(unsigned channelId, TAfParams* params);
		bool platformSendCmdImpl(uint8_t* cmd, unsigned cmdLen, int timeout);
        unsigned platformGetReplyImpl(uint8_t* reply, unsigned maxReplyLen, int timeout);

        RLibStatus            mStatus;
        TFrameFormerLayer*    mFrameFormerLayer;
		TBaseLensController*  mLensController[CfgDefs::DevNum];
        TDrvLayer*            mDrvLayer;
        TBufPool              mBufPool;

		TMsgWrapperPoolQueue  mFrameQueue[CfgDefs::DevNum];
		TMsgWrapperPoolQueue  mCmdReplyQueue[CfgDefs::DevNum];
		TWinCsGuard		      mGuard[CfgDefs::DevNum];
		QSemaphore            mSemaphore[CfgDefs::DevNum];
        TRLibDevParams        mParams;
        RVideoStreamMode      mVideoStreamMode;
};

#endif // LIB_R_IMPL_H


