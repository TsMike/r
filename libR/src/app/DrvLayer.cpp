//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/DrvLayer.h"
#include "app/libRImpl.h"
#include "app/udp_lib_adapter.h"


#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
struct TDevParams
{
    static const unsigned* getChannelWorkTime() { static const unsigned ChannelWorkTime = 1; return &ChannelWorkTime; }
    static unsigned  getChannelNum() { return 1; }

    CfgDefs::TNetAddr          srcDev;
    CfgDefs::TNetAddr          dstDev;
    TMsgWrapperPoolQueue*      bufPool;
    unsigned                   devWorkTime;
    TChannelStreamGen::TParams streamParams;
    unsigned                   chunkLen;
};
#endif

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TDrvLayer::TDrvLayer(TLibR* parent) :
							   TThread(L"DrvLayerThread"),
                               mParent(parent),
							   #if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
								mDevBundleStreamGen(),
							   #endif
							   mSemaphore()
{
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
        configThermoTestGen();
        configVideoTestGen();

		connect(this,&TDrvLayer::sendDrvBufAuxSignal,this,&TDrvLayer::sendDrvBufSignal,Qt::DirectConnection);
	#endif

    #if (WORK_MODE == WORK_APPLICATION)
        T_UDP_LibAdapter::init();

        //---
		connect(T_UDP_LibAdapter::getInstance(),&T_UDP_LibAdapter::sendMsgSignal,this,&TDrvLayer::sendDrvBufSignal,Qt::DirectConnection);
		connect(this,&TDrvLayer::sendChannelCmdSignal,T_UDP_LibAdapter::getInstance(),&T_UDP_LibAdapter::receiveMsgSlot,Qt::DirectConnection);
		configThermoStream();
        configVideoStream();

        //---
        configThermoLens();
		configVideoDevGroup();

        //---
        configPlatform();
    #endif

    #if 1
		qDebug() << qPrintable(QString("[DrvLayer]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
	#endif
}

//-----------------------------------------------------------------------------
TDrvLayer::~TDrvLayer()
{
	threadFinish();
    #if (WORK_MODE == WORK_APPLICATION)
		T_UDP_LibAdapter::cleanUp();
	#endif

	#if 1
		qDebug() << qPrintable(QString("[DrvLayer]").leftJustified(SysUtils::ClassNameJustify))
				 << SysUtils::getThreadId()
				 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
	#endif
}

//-----------------------------------------------------------------------------
void TDrvLayer::activate()
{
    #if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
        start(QThread::InheritPriority);
	#endif
}

//-----------------------------------------------------------------------------
bool TDrvLayer::onExec()
{
	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		::Sleep(CfgDefs::TestDrvBufPeriod);
		mSemaphore.release();
	#endif

	mSemaphore.acquire();
	if(threadExit())
		return threadExit();

	#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
		TDrvBufPtr drvBufPtr = mDevBundleStreamGen.genDrvBuf();
		sendDrvBufAuxSignal(drvBufPtr);
	#endif

	return threadExit();
}

//-----------------------------------------------------------------------------
CmdReplyNotify TDrvLayer::getCmdReplyNotify()
{
    return TLibR::getInstance()->getParams()->cmdReplyNotify;
}

//-----------------------------------------------------------------------------
void TDrvLayer::thermoCmdReplyCallback(const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer, UDP_LIB::TDirection /*dir*/)
{
	TLibR::channelGetReplyCallback(CfgDefs::ThermoStreamId,host,peer);
}

//-----------------------------------------------------------------------------
void TDrvLayer::videoDevGroupCmdReplyCallback(const UDP_LIB::TNetAddr& host, const  UDP_LIB::TNetAddr& peer, UDP_LIB::TDirection /*dir*/)
{
	TLibR::channelGetReplyCallback(CfgDefs::VideoStreamId,host,peer);
}

//-----------------------------------------------------------------------------
void TDrvLayer::platformCmdReplyCallback(const UDP_LIB::TNetAddr& /*host*/, const  UDP_LIB::TNetAddr& peer, UDP_LIB::TDirection /*dir*/)
{
	CmdReplyNotify cmdReplyCallback;
	if(cmdReplyCallback = getCmdReplyNotify()) {
		(*cmdReplyCallback)(peer.ipAddr,peer.port);
	}
}



#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)

//TVS_testFFT
//TVS_GrayGradientX
//TVS_MovingVertLine
//IS_Const
//IS_TestPattern
//ZeroPattern

//-----------------------------------------------------------------------------
void TDrvLayer::configThermoTestGen()
{
    const TRLibDevParams* devParams = mParent->getParams();
    TDevParams thermoDevParams =
    {
        T_UDP_LibAdapter::makeNetAddr(devParams->thermoChannelIP,devParams->thermoChannelStreamPort),
        0, //T_UDP_LibAdapter::makeNetAddr(devParams->hostIP,devParams->thermoChannelStreamPort),
        mParent->getBufPool().getPool<TDrvBufStreamETH::TDrvBufPool>(),
        1,
        {
            CfgDefs::ThermoChannelId /*CfgDefs::ThermoStreamId*/,
            CfgDefs::ThermoScreenFrameWidth,
            CfgDefs::ThermoScreenFrameHeight,
            &TChannelStreamGen::TVS_testFFT
        },
        16*CfgDefs::EthStreamPacketSize
    };
    mDevBundleStreamGen.insertDevGen(
                                      thermoDevParams.srcDev,            // srcDev
                                      thermoDevParams.dstDev,            // dstDev
                                      thermoDevParams.bufPool,           // pool
                                      thermoDevParams.devWorkTime,       // devWorkTime
                                      &thermoDevParams.streamParams,     // channelParams
                                      TDevParams::getChannelWorkTime(),  // channelWorkTime
                                      TDevParams::getChannelNum(),       // channelNum
                                      thermoDevParams.chunkLen           // mChunkLen (default: 8192)
                                    );
}

//-----------------------------------------------------------------------------
void TDrvLayer::configVideoTestGen()
{
    const TRLibDevParams* devParams = mParent->getParams();
    TDevParams videoDevParams =
    {
        T_UDP_LibAdapter::makeNetAddr(devParams->videoChannelIP,devParams->videoChannelStreamPort),
        0, //T_UDP_LibAdapter::makeNetAddr(devParams->hostIP,devParams->videoChannelStreamPort),
        mParent->getBufPool().getPool<TDrvBufStreamETH::TDrvBufPool>(),
        4,
        {
            CfgDefs::VideoChannelId/*CfgDefs::VideoStreamId*/,
            CfgDefs::VideoScreenFrameWidth,
            CfgDefs::VideoScreenFrameHeight,
        /*&TChannelStreamGen::TVS_testFFT*/ &TChannelStreamGen::TVS_MovingVertLine
        },
        16*CfgDefs::EthStreamPacketSize
    };
    mDevBundleStreamGen.insertDevGen(
                                      videoDevParams.srcDev,             // srcDev
                                      videoDevParams.dstDev,             // dstDev
                                      videoDevParams.bufPool,            // pool
                                      videoDevParams.devWorkTime,        // devWorkTime
                                      &videoDevParams.streamParams,      // channelParams
                                      TDevParams::getChannelWorkTime(),  // channelWorkTime
                                      TDevParams::getChannelNum(),       // channelNum
                                      videoDevParams.chunkLen            // mChunkLen (default: 8192)
                                    );
}
#endif

#if (WORK_MODE == WORK_APPLICATION)
//-----------------------------------------------------------------------------
bool TDrvLayer::configThermoStream()
{
    const TRLibDevParams* devParams = mParent->getParams();
    T_UDP_RxChannel::TParams rxParams = {
                                           {
                                               CfgDefs::EthStreamPacketSize,          // netPacketSize
                                               CfgDefs::EthStreamPacketsInBuf,        // numPacketsInBundle
                                               128,                                   // numBundles
                                               THREAD_PRIORITY_HIGHEST,               // threadPriority
											   UdpLibTimeout,                         // timeout
                                               8*1024*1024,                           // socketBufSize
                                               devParams->thermoChannelIP,            // peerAddr
                                               devParams->thermoChannelStreamPort,    // peerPort
                                               T_UDP_LibAdapter::rxNotify             // onTransferReady
                                           },
                                           mParent->getBufPool().getPool<TDrvBufStreamETH::TDrvBufPool>() // msgBufPool
                                         };
    T_UDP_TxChannel::TParams txParams =  {
                                           {
                                               CfgDefs::EthCmdPacketSize,             // netPacketSize
                                               CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                               16,                                    // numBundles
                                               THREAD_PRIORITY_NORMAL,                // threadPriority
											   UdpLibTimeout,                         // timeout
                                               64*1024,                               // socketBufSize
                                               devParams->thermoChannelIP,            // peerAddr
                                               devParams->thermoChannelStreamPort,    // peerPort
                                               T_UDP_LibAdapter::txNotify             // onTransferReady
                                           },
                                           16                                         // txQueueLimit
                                         };

    if(T_UDP_LibAdapter::createSocket(devParams->hostIP,devParams->thermoChannelStreamPort,&rxParams,&txParams) == UDP_LIB::Ok) {
        qDebug() << "[INFO] socket {thermo-stream} created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->thermoChannelStreamPort;
        return true;
    } else {
        mParent->setStatusImpl(RLibThermoStreamInitError);
        qDebug() << "[ERROR] socket {thermo-stream} not created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->thermoChannelStreamPort;
        return false;
    }
}

//-----------------------------------------------------------------------------
bool TDrvLayer::configVideoStream()
{
    if( mParent->getStatusImpl() != RLibNotInitialized)
        return false;

    const TRLibDevParams* devParams = mParent->getParams();
    T_UDP_RxChannel::TParams rxParams = {
                                           {
                                               CfgDefs::EthStreamPacketSize,          // netPacketSize
                                               CfgDefs::EthStreamPacketsInBuf,        // numPacketsInBundle
                                               128,                                   // numBundles
                                               THREAD_PRIORITY_HIGHEST,               // threadPriority
											   UdpLibTimeout,                         // timeout
                                               8*1024*1024,                           // socketBufSize
                                               devParams->videoChannelIP,             // peerAddr
                                               devParams->videoChannelStreamPort,     // peerPort
                                               T_UDP_LibAdapter::rxNotify             // onTransferReady
                                           },
                                           mParent->getBufPool().getPool<TDrvBufStreamETH::TDrvBufPool>() // msgBufPool
                                         };
    T_UDP_TxChannel::TParams txParams =  {
                                           {
                                               CfgDefs::EthCmdPacketSize,             // netPacketSize
                                               CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                               16,                                    // numBundles
                                               THREAD_PRIORITY_NORMAL,                // threadPriority
											   UdpLibTimeout,                         // timeout
                                               64*1024,                               // socketBufSize
                                               devParams->videoChannelIP,             // peerAddr
                                               devParams->videoChannelStreamPort,     // peerPort
                                               T_UDP_LibAdapter::txNotify             // onTransferReady
                                           },
                                           16                                         // txQueueLimit
                                         };

    if(T_UDP_LibAdapter::createSocket(devParams->hostIP,devParams->videoChannelStreamPort,&rxParams,&txParams) == UDP_LIB::Ok) {
        qDebug() << "[INFO] socket {video-stream} created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->videoChannelStreamPort;
        return true;
    } else {
        qDebug() << "[ERROR] socket {video-stream} not created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->videoChannelStreamPort;
        mParent->setStatusImpl(RLibVideoStreamInitError);
        return false;
    }
}

//-----------------------------------------------------------------------------
bool TDrvLayer::configThermoLens()
{
    if( mParent->getStatusImpl() != RLibNotInitialized)
        return false;

    const TRLibDevParams* devParams = mParent->getParams();

    UDP_LIB::TParams rxParams = {
                                    CfgDefs::EthCmdPacketSize,             // netPacketSize
                                    CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                    64,                                    // numBundles
                                    THREAD_PRIORITY_NORMAL,                // threadPriority
									UdpLibTimeout,                         // timeout
                                    64*1024,                               // socketBufSize
                                    devParams->thermoChannelIP,            // peerAddr
                                    devParams->thermoChannelLensPort,      // peerPort
									thermoCmdReplyCallback                 // onTransferReady
                                };
    UDP_LIB::TParams txParams = {
                                    CfgDefs::EthCmdPacketSize,             // netPacketSize
                                    CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                    64,                                    // numBundles
                                    THREAD_PRIORITY_NORMAL,                // threadPriority
									UdpLibTimeout,                         // timeout
                                    64*1024,                               // socketBufSize
                                    devParams->thermoChannelIP,            // peerAddr
                                    devParams->thermoChannelLensPort,      // peerPort
                                    0                                      // onTransferReady
                                };

    if(UDP_LIB::createSocket(devParams->hostIP,devParams->thermoChannelLensPort,&rxParams,&txParams) == UDP_LIB::Ok) {
        qDebug() << "[INFO] socket {thermo-lens} created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->thermoChannelLensPort;
        return true;
    } else {
        qDebug() << "[ERROR] socket {thermo-lens} not created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->thermoChannelLensPort;
        mParent->setStatusImpl(RLibThermoLensInitError);
        return false;
    }
}

//-----------------------------------------------------------------------------
bool TDrvLayer::configVideoDevGroup()
{
    if( mParent->getStatusImpl() != RLibNotInitialized)
        return false;

    const TRLibDevParams* devParams = mParent->getParams();

    UDP_LIB::TParams rxParams = {
                                    CfgDefs::EthCmdPacketSize,             // netPacketSize
                                    CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                    64,                                    // numBundles
                                    THREAD_PRIORITY_NORMAL,                // threadPriority
									UdpLibTimeout,                         // timeout
                                    64*1024,                               // socketBufSize
                                    devParams->videoChannelIP,             // peerAddr
									devParams->videoChannelDevGroupPort,   // peerPort
									videoDevGroupCmdReplyCallback          // onTransferReady
                                };
    UDP_LIB::TParams txParams = {
                                    CfgDefs::EthCmdPacketSize,             // netPacketSize
                                    CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                    64,                                    // numBundles
                                    THREAD_PRIORITY_NORMAL,                // threadPriority
									UdpLibTimeout,                         // timeout
                                    64*1024,                               // socketBufSize
                                    devParams->videoChannelIP,             // peerAddr
									devParams->videoChannelDevGroupPort,   // peerPort
                                    0                                      // onTransferReady
                                };

	if(UDP_LIB::createSocket(devParams->hostIP,devParams->videoChannelDevGroupPort,&rxParams,&txParams) == UDP_LIB::Ok) {
		qDebug() << "[INFO] socket {video-dev-group} created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->videoChannelDevGroupPort;
        return true;
    } else {
		qDebug() << "[ERROR] socket {video-lens} not created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->videoChannelDevGroupPort;
        mParent->setStatusImpl(RLibVideoLensInitError);
        return false;
    }
}

//-----------------------------------------------------------------------------
bool  TDrvLayer::configPlatform()
{
    if( mParent->getStatusImpl() != RLibNotInitialized)
        return false;

    const TRLibDevParams* devParams = mParent->getParams();

    UDP_LIB::TParams rxParams = {
                                    CfgDefs::EthCmdPacketSize,             // netPacketSize
                                    CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                    64,                                    // numBundles
                                    THREAD_PRIORITY_NORMAL,                // threadPriority
									UdpLibTimeout,                         // timeout
                                    64*1024,                               // socketBufSize
                                    devParams->platformIP,                 // peerAddr
                                    devParams->platformPort,               // peerPort
									platformCmdReplyCallback               // onTransferReady
                                };
    UDP_LIB::TParams txParams = {
                                    CfgDefs::EthCmdPacketSize,             // netPacketSize
                                    CfgDefs::EthCmdPacketsInBuf,           // numPacketsInBundle
                                    64,                                    // numBundles
                                    THREAD_PRIORITY_NORMAL,                // threadPriority
									UdpLibTimeout,                         // timeout
                                    64*1024,                               // socketBufSize
                                    devParams->platformIP,                 // peerAddr
                                    devParams->platformPort,               // peerPort
                                    0                                      // onTransferReady
                                };

    if(UDP_LIB::createSocket(devParams->hostIP,devParams->platformPort,&rxParams,&txParams) == UDP_LIB::Ok) {
        qDebug() << "[INFO] socket {platform} created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->platformPort;
        return true;
    } else {
        qDebug() << "[ERROR] socket {platform} not created" << "IP:" << hex << devParams->hostIP << "port:" << dec << devParams->platformPort;
        mParent->setStatusImpl(RLibPlatformInitError);
        return false;
    }
}

#endif
