#if !defined(VIDEO_FRAME_FORMER_H)
#define VIDEO_FRAME_FORMER_H

#include "app/CfgDefs.h"
#include "app/Msg.h"
#include "app/DrvBuf.h"
#include "app/Frame.h"

#include <opencv2/core/core.hpp>

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseFrameFormer
{
	public:
		TBaseFrameFormer(TMsgWrapperPoolQueue* framePool, TMsgWrapperPoolQueue* inQueue = 0, TMsgWrapperPoolQueue* outQueue = 0) :
			mFramePool(framePool),
			mInQueue(inQueue),
			mOutQueue(outQueue)
		{

		}
		virtual ~TBaseFrameFormer() {}
		int formFrames();

		TMsgWrapperPoolQueue* setInQueue(TMsgWrapperPoolQueue* inQueue)
		{
			qDebug() << "inQueue" << inQueue;
			TMsgWrapperPoolQueue* oldInQueue = mInQueue;
			mInQueue = inQueue;
			return oldInQueue;
		}

		TMsgWrapperPoolQueue* setOutQueue(TMsgWrapperPoolQueue* outQueue)
		{
			TMsgWrapperPoolQueue* oldOutQueue = mOutQueue;
			mOutQueue = outQueue;
			return oldOutQueue;
		}

	protected:
		virtual bool formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame) = 0;
		bool checkFrameCompatibility(const TBaseFrame* inFrame, const TBaseFrame* outFrame) const
		{
			if(!inFrame || !outFrame)
				return false;
			if((inFrame->height() == outFrame->height()) && (inFrame->width() == outFrame->width()))
				return true;
			else
				return false;
		}

		TMsgWrapperPoolQueue* mFramePool;
		TMsgWrapperPoolQueue* mInQueue;
		TMsgWrapperPoolQueue* mOutQueue;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TThermoFrameFormer : public TBaseFrameFormer
{
	public:
        TThermoFrameFormer(TMsgWrapperPoolQueue* framePool, TMsgWrapperPoolQueue* inQueue = 0, TMsgWrapperPoolQueue* outQueue = 0) :
			TBaseFrameFormer(framePool, inQueue, outQueue)
		{

		}
        ~TThermoFrameFormer() {}

	protected:
		bool formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame);
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TVideoFrameFormer : public TBaseFrameFormer
{
    public:
        TVideoFrameFormer(TMsgWrapperPoolQueue* framePool, TMsgWrapperPoolQueue* inQueue = 0, TMsgWrapperPoolQueue* outQueue = 0);
        ~TVideoFrameFormer() {}

    protected:
        bool formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame);
        #if defined(USE_TEST_DEMOSAIC_IMG)
            void genBayerTestFrame(TRawFrame::TPixel* inFrame, cv::Mat& testFrame, unsigned pixNum);

            unsigned mFrameNum;
            cv::Mat  mTestImg;
        #endif
		cv::Mat mGrayFrameMat;
		cv::Mat mOutFrameMat;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TInfoFrameFormer : public TBaseFrameFormer
{
	public:
		TInfoFrameFormer(TMsgWrapperPoolQueue* framePool, TMsgWrapperPoolQueue* inQueue = 0, TMsgWrapperPoolQueue* outQueue = 0) :
			TBaseFrameFormer(framePool, inQueue, outQueue)
		{
			if(!PaletteInitialized) {
				genPalette();
				PaletteInitialized = true;
			}
		}
		~TInfoFrameFormer() {}

	protected:
		bool formFrame(TBaseFrame* inFrame, TBaseFrame* outFrame);

	private:
		static const unsigned BasePaletteSize = 16;
		static const unsigned PaletteSize     = 256;

		static const unsigned ColorMask       = 0x0f;
		static const unsigned IntensityPos    = 4;
		static const unsigned IntensityMask   = 0x07;
		static const unsigned AlphaMask       = 0x80;

		static const QRgb BasePalette[BasePaletteSize];
		static QRgb Palette[PaletteSize];
		static bool PaletteInitialized;

		static void genPalette();
		static unsigned genAlpha(unsigned idx);
		static QRgb genColor(QRgb baseColor, unsigned intensity, int alpha);
};


#endif // VIDEO_FRAME_FORMER_H


