#if !defined(CFG_DEFS_H)
#define CFG_DEFS_H

#include <cstdint>

#define WORK_APPLICATION              0
#define TESTGEN_AT_FRAME_FORMER_LAYER 1
#define TESTGEN_AT_DRV_LAYER          2

#define WORK_MODE TESTGEN_AT_DRV_LAYER      // CONFIG

//*** test settings section ***************************************************
#define MSG_SELF_RELEASE				    // CONFIG
//*** test settings section (end) *********************************************

namespace CfgDefs {

//-----------------------------------------------------------------------------
typedef int64_t NetAddrWordType;
struct NetAddrType
{
	NetAddrType(NetAddrWordType nAddr = -1) : netAddr(nAddr) {}
	operator NetAddrWordType() const { return netAddr; }

	NetAddrWordType netAddr;
};
inline bool operator<(const NetAddrType& el1, const NetAddrType& el2) {
	return el1.netAddr < el2.netAddr;
}

//typedef NetAddrWordType TNetAddr;
typedef NetAddrType TNetAddr;
static TNetAddr NetNoAddr() { return TNetAddr(-1); }
//-----------------------------------------------------------------------------

static const int ThermoRawFrameNum       = 32;
static const int ThermoScreenFrameNum    = 32;
static const int ThermoScreenFrameWidth  = 640;
static const int ThermoScreenFrameHeight = 480;

static const int VideoRawFrameNum        = 32;
static const int VideoScreenFrameNum     = 32;
static const int VideoScreenFrameWidth   = 1280;
static const int VideoScreenFrameHeight  = 960;

static const int EthStreamBufNum         = 256;
static const int EthStreamPacketSize     = 1472;
static const int EthStreamPacketsInBuf   = 64;
static const int EthStreamBufSize        = EthStreamPacketSize*EthStreamPacketsInBuf;

static const int EthCmdBufNum            = 256;
static const int EthCmdPacketSize        = 1472;
static const int EthCmdPacketsInBuf      = 1;
static const int EthCmdBufSize           = EthCmdPacketSize*EthCmdPacketsInBuf;

static const unsigned DevNum             = 2;
static const unsigned ThermoStreamId     = 0; // must be always 0
static const unsigned VideoStreamId      = 1; // must be always 1
static const unsigned ThermoChannelId    = 1;
static const unsigned VideoChannelId     = 1;

//*** test settings section ***************************************************
extern TNetAddr getViewNetSrcFilter(unsigned channelId);

#define AF_TEST_INFO

//-----------------------------------------------------------------------------
#if (WORK_MODE == TESTGEN_AT_FRAME_FORMER_LAYER)
    static const int TestFramePeriod       = 40;	    // CONFIG
#endif

//-----------------------------------------------------------------------------
#if (WORK_MODE == TESTGEN_AT_DRV_LAYER)
    static const int TestDrvBufPeriod      = 2;	        // CONFIG
#endif

//-----------------------------------------------------------------------------
#if (WORK_MODE != TESTGEN_AT_FRAME_FORMER_LAYER)
	//#define USE_TEST_DEMOSAIC_IMG                     // CONFIG
    //#define FRAME_IP_PIPE								// CONFIG
#endif

//*** test settings section (end) *********************************************

}

#endif // CFG_DEFS_H


