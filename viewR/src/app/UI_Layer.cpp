//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/qmath.h>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtGui/QResizeEvent>
#include <QtCore/QDebug>
#include <QtCore/QCoreApplication> /*TEST*/
#include <QtWidgets/QDockWidget>
#include <QtCore/QSignalMapper>

#include <QtWidgets/QGroupBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>


#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/viewR.h"
#include "app/UI_Layer.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
T_UI_Layer::T_UI_Layer(TViewR* parent) :
                                QMainWindow(0, 0/* Qt::FramelessWindowHint*/),
                                mParent(parent),
								mMainScene(0),
								mMainView(0),
                                mThermoStreamPixmap(0),
                                mVideoStreamPixmap(0),
                                mTimer(),
                                mNoThermoStreamPixmap(CfgDefs::ThermoScreenFrameWidth,CfgDefs::ThermoScreenFrameHeight),
                                mNoVideoStreamPixmap(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight)
{
    setWindowTitle(tr("R view"));

    /*QStatusBar* statusInfo = */ //statusBar();
    /*TEST*/ //statusInfo->setSizeGripEnabled(false);
    /*TEST*/ //statusInfo->showMessage(tr("slonick"),2000);
    createMainScene();
    //mTimer.start();

    //---
    QSignalMapper* watchDogMapper = new QSignalMapper(this);
    for(unsigned k = 0; k < ChannelNum; ++k) {
        mWatchDogTimer[k].setTimerType(Qt::PreciseTimer);
        connect(&mWatchDogTimer[k],SIGNAL(timeout()),watchDogMapper,SLOT(map()));
        mWatchDogTimer[k].start(StreamTimeout);
        watchDogMapper->setMapping(&mWatchDogTimer[k],k);
    }
    connect(watchDogMapper,SIGNAL(mapped(int)),this,SLOT(watchDogSlot(int)));

    #if 1
        qDebug() << qPrintable(QString("[UI_Layer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
T_UI_Layer::~T_UI_Layer()
{
    #if 1
        qDebug() << qPrintable(QString("[UI_Layer]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
void T_UI_Layer::createMainScene()
{
    mMainScene = new TGraphicsScene(this);

    #if 0 /*TEST*/
        QRadialGradient gradient(0,0,100);
        gradient.setSpread(QGradient::RepeatSpread);
        mMainScene->setBackgroundBrush(gradient);
        // Qt::lightGray

    #endif
    //mMainScene->setBackgroundBrush(QColor(0x00,0x45,0x85));
    mMainScene->setBackgroundBrush(QColor(0x20,0x20,0x20));

    //initPixmapItem(CfgDefs::MainScreenFrameWidth,CfgDefs::MainScreenFrameHeight,QColor(0x00,0x00,0x00,255),0);
    #if defined(THERMO_CHANNEL_ENABLED)
        mNoThermoStreamPixmap.fill(QColor(0x00,0x00,0x70,255));
        mThermoStreamPixmap = initPixmapItem(CfgDefs::ThermoScreenFrameWidth,CfgDefs::ThermoScreenFrameHeight,mNoThermoStreamPixmap,2);
    #endif
    #if defined(VIDEO_CHANNEL_ENABLED)
        mNoVideoStreamPixmap.fill(QColor(0x00,0x00,0x80,255));
        mVideoStreamPixmap = initPixmapItem(CfgDefs::VideoScreenFrameWidth,CfgDefs::VideoScreenFrameHeight,mNoVideoStreamPixmap,1);
    #endif
	mMainView = new TGraphicsView(mMainScene,this);

    /*TEST*/ //mMainView->setAttribute(Qt::WA_PaintOnScreen);

    /*TEST*/ mMainView->setFrameStyle(QFrame::NoFrame);
    /*TEST*/ //mMainView->setFrameStyle(QFrame::Box | QFrame::Plain);

    /*TEST*/ //mMainView->setLineWidth(10);

    /*TEST*/ //mMainView->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    /*TEST*/ //mMainView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    /*TEST*/ //mMainView->setAlignment(Qt::AlignCenter);

	#if 1
        QDockWidget* dockControlWidget = new QDockWidget(0,Qt::WindowCloseButtonHint);
        dockControlWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
        dockControlWidget->setTitleBarWidget(new QWidget(dockControlWidget));

        QWidget* controlWidget = new QWidget;
		QVBoxLayout* mainLayout = new QVBoxLayout;

		#if defined(THERMO_CHANNEL_ENABLED)
			QGroupBox* thermoGroupBox = new QGroupBox(tr("Thermo channel"));
			QVBoxLayout* thermoLayout = new QVBoxLayout;
			QCheckBox* thermoOnCheckBox = new QCheckBox(tr("on/off"),this);
			thermoOnCheckBox->setObjectName(tr("thermo"));
			thermoLayout->addSpacing(8);
			thermoLayout->addWidget(thermoOnCheckBox);
			mThermoAfCheckBox = new QCheckBox(tr("AF on/off"),this);
			mThermoAfCheckBox->setObjectName(tr("thermo"));
			thermoLayout->addWidget(mThermoAfCheckBox);
			thermoGroupBox->setLayout(thermoLayout);
			mainLayout->addWidget(thermoGroupBox);
			thermoGroupBox->setStyleSheet("QGroupBox {color: rgb(250,20,20); border: 1px solid rgb(0,0,0);}");
			thermoOnCheckBox->setStyleSheet("color: rgb(0,0,0);");
			mThermoAfCheckBox->setStyleSheet("color: rgb(0,0,0);");
			mainLayout->addStretch(1);
			connect(thermoOnCheckBox,SIGNAL(stateChanged(int)),mParent,SLOT(streamControlSlot(int)));
			connect(mThermoAfCheckBox,SIGNAL(stateChanged(int)),mParent,SLOT(lensControlSlot(int)));
		#endif
		#if defined(VIDEO_CHANNEL_ENABLED)
			QGroupBox* videoGroupBox = new QGroupBox(tr("Video channel"));
			QVBoxLayout* videoLayout = new QVBoxLayout;
			QCheckBox* videoOnCheckBox = new QCheckBox(tr("on/off"),this);
			videoOnCheckBox->setObjectName(tr("video"));
			videoLayout->addSpacing(8);
			videoLayout->addWidget(videoOnCheckBox);
			mVideoAfCheckBox = new QCheckBox(tr("AF on/off"),this);
			mVideoAfCheckBox->setObjectName(tr("video"));
			videoLayout->addWidget(mVideoAfCheckBox);
			videoGroupBox->setLayout(videoLayout);
			mainLayout->addWidget(videoGroupBox);
			videoGroupBox->setStyleSheet("QGroupBox {color: rgb(20,20,250); border: 1px solid rgb(0,0,0);}");
			videoOnCheckBox->setStyleSheet("color: rgb(0,0,0);");
			mVideoAfCheckBox->setStyleSheet("color: rgb(0,0,0);");
			mainLayout->addStretch(1);
			connect(videoOnCheckBox,SIGNAL(stateChanged(int)),mParent,SLOT(streamControlSlot(int)));
			connect(mVideoAfCheckBox,SIGNAL(stateChanged(int)),mParent,SLOT(lensControlSlot(int)));
		#endif

		controlWidget->setLayout(mainLayout);
		const int ControlWidth = 180;
        dockControlWidget->setWidget(controlWidget);
        dockControlWidget->setMinimumWidth(ControlWidth);
        dockControlWidget->setMaximumWidth(ControlWidth);
        dockControlWidget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
        controlWidget->setStyleSheet("background-color: rgb(100,100,100); color: rgb(255,255,0)");
        addDockWidget(Qt::LeftDockWidgetArea, dockControlWidget);
    #endif

    setCentralWidget(mMainView);
}

//-----------------------------------------------------------------------------
QGraphicsPixmapItem*  T_UI_Layer::initPixmapItem(unsigned width, unsigned height, QPixmap& pixmap, qreal zValue)
{
    QGraphicsPixmapItem* pixmapItem = new QGraphicsPixmapItem(pixmap);
    pixmapItem->setZValue(zValue);
    qreal dx = (CfgDefs::MainScreenFrameWidth - width)/2;
    qreal dy = (CfgDefs::MainScreenFrameHeight - height)/2;
    qDebug() << "dx" << dx << "dy" << dy;
    pixmapItem->moveBy(dx,dy);
    /*TEST*/ pixmapItem->setCacheMode(QGraphicsItem::NoCache);
    mMainScene->addItem(pixmapItem);
    return pixmapItem;
}

//-----------------------------------------------------------------------------
QGraphicsPixmapItem* T_UI_Layer::initPixmapItem(unsigned width, unsigned height, QColor color, qreal zValue)
{
	QPixmap pixmap(width, height);
	pixmap.fill(color);
    return initPixmapItem(width,height,pixmap,zValue);
}

//-----------------------------------------------------------------------------
void T_UI_Layer::showFrame(TScreenFramePtr& framePtr, QGraphicsPixmapItem* pixmapItem)
{
	TBaseFrame* frame = checkMsg<TBaseFrame>(framePtr);
	if(frame) {
        //qDebug() << "showFrame:" << (*framePtr).msgId();
		QPixmap pixmap = QPixmap::fromImage(*(frame->getImage()));
		pixmapItem->setPixmap(pixmap);
		#if defined(FRAME_RESIZE_TEST)
			slon!
			mMainView->setSceneRect(mVideoStreamPixmap->boundingRect());
			QResizeEvent resizeEvent(mMainView->size(),mMainView->size());
			mMainView->resizeEvent(&resizeEvent);
		#endif
	} else {
		qDebug() << "[ERROR] T_UI_Layer::receiveFrameSlot src:" << framePtr->netSrc() << "dst:" << framePtr->netDst();
	}
}

//-----------------------------------------------------------------------------
void T_UI_Layer::receiveFrameSlot(TScreenFramePtr framePtr)
{
    if(framePtr) {
        /*TEST*/ // qDebug() << "Rx Frame:" << framePtr->netDst();//<< SysUtils::getThreadId();
        switch(framePtr->netDst()) {
            case CfgDefs::ThermoStreamId:
                mStreamExist[CfgDefs::ThermoStreamId].store(1);
                mWatchDogTimer[CfgDefs::ThermoStreamId].start(StreamTimeout);
                showFrame(framePtr,mThermoStreamPixmap);
				break;
            case CfgDefs::VideoStreamId:
                mStreamExist[CfgDefs::VideoStreamId].store(1);
                mWatchDogTimer[CfgDefs::VideoStreamId].start(StreamTimeout);
				showFrame(framePtr,mVideoStreamPixmap);
				break;
			default:
				qDebug() << "this dst id not showed on the screen" << framePtr->netDst();
				break;
		}
        #if !defined(MSG_SELF_RELEASE)
            releaseMsg(framePtr);
        #endif
    } else {
        qDebug() << "[ERROR] Rx Frame";
    }
}

//-----------------------------------------------------------------------------
void T_UI_Layer::watchDogSlot(int channelId)
{
    if(mStreamExist[channelId].testAndSetAcquire(1,0)) {
        #if defined(THERMO_CHANNEL_ENABLED)
            if(channelId == CfgDefs::ThermoStreamId) {
                mThermoStreamPixmap->setPixmap(mNoThermoStreamPixmap);
            }
        #endif
        #if defined(VIDEO_CHANNEL_ENABLED)
            if(channelId == CfgDefs::VideoStreamId) {
                mVideoStreamPixmap->setPixmap(mNoVideoStreamPixmap);
            }
        #endif
    }
}

//-----------------------------------------------------------------------------
void T_UI_Layer::setAfCheckBox(unsigned channelId, Qt::CheckState checkState)
{
	#if defined(THERMO_CHANNEL_ENABLED)
		if(channelId == CfgDefs::ThermoStreamId)
			mThermoAfCheckBox->setCheckState(checkState);
	#endif
	#if defined(VIDEO_CHANNEL_ENABLED)
		if(channelId == CfgDefs::VideoStreamId)
			mVideoAfCheckBox->setCheckState(checkState);
	#endif
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TGraphicsScene::TGraphicsScene(QObject* parent) : QGraphicsScene(parent)
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsScene]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
TGraphicsScene::~TGraphicsScene()
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsScene]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TGraphicsView::TGraphicsView(QGraphicsScene* scene, T_UI_Layer* manager) :
                                                                              QGraphicsView(scene),
                                                                              mManager(manager)
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsView]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("constructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
TGraphicsView::~TGraphicsView()
{
    #if 1
        qDebug() << qPrintable(QString("[GraphicsView]").leftJustified(SysUtils::ClassNameJustify))
                 << SysUtils::getThreadId()
                 << qPrintable(QString("destructor").leftJustified(SysUtils::JobNameJustify));
    #endif
}

//-----------------------------------------------------------------------------
void TGraphicsView::resizeEvent(QResizeEvent* event)
{
    QSize newSize = event->size();
    const int dX = 20;
    const int dY = 20;
    qreal viewScale = 1.0;

	#if 1
        const int width  = CfgDefs::MainScreenFrameWidth;
        const int height = CfgDefs::MainScreenFrameHeight;
    #else
        const int width  = mManager->getVideoPixmap()->pixmap().width();
        const int height = mManager->getVideoPixmap()->pixmap().height();
    #endif

    if((newSize.width() > (width + dX)) && (newSize.height() > ( + dY))) {
        qreal scaleX = (contentsRect().width()  - dX)/static_cast<qreal>(width);
        qreal scaleY = (contentsRect().height() - dY)/static_cast<qreal>(height);
        viewScale = qMin(scaleX,scaleY);
    }
    setTransform(QTransform::fromScale(viewScale,viewScale),false);
    /*TEST*/ //testViewRelativeCrd(newSize,viewScale);
    QGraphicsView::resizeEvent(event);
}

//-----------------------------------------------------------------------------
/*TEST*/ void TGraphicsView::testViewRelativeCrd(QSize size, qreal scale)
{
    QPointF p = mapToScene(QPoint(0,0));
    qreal dX = (p.x() > 0) ? 0.0 : qFabs(p.x());
    qreal dY = (p.y() > 0) ? 0.0 : qFabs(p.y());

    qreal x = (contentsRect().width() - scale*2*dX)/scale;
    qreal y = (contentsRect().height() - scale*2*dY)/scale;
    qDebug() << "[Viewport]" << size << rect() << contentsRect() /*<< sceneRect()*/ << p << "x:" << x << "y:" << y << "scale:" << scale;
}

