greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE    = app
TARGET      = viewR
CONFIG     += console
DEPENDPATH += .
TOPDIR      = ../..

include($$TOPDIR/common.pri)
include($$TOPDIR/build.pri)

HEADERS    += \
               BufPool.h          \
               tqueue.h           \
               tthread.h          \
               msg.h              \
               RawBuf.h           \
               DrvBuf.h           \
               SysUtils.h         \
               Frame.h            \
               FrameFormerLayer.h \
               UI_Layer.h         \
               viewR.h            \
               CfgDefs.h
SOURCES    += \
               BufPool.cpp          \
               FrameFormerLayer.cpp \
               UI_Layer.cpp              \
               viewR.cpp            \
               main.cpp
QT         += testlib

win32 {
    LIBS += -lsetupapi -luuid -ladvapi32
}

LIBS += -llibR

#------------------------------------------------------------------------------------------------
# notes:
#
# QT += core gui - not need because core and gui are included by default


# FORMS += \
