#if !defined(SYS_UTILS_H)
#define SYS_UTILS_H

#include <QtCore/QThread>
#include <QtCore/QString>
#include <QtCore/QElapsedTimer>
#include <QtCore/QSystemSemaphore>
#include <QtCore/QSharedMemory>

namespace SysUtils {

static const int ClassNameJustify = 30;
static const int JobNameJustify   = 16;

//-----------------------------------------------------------------------------
inline const char* getThreadId()
{
    #if _WIN64
        return qPrintable(QString("thread 0x%1").arg(int64_t(QThread::currentThread()),16,16,QLatin1Char('0')));
    #else
        return qPrintable(QString("thread 0x%1").arg(int(QThread::currentThread()),8,16,QLatin1Char('0')));
    #endif
}

//-----------------------------------------------------------------------------
inline QString getElapsedTime(const QElapsedTimer& timer)
{
    const int TimeGroupNum = 3;
    const int TimeGroupLen = 3;

    QString timeStr = QString("%1").arg(timer.nsecsElapsed()/1000,TimeGroupNum*TimeGroupLen,10,QLatin1Char('0'));
    for(int n = TimeGroupNum-1; n; --n)
        timeStr.insert(n*TimeGroupLen,':');
	timeStr += " us";
    return timeStr;
}

//-----------------------------------------------------------------------------
template<typename T> class TypeTraits
{
	private:
		template <typename U> struct PointerTraits
		{
			enum { result = false };
		};
		template <typename U> struct PointerTraits<U*>
		{
			enum { result = true };
		};

	public:
		enum { isPointer = PointerTraits<T>::result };
};


//****************************************************************************************
// Enumerator - generate unique classId for class type
//
//
//              The special class (Enumerator family) for this action is need because
//              classId's must be the same for all TMsgWrappers which are differenced
//              only by MsgDeletor type
//****************************************************************************************

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseTypeEnumerator
{
	protected:
		static int generateClassId() { static int classId; return ++classId; }
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
template<typename T> class TTypeEnumerator : public TBaseTypeEnumerator
{
	public:
		static int classId() { static const int classId = TBaseTypeEnumerator::generateClassId(); return classId; }
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TAppSingleton
{
	public:
		TAppSingleton(QString name) : mAppSem(name + "_SEM", 1), mSharedMem(name + "_MEM"), mIsRunning(false)
		{
			mAppSem.acquire();

			{
				QSharedMemory sharedMem(name + "_MEM");
				sharedMem.attach();
			}
			if(mSharedMem.attach()) {
				mIsRunning = true;
			}
			else {
				mSharedMem.create(1);
				mIsRunning = false;
			}
			mAppSem.release();
		}
		~TAppSingleton() {}
		bool isRunning() const { return mIsRunning; }

	private:
		bool				mIsRunning;
		QSharedMemory		mSharedMem;
		QSystemSemaphore	mAppSem;
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
inline bool isFuzzyEqual(double x1, double x2, double tol = 0.02) {
	if(qAbs(x1-x2)/(x2 + DBL_EPSILON) > tol)
		return false;
	else
		return true;
}

}


#endif // SYS_UTILS_H


