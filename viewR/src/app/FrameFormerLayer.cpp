//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/FrameFormerLayer.h"
#include "app/viewR.h"

#include "libR.h"


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
TFrameFormerLayer::TFrameFormerLayer(TViewR* parent) :
                                     TThread(L"FrameFormerThread(viewer)"),
                                     mParent(parent),
                                     mSemaphore(),
                                     mScreenFrameQueue(),
                                     mFrameNotifyQueue(),
                                     mFrameBuf(0)
{
    mFrameBuf = new uint8_t [getByteBufSize()];
    qDebug() << "[FrameFormerLayer(viewer)]" << SysUtils::getThreadId();
}

//-----------------------------------------------------------------------------
TFrameFormerLayer::~TFrameFormerLayer()
{
    threadFinish();
    delete [] mFrameBuf;
    mFrameBuf = 0;
    qDebug() << "[~FrameFormerLayer(viewer)]" << SysUtils::getThreadId();
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::onExec()
{
    //---
    int channelId = waitForFrame();
    if(threadExit() || (channelId < 0))
        return threadExit();

    //---
    TScreenFramePtr framePtr;
    if(isChannelEnabled(channelId) && getScreenFramePtr(channelId,framePtr)) {
          emit sendFrameSignal(framePtr);
    }
    return threadExit();
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::isChannelEnabled(int channelId)
{
    #if defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
        return (channelId == CfgDefs::ThermoStreamId) || (channelId == CfgDefs::VideoStreamId);
    #elif defined(THERMO_CHANNEL_ENABLED) && !defined(VIDEO_CHANNEL_ENABLED)
        return (channelId == CfgDefs::ThermoStreamId);
    #elif !defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
        return (channelId == CfgDefs::VideoStreamId);
    #else
        return false;
    #endif
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::getScreenFramePtr(unsigned channelId, TScreenFramePtr& dstFramePtr)
{
    TBaseFrame*     dstFrame;
    TRLibFrame      srcFrame;

    srcFrame.buf         = mFrameBuf;
    srcFrame.byteBufSize = getByteBufSize();
    srcFrame.status      = (RLibStatus)1;

    srcFrame.channelId = channelId;

    if(libRgetFrame(channelId,&srcFrame) && (srcFrame.status == RLibOk)) {
        /*TEST*/ //qDebug() << srcFrame.channelId << srcFrame.frameNum << srcFrame.byteBufSize << srcFrame.pixelSize << srcFrame.status;
        if(getFrame(dstFramePtr,channelId) && (dstFrame = checkMsg<TBaseFrame>(dstFramePtr)) && checkFrameCompatibility(channelId,&srcFrame,dstFrame)) {
            dstFramePtr->setNetDst(srcFrame.channelId);
            dstFramePtr->setMsgId(srcFrame.frameNum);
            TScreenFrame::TPixel* dstPixelBuf = dstFrame->getPixelBuf<TScreenFrame>();
            if(channelId == CfgDefs::ThermoStreamId) {
                for(unsigned n = 0; n < srcFrame.byteBufSize; ++n) {
                    uint8_t pixVal = *srcFrame.buf++;
                    *dstPixelBuf++ = qRgba(pixVal,pixVal,pixVal,255);
                }
            } else {
                std::memcpy(dstPixelBuf,srcFrame.buf,srcFrame.byteBufSize);
            }
            return true;
        } else {
            qDebug() << "[ERROR] [TFrameFormerLayer::getScreenFramePtr] (2)";
        }
    } else {
        qDebug() << "[ERROR] [TFrameFormerLayer::getScreenFramePtr] (1)" << srcFrame.status;
    }
    return false;
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::checkFrameCompatibility(unsigned channelId, const TRLibFrame* src, const TBaseFrame* dst)
{
	//qDebug() << channelId << src->pixelSize;

    bool pixelSizeOk = (channelId == CfgDefs::ThermoStreamId) ? (src->pixelSize == 1) : (src->pixelSize == sizeof(TScreenFrame::TPixel));
    return ((src->frameHeight == dst->height()) && (src->frameWidth == dst->width()) && pixelSizeOk);
}

//-----------------------------------------------------------------------------
void TFrameFormerLayer::receiveFrameNotifyfSlot(int channelId)
{
    mFrameNotifyQueue.put(channelId);
    mSemaphore.release();
}

//-----------------------------------------------------------------------------
int TFrameFormerLayer::waitForFrame()
{
    mSemaphore.acquire();
    int channelId = -1;
    mFrameNotifyQueue.get(channelId);
    return channelId;
}

//-----------------------------------------------------------------------------
bool TFrameFormerLayer::getFrame(TScreenFramePtr& frame, unsigned devId)
{
    if(devId == CfgDefs::ThermoStreamId) {
        return mParent->getBufPool().getBuf<TThermoScreenFrame::TFramePool>(frame);
    }
    if(devId == CfgDefs::VideoStreamId) {
        return mParent->getBufPool().getBuf<TVideoScreenFrame::TFramePool>(frame);
    }
    return false;
}

