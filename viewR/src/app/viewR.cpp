//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include <QtCore/qmath.h>
#include <QtCore/QDebug>

//#include "app/CfgDefs.h"
//#include "app/SysUtils.h"
#include "app/viewR.h"
#include "app/UI_Layer.h"
#include "app/FrameFormerLayer.h"
#include "app/DrvBuf.h"

#include "libR.h"

#pragma comment(lib, "Ws2_32.lib")

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

TViewR* TViewR::mInstance = 0;

//-----------------------------------------------------------------------------
void TViewR::afReplyNotify(unsigned channelId, TAfStatus* status)
{
	if((TViewR::getInstance()->mAfParams[channelId].mode == RLibLensAfMode) && (status->params.mode == RLibLensDirectMode)) {
		TViewR::getInstance()->mAfParams[channelId].mode = RLibLensDirectMode;
		TViewR::getInstance()->mUiLayer->setAfCheckBox(channelId,Qt::Unchecked);
	}

	if(status->params.mode == RLibLensAfMode) {
		qDebug() << "ch:" << channelId << "mode:" << status->params.mode << "state:" << status->afState << "focus:" << status->focusValue << "focus func:" << status->focusFuncValue;
	}
}

//-----------------------------------------------------------------------------
TViewR::TViewR() : QObject(),
                   mUiLayer(0),
                   mFrameFormerLayer(0),
                   mBufPool(),
                   mTestTimer()

{
    //---
    mBufPool.insertPool(new TThermoScreenFrame::TFramePool(CfgDefs::ThermoScreenFrameNum,TThermoScreenFrame::TCreator()));
    mBufPool.insertPool(new TVideoScreenFrame::TFramePool(CfgDefs::VideoScreenFrameNum,TVideoScreenFrame::TCreator()));
    mBufPool.insertPool(new TThermoRawFrame::TFramePool(CfgDefs::ThermoRawFrameNum,TThermoRawFrame::TCreator()));
    mBufPool.insertPool(new TVideoRawFrame::TFramePool(CfgDefs::VideoRawFrameNum,TVideoRawFrame::TCreator()));

    #if 1
        qDebug() << "\n*** class Id's (1) ***";
        qDebug() << "TThermoScreenFrame::TFramePool classId:"  << SysUtils::TTypeEnumerator<TThermoScreenFrame::TFramePool>::classId();
        qDebug() << "TVideoScreenFrame::TFramePool classId: "  << SysUtils::TTypeEnumerator<TVideoScreenFrame::TFramePool>::classId();
        qDebug() << "TThermoRawFrame::TFramePool classId:   "  << SysUtils::TTypeEnumerator<TThermoRawFrame::TFramePool>::classId();
        qDebug() << "TVideoRawFrame::TFramePool classId:    "  << SysUtils::TTypeEnumerator<TVideoRawFrame::TFramePool>::classId();

        qDebug() << "\n*** class Id's (2) ***";
        qDebug() << "TThermoScreenFrame classId:" << SysUtils::TTypeEnumerator<TThermoScreenFrame>::classId();
        qDebug() << "TVideoScreenFrame classId: " << SysUtils::TTypeEnumerator<TVideoScreenFrame>::classId();
        qDebug() << "TThermoRawFrame classId:   " << SysUtils::TTypeEnumerator<TThermoRawFrame>::classId();
        qDebug() << "TVideoRawFrame classId:    " << SysUtils::TTypeEnumerator<TVideoRawFrame>::classId();

        qDebug() << "\n*** class Id's (3) ***";
        qDebug() << "TRawBuf classId:          " << SysUtils::TTypeEnumerator<TRawBuf>::classId();
        qDebug() << "TBaseFrame classId:       " << SysUtils::TTypeEnumerator<TBaseFrame>::classId();
    #endif

    //---
    qRegisterMetaType<TDrvBufPtr>("TDrvBufPtr");
    qRegisterMetaType<TScreenFramePtr>("TScreenFramePtr");

    //---
    mUiLayer          = new T_UI_Layer(this);
    mFrameFormerLayer = new TFrameFormerLayer(this);

    //---
    //connect(mDrvLayer,&TDrvLayer::sendDrvBufSignal,mFrameFormerLayer,&TFrameFormerLayer::receiveDrvBufSlot,Qt::DirectConnection);
    connect(mFrameFormerLayer,&TFrameFormerLayer::sendFrameSignal,mUiLayer,&T_UI_Layer::receiveFrameSlot,Qt::QueuedConnection);
    connect(this,&TViewR::sendFrameNotifySignal,mFrameFormerLayer,&TFrameFormerLayer::receiveFrameNotifyfSlot,Qt::DirectConnection);

    //---
    mFrameFormerLayer->start(QThread::InheritPriority);

    //---
    TRLibDevParams params =
    {
        inet_addr(CfgDefs::HostIP),
        TViewR::frameReadyNotify,
		0,										// CmdReplyNotify
		afReplyNotify,  						// AfReplyNotify
        inet_addr(CfgDefs::ThermoChannelIP),
        CfgDefs::ThermoChannelTestPort,
        CfgDefs::ThermoChannelLensPort,
        CfgDefs::ThermoChannelStreamPort,
        inet_addr(CfgDefs::VideoChannelIP),
        CfgDefs::VideoChannelTestPort,
		CfgDefs::VideoChannelDevGroupPort,
        CfgDefs::VideoChannelStreamPort,
        inet_addr(CfgDefs::PlatformIP),
        CfgDefs::PlatformPort
    };

	mInstance = this;
    RLibStatus status;
    status = libRinit(&params);

	#if defined(THERMO_CHANNEL_ENABLED)
		TAfParams thermoAfParams;
		libRGetLensControlMode(CfgDefs::ThermoStreamId, &thermoAfParams);
		thermoAfParams.afAvgNum = 1;
		//thermoAfParams.afAlgType = 0;
		thermoAfParams.afCallbackMode = /*RLibAfCallbackNormal*/ RLibAfCallbackAlways;
		libRSetLensControlMode(CfgDefs::ThermoStreamId, &thermoAfParams);
	#endif
	#if defined(VIDEO_CHANNEL_ENABLED)
		TAfParams videoAfParams;
		libRGetLensControlMode(CfgDefs::VideoStreamId, &videoAfParams);
		videoAfParams.afAvgNum = 1;
		//videoAfParams.afAlgType = 0;
		videoAfParams.afCallbackMode = /*RLibAfCallbackNormal*/ RLibAfCallbackAlways;
		libRSetLensControlMode(CfgDefs::VideoStreamId, &videoAfParams);
	#endif

	//---
	if(!libRGetLensControlMode(CfgDefs::ThermoStreamId, &mAfParams[CfgDefs::ThermoStreamId])) {
		qDebug() << "[ERROR] libRGetLensControlMode for Thermo channel";
	}
	if(!libRGetLensControlMode(CfgDefs::VideoStreamId, &mAfParams[CfgDefs::VideoStreamId])) {
		qDebug() << "[ERROR] libRGetLensControlMode for Video channel";
	}

    //libRsetVideoStreamMode(RVideoStreamMode::RLibRawVideo);
    //RLibRawVideo               =  0,
    //RLibGrayScaleVideo         =  1,
    //RLibColorVideo             =  2

    #if 0 // TEST
        mTestTimer.setTimerType(Qt::PreciseTimer);
        connect(&mTestTimer,&QTimer::timeout,this,&TViewR::testTimerSlot);
        mTestTimer.start(500);
    #endif

    qDebug() << "[INFO] [TViewR] libRinit status:" << status << SysUtils::getThreadId();;
}

//-----------------------------------------------------------------------------
TViewR::~TViewR()
{
    //libRfinish();
    libRcleanUp();
    delete mFrameFormerLayer;
    mFrameFormerLayer = 0;
    delete mUiLayer;
    mUiLayer = 0;
    mInstance = 0;
    qDebug() << "[INFO] [~TViewR]" << SysUtils::getThreadId();
}

//-----------------------------------------------------------------------------
void TViewR::showUi()
{
    mUiLayer->show();
    //mUiLayer->showMinimized();
}

//-----------------------------------------------------------------------------
void TViewR::frameReadyNotify(unsigned channelId, unsigned /*byteFrameSize*/, unsigned /*framesInQueue*/)
{
    if(getInstance()) {
        emit getInstance()->sendFrameNotifySignal(channelId);
    }
    //qDebug() << "channelId:" << channelId << "frame size(bytes):" << byteFrameSize << "frames in queue:" << framesInQueue;
}

//-----------------------------------------------------------------------------
void TViewR::testTimerSlot()
{
    #if 0
        static const unsigned CmdLen = 32;
        static uint8_t Buf[CmdLen];
        static uint8_t Count;

        for(unsigned k = 0; k < CmdLen; ++k) {
            Buf[k] = Count++;
        }
        bool s0 = libRLensSendCmd(0,Buf,CmdLen);
        bool s1 = libRLensSendCmd(1,Buf,CmdLen);
        bool s2 = libRPlatformSendCmd(Buf,CmdLen);

        qDebug() << "[TEST]" << Count << s0 << s1 << s2;
    #endif
}

//-----------------------------------------------------------------------------
void TViewR::streamControlSlot(int streamCmd)
{
	/*TEST*/ //qDebug() << "[INFO] [TViewR::streamControlSlot] sender:" << sender()->objectName() << "cmd:" << streamCmd;
	unsigned cmd = streamCmd ? 1 : 0;
	if(sender()->objectName() == QString("thermo")) {
		libRstreamCmd(CfgDefs::ThermoStreamId,cmd);
	}
	if(sender()->objectName() == QString("video")) {
		libRstreamCmd(CfgDefs::VideoStreamId,cmd);
	}
}

//-----------------------------------------------------------------------------
void TViewR::lensControlSlot(int lensCmd)
{
	/*TEST*/ qDebug() << "[INFO] [TViewR::lensControlSlot] sender:" << sender()->objectName() << "cmd:" << lensCmd;
	int8_t lensMode = (lensCmd == 0) ? RLibLensDirectMode : RLibLensAfMode;
	QCheckBox* checkBox = static_cast<QCheckBox*>(sender());
	Qt::CheckState currState = checkBox->checkState();
	Qt::CheckState prevState = (currState == Qt::Unchecked) ? Qt::Checked : Qt::Unchecked;

	if(sender()->objectName() == QString("thermo")) {
		int8_t prevMode = mAfParams[CfgDefs::ThermoStreamId].mode;
		mAfParams[CfgDefs::ThermoStreamId].mode = lensMode;
		if(!libRSetLensControlMode(CfgDefs::ThermoStreamId,&mAfParams[CfgDefs::ThermoStreamId])) {
			checkBox->setCheckState(prevState);
			mAfParams[CfgDefs::ThermoStreamId].mode = prevMode;
			qDebug() << "[ERROR] lensControlSlot (thermo)";
		}
	}
	if(sender()->objectName() == QString("video")) {
		int8_t prevMode = mAfParams[CfgDefs::VideoStreamId].mode;
		mAfParams[CfgDefs::VideoStreamId].mode = lensMode;
		if(!libRSetLensControlMode(CfgDefs::VideoStreamId,&mAfParams[CfgDefs::VideoStreamId])) {
			checkBox->setCheckState(prevState);
			mAfParams[CfgDefs::VideoStreamId].mode = prevMode;
			qDebug() << "[ERROR] lensControlSlot (video)";
		}
	}
}
