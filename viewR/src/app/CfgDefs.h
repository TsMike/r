#if !defined(CFG_DEFS_H)
#define CFG_DEFS_H

#include <cstdint>

namespace CfgDefs {

#define THERMO_CHANNEL_ENABLED
#define VIDEO_CHANNEL_ENABLED


//-----------------------------------------------------------------------------
typedef int64_t NetAddrWordType;
struct NetAddrType
{
	NetAddrType(NetAddrWordType nAddr = -1) : netAddr(nAddr) {}
	operator NetAddrWordType() const { return netAddr; }

	NetAddrWordType netAddr;
};
inline bool operator<(const NetAddrType& el1, const NetAddrType& el2) {
	return el1.netAddr < el2.netAddr;
}

//typedef NetAddrWordType TNetAddr;
typedef NetAddrType TNetAddr;
static TNetAddr NetNoAddr() { return TNetAddr(-1); }
//-----------------------------------------------------------------------------
static const char* HostIP                     = "192.168.10.1";
static const char* ThermoChannelIP            = "192.168.10.2";
static const char* VideoChannelIP             = "192.168.10.3";
static const char* PlatformIP                 = "192.168.10.4";

#if defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
	static const unsigned ThermoChannelTestPort    = 50000;
	static const unsigned ThermoChannelLensPort    = 50001;
	static const unsigned ThermoChannelStreamPort  = 50002;

	static const unsigned VideoChannelTestPort     = 50010;
	static const unsigned VideoChannelDevGroupPort = 50011;
	static const unsigned VideoChannelStreamPort   = 50012;

	static const unsigned PlatformPort             = 6000;
#elif defined(THERMO_CHANNEL_ENABLED) && !defined(VIDEO_CHANNEL_ENABLED)
	static const unsigned ThermoChannelTestPort    = 50000;
	static const unsigned ThermoChannelLensPort    = 50001;
	static const unsigned ThermoChannelStreamPort  = 50002;

	static const unsigned VideoChannelTestPort     = 51010;
	static const unsigned VideoChannelDevGroupPort = 51011;
	static const unsigned VideoChannelStreamPort   = 51012;

	static const unsigned PlatformPort             = 6000;
#elif !defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
	static const unsigned ThermoChannelTestPort    = 51000;
	static const unsigned ThermoChannelLensPort    = 51001;
	static const unsigned ThermoChannelStreamPort  = 51002;

	static const unsigned VideoChannelTestPort     = 50010;
	static const unsigned VideoChannelDevGroupPort = 50011;
	static const unsigned VideoChannelStreamPort   = 50012;

	static const unsigned PlatformPort             = 6001;
#else
#endif

static const int ThermoRawFrameNum       = 32;
static const int ThermoScreenFrameNum    = 32;
static const int ThermoScreenFrameWidth  = 640;
static const int ThermoScreenFrameHeight = 480;

static const int VideoRawFrameNum        = 32;
static const int VideoScreenFrameNum     = 32;
static const int VideoScreenFrameWidth   = 1280;
static const int VideoScreenFrameHeight  = 960;

#if defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
    static const int MainScreenFrameWidth   = qMax(ThermoScreenFrameWidth,VideoScreenFrameWidth);
    static const int MainScreenFrameHeight  = qMax(ThermoScreenFrameHeight,VideoScreenFrameHeight);
#elif defined(THERMO_CHANNEL_ENABLED) && !defined(VIDEO_CHANNEL_ENABLED)
    static const int MainScreenFrameWidth   = ThermoScreenFrameWidth;
    static const int MainScreenFrameHeight  = ThermoScreenFrameHeight;
#elif !defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
    static const int MainScreenFrameWidth   = VideoScreenFrameWidth;
    static const int MainScreenFrameHeight  = VideoScreenFrameHeight;
#else
#endif

static const unsigned DevNum            = 2;
static const unsigned ThermoStreamId    = 0; // must be always 0
static const unsigned VideoStreamId     = 1; // must be always 1

}

#define MSG_SELF_RELEASE				// MUST BE ON!

#endif // CFG_DEFS_H


