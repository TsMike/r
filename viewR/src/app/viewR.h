#if !defined(VIEW_R_H)
#define VIEW_R_H

#include <QtCore/QObject>
#include <QtCore/QTimer>

#include "app/CfgDefs.h"
#include "app/BufPool.h"

#include "libRDefs.h"

//-----------------------------------------------------------------------------
class T_UI_Layer;
class TFrameFormerLayer;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TViewR : public QObject
{
    Q_OBJECT

    signals:
        void sendFrameNotifySignal(int);

    public:
        TViewR();
        ~TViewR();
        void showUi();
        TBufPool& getBufPool() { return mBufPool; }
        static TViewR* getInstance() { return mInstance; }

    public slots:
         void testTimerSlot();
         void streamControlSlot(int);
		 void lensControlSlot(int);

    private:
        static void frameReadyNotify(unsigned channelId, unsigned byteFrameSize, unsigned framesInQueue);
		static void afReplyNotify(unsigned channelId, TAfStatus* status);

        static TViewR*        mInstance;
        T_UI_Layer*           mUiLayer;
        TFrameFormerLayer*    mFrameFormerLayer;
        TBufPool              mBufPool;
        QTimer                mTestTimer;
		TAfParams             mAfParams[CfgDefs::DevNum];
};

#endif // VIEW_R_H


