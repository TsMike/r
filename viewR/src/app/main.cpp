//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include <QtWidgets/QApplication>
#include <QTextCodec>
//#include <QtCore/QDebug>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/viewR.h"
#include "app/DrvBuf.h"

QT_USE_NAMESPACE

#if defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
    #define APP_NAME "VIEW_R"
#elif defined(THERMO_CHANNEL_ENABLED) && !defined(VIDEO_CHANNEL_ENABLED)
    #define APP_NAME "VIEW_RT"
#elif !defined(THERMO_CHANNEL_ENABLED) && defined(VIDEO_CHANNEL_ENABLED)
    #define APP_NAME "VIEW_RV"
#else
#endif


//-----------------------------------------------------------------------------
SysUtils::TAppSingleton AppSingleton(APP_NAME);

//-----------------------------------------------------------------------------
int main(int argc, char** argv)
{
    if(AppSingleton.isRunning()) {
        qDebug() << "Attempt to start the second App instance";
        return 0;
    }

    QApplication app(argc, argv);
    #if defined(Q_OS_WIN)
        if (QLocale::system().country() == QLocale::RussianFederation)
            QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM866"));
    #endif
    TViewR viewR;
    viewR.showUi();

    return app.exec();
}
