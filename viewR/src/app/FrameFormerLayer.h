#if !defined(FRAME_FORMER_LAYER_H)
#define FRAME_FORMER_LAYER_H

#include <QtCore/QSemaphore>

#include "app/CfgDefs.h"
#include "app/SysUtils.h"
#include "app/tthread.h"
#include "app/Frame.h"
#include "app/DrvBuf.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TBaseFrameFormer;
class TViewR;
struct TRLibFrame;

class TFrameFormerLayer : public TThread
{
    Q_OBJECT

    public slots:
        void receiveFrameNotifyfSlot(int channelId);

    signals:
        void sendFrameSignal(TScreenFramePtr);

    protected:
        virtual bool onExec();
        void threadFinish() { setThreadExit(); mSemaphore.release();  TThread::threadFinish(); }

    public:
        explicit TFrameFormerLayer(TViewR* parent);
        ~TFrameFormerLayer();
        static unsigned getByteBufSize() { return CfgDefs::MainScreenFrameHeight*CfgDefs::MainScreenFrameWidth*sizeof(TScreenFrame::TPixel); }

    private:
        typedef TQueue<int,TQueueSl,TWinCsGuard> TFrameNotifyQueue;

        bool getFrame(TScreenFramePtr& testFrame, unsigned devId);
        int waitForFrame();
        bool getScreenFramePtr(unsigned channelId, TScreenFramePtr& dstFramePtr);
        bool checkFrameCompatibility(unsigned channelId, const TRLibFrame* src, const TBaseFrame* dst);
        bool isChannelEnabled(int channelId);

        TViewR*               mParent;
        QSemaphore            mSemaphore;
        TMsgWrapperPoolQueue  mScreenFrameQueue;
        TFrameNotifyQueue     mFrameNotifyQueue;
        uint8_t*              mFrameBuf;
};

#endif // FRAME_FORMER_LAYER_H


