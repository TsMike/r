#if !defined(UI_LAYER_H)
#define UI_LAYER_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QCheckBox>
#include <QtCore/QTimer>
#include <QtCore/QAtomicInt>

#include "app/Frame.h"
#include "app/RawBuf.h"

//-----------------------------------------------------------------------------
class TGraphicsScene;
class TGraphicsView;
class TViewR;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class T_UI_Layer : public QMainWindow
{
 Q_OBJECT

 public:
    static const unsigned ChannelNum    = 2;
    static const unsigned StreamTimeout = 500;

    explicit T_UI_Layer(TViewR* parent);
    ~T_UI_Layer();
    QGraphicsPixmapItem* getVideoPixmap() { return mVideoStreamPixmap; }
	void setAfCheckBox(unsigned channelId, Qt::CheckState checkState);

 public slots:
   void receiveFrameSlot(TScreenFramePtr);
   void watchDogSlot(int);

 private:
    void createMainScene();
	QGraphicsPixmapItem*  initPixmapItem(unsigned width, unsigned height, QColor color, qreal zValue);
    QGraphicsPixmapItem*  initPixmapItem(unsigned width, unsigned height, QPixmap& pixmap, qreal zValue);
    void showFrame(TScreenFramePtr& framePtr, QGraphicsPixmapItem* pixmapItem);

 signals:

 private:
    TViewR*              mParent;
    TGraphicsScene*      mMainScene;
    TGraphicsView*       mMainView;
    QGraphicsPixmapItem* mThermoStreamPixmap;
    QGraphicsPixmapItem* mVideoStreamPixmap;
	QElapsedTimer        mTimer;
    QTimer               mWatchDogTimer[ChannelNum];
	QAtomicInt           mStreamExist[ChannelNum];
    QPixmap              mNoThermoStreamPixmap;
    QPixmap              mNoVideoStreamPixmap;
	#if defined(THERMO_CHANNEL_ENABLED)
		QCheckBox*		 mThermoAfCheckBox;
	#endif
	#if defined(VIDEO_CHANNEL_ENABLED)
		QCheckBox*		 mVideoAfCheckBox;
	#endif
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TGraphicsScene : public QGraphicsScene
{
 Q_OBJECT

 public:
    TGraphicsScene(QObject* parent = 0);
    ~TGraphicsScene();

 private:

 public slots:

 signals:

 private:
};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
class TGraphicsView : public QGraphicsView
{
    Q_OBJECT

    public:
        TGraphicsView(QGraphicsScene* scene, T_UI_Layer* manager);
        ~TGraphicsView();
        virtual void resizeEvent(QResizeEvent* event);
        /*TEST*/ void testViewRelativeCrd(QSize size, qreal scale);

   private:
        T_UI_Layer* mManager;
};

#endif // UI_LAYER_H


